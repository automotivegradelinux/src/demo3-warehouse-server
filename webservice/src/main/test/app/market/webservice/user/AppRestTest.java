/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.webservice.user;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.junit.Test;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import app.market.model.app.App;
import app.market.utils.constants.Constants;
import app.market.utils.json.JsonMapperUtils;
//import app.market.utils.webservice.WebServiceClient;
public class AppRestTest {

    //@Test
   /* public void saveApp(){
    	RestTemplate rest = new RestTemplate();
    	App app = new App();
    	app.setAppName("testAppkkkkkkkkkkkkkkkkkk");
    	app.setTypeId("0001");
    	app.setDeveloper("3e93dfe1-6ff0-4520-a668-b00c315c862b");
    	app.setVersionName("1.0");
    	app.setAppAbstract("lllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllllthis is play app apppklj;pppppjkljlkjlkjlkjlkjlkjlkj");

        HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_JSON.toString());

        String appJsonStr = JsonMapperUtils.writeValueAsString(app);
        HttpEntity<String> entity = new HttpEntity<String>(appJsonStr, headers);

    	String str = rest.postForObject(WebServiceClient.REST_APP_INFO, entity, String.class);

    	System.out.println(str);
    }

    @Test
    public void delAppInfo(){
    	RestTemplate rest = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add( Constants.TOKEN_AUTHORIZATION, "cddd" );

    	//rest.delete(WebServiceClient.REST_APP_INFO_PARM_ID, new String[]{"f02bc432-6ef9-4165-932e-3aede966ce1d"});
    	HttpEntity<String> formEntity = new HttpEntity<String>("", headers);
        ResponseEntity<String> exchange = rest.exchange(WebServiceClient.REST_APP_INFO_PARM_ID, HttpMethod.DELETE, formEntity, String.class, new String[]{"f02bc432-6ef9-4165-932e-3aede966ce1d"});

        System.out.println( exchange.getBody() );
    	//rest.delete(WebServiceClient.REST_APP_PARM_ID);
    }

    //@Test
    public void getAppInfo(){
    	RestTemplate rest = new RestTemplate();
    	String str = rest.getForObject(WebServiceClient.REST_APP_INFO_PARM_ID, String.class, new String[]{"f435bee3-acf7-4a0c-baa7-320f7330895a"});

    	System.out.println( str );
    }

    //@Test
    public void getAppType(){
    	RestTemplate rest = new RestTemplate();
    	String str = rest.getForObject(WebServiceClient.REST_APP_TYPE_COLLECTION, String.class);

    	System.out.println("getAppType=======" + str );
    }

    //@Test
	public void uploadAppFile() {
		RestTemplate rest = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
        MediaType type = MediaType.parseMediaType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        headers.setContentType(type);
        headers.add("Accept", MediaType.APPLICATION_OCTET_STREAM_VALUE);

		// 找到目标文件
		File file = new File("D:/aa.docx");
		// 建立数据的输入通道
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(file);

//			// 建立缓冲数组配合循环读取文件的数据。
//			// 保存每次读取到的字节个数。
//			byte[] buf = new byte[1024];
//			int length = 0;
//			// 存储读取到的数据缓冲数组的长度一般是1024的倍数，因为与计算机的处理单位。理论上缓冲数组越大，效率越高
//			while ((length = fileInputStream.read(buf)) != -1) {
//				// read方法如果读取到了文件的末尾，那么会返回-1表示。
//				System.out.println(length);
//				System.out.print(new String(buf, 0, length));
//			}
			// 关闭资源
			// fileInputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
		}


//    	App app = new App();
//    	app.setAppName("testApp");
//
//        HttpHeaders headers = new HttpHeaders();
//        MediaType type = MediaType.parseMediaType("application/json; charset=UTF-8");
//        headers.setContentType(type);
//        headers.add("Accept", MediaType.APPLICATION_JSON.toString());
//
//        String appJsonStr = JsonMapperUtils.writeValueAsString(app);
//        HttpEntity<String> entity = new HttpEntity<String>(appJsonStr, headers);
        HttpEntity<InputStream> entity = new HttpEntity<InputStream>(fileInputStream, headers);

		String str = rest.postForObject(WebServiceClient.REST_APP_FILE, entity, String.class,new String[] {"aa"});

		System.out.println(str);
	}

//	//@Test
//	public void testUpload() throws Exception {
//		String url = WebServiceClient.REST_APP_FILE_TEXT;
//		String filePath = "D:\\aa.docx";
//
//		RestTemplate rest = new RestTemplate();
//		FileSystemResource resource = new FileSystemResource(new File(filePath));
//		MultiValueMap<String, Object> param = new LinkedMultiValueMap<>();
//		param.add("jarFile", resource);
//		param.add("fileName", "test.txt");
//
//		//String string = rest.postForObject(url, param, String.class);
//
//		HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<MultiValueMap<String,Object>>(param);
//		ResponseEntity<String> responseEntity = rest.exchange(url, HttpMethod.POST, httpEntity, String.class);
//		System.out.println(responseEntity.getBody());
//
//		//System.out.println(string);
//	}

    //@Test
    public void selectAppList() {

        RestTemplate rest = new RestTemplate();
        //"/api/appRest/selectPaginationDataByExample/{appName}/{appTypeId}/{appDeveloper}/{sort}/{order}/{offset}/{limit}"
        String str = rest.getForObject( WebServiceClient.REST_APP_COLLECTION_APP, String.class,new String[] { "%22%22", "%22%22","%22%22","1","asc","0","10" } );

        System.out.println( str );
    }*/

}
