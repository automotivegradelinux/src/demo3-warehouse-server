/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.webservice.user;

import java.io.File;

import org.junit.Test;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import app.market.utils.constants.Constants;
//import app.market.utils.webservice.WebServiceClient;

public class UploadTest {

	/*@Test
	public void upload() {
		String url = WebServiceClient.REST_APP_FILE;
		String filePath = "D:\\testupload\\dashboard.wgt";

		RestTemplate rest = new RestTemplate();
		FileSystemResource resource = new FileSystemResource(new File(filePath));
		MultiValueMap<String, Object> param = new LinkedMultiValueMap<>();
		param.add("multipartFile", resource);
		param.add("appTypeId", "f435bee3-acf7-4a0c-baa7-320f733095a");
		param.add("appId", "f435bee3-acf7-4a0c-baa7320f7330895a");

        HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders .add( Constants.TOKEN_AUTHORIZATION, "fg" );
        requestHeaders.add( Constants.TOKEN_AUTHORIZATION_RESOURCE, "dsf" );

        HttpEntity<Object> requestEntity = new HttpEntity<Object>(param, requestHeaders );

		//String string = rest.postForObject(url, param, String.class);
        String string = rest.postForObject(url, param, String.class);
		System.out.println(string);
	}

	//@Test
	public void download() {
        RestTemplate rest = new RestTemplate();

		String url = WebServiceClient.REST_APP_FILE_PARM_FILENAME_TYPEID_APPID;
		String typeId = "f435bee3-acf7-4a0c-baa7-320f733095a";
		String appId = "f435bee3-acf7-4a0c-baa7320f7330895a";
		String fileName = "3af4d3e0-476e-4b42-b5b7-cd9b002537c4_dashboard.wgt";
		String[] var = new String[] { fileName, typeId, appId  };

    	ResponseEntity<byte[]> exchange = rest.exchange(url, HttpMethod.GET, null, byte[].class, var);

		System.out.println(exchange.getBody());
		System.out.println(exchange.getStatusCode());
		System.out.println(exchange.getHeaders().get("DispositionFormData"));
	}*/

}
