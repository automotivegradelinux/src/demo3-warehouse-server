/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market;

public class WebServiceConstants {

    /** Time-out */
    public static final long EXPIRE_TIME_MINUTES = Long.valueOf( PropertyUtil.getRedisPropertites( "redis.timeout" ) );
    public static final long EXPIRE_TIME_HOURS = Long.valueOf( PropertyUtil.getRedisPropertites( "redis.refreshtimeout" ) );

    public static final String PATTERN_SKIP_LOGIN_AUTHRIZATION = "[A-Za-z0-9_/-]+/login";
    public static final String PATTERN_SKIP_REFRESHTOKEN_AUTHRIZATION = "[A-Za-z0-9_/-]+/token/[A-Za-z0-9_/:-]+";
    public static final String PATTERN_RESOURSE_API = "./api/v1/(.*?)[/{*|]\\w";
    public static final String PATTERN_RESOURSE_S = "/api/v1/";
    public static final String PATTERN_RESOURSE_PARAM_S = "/{";
    public static final String SEPARATOR  = "/";

	public static final String RESOURSE_TYPE_RES = "resource";
	public static final String RESOURSE_TYPE_API = "api";
}
