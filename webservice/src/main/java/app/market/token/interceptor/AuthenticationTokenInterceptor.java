/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.token.interceptor;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import app.market.WebServiceConstants;
import app.market.token.service.RedisTokenManager;
import app.market.utils.constants.Constants;

@Aspect
@Component
public class AuthenticationTokenInterceptor extends HandlerInterceptorAdapter {

	@Autowired
	private RedisTokenManager tokenManager;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		if (Pattern.matches(WebServiceConstants.PATTERN_SKIP_LOGIN_AUTHRIZATION, request.getRequestURI())
				|| (Pattern.matches(WebServiceConstants.PATTERN_SKIP_REFRESHTOKEN_AUTHRIZATION, request.getRequestURI())
						&& HttpMethod.GET.name().equals(request.getMethod())))
			return true;

		// get token from header
		String authentication = request.getHeader(Constants.TOKEN_AUTHORIZATION);
		String servletPath = request.getHeader(Constants.TOKEN_AUTHORIZATION_RESOURCE);
		String url = request.getRequestURI();
		String httpMethod = request.getMethod();

		// check authentication
	    int status = tokenManager.checkAuthentication(authentication, servletPath, url, httpMethod);
		if (status != Constants.STATUS_SUCCESS) {
			response.setStatus(status);
			return false;
		}
		return true;
	}

}
