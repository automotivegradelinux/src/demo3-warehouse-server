/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.webservice.resource;

import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.market.common.comm;
import app.market.core.resource.DictionaryCore;
import app.market.model.errors.ResponseErrors;
import app.market.model.resource.Dictionary;
import app.market.model.resource.DictionaryExample;
import app.market.model.resource.DictionaryExample.Criteria;
import app.market.utils.constants.Constants;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.property.KeysConstants;
import app.market.utils.webservice.ApiParam;
import app.market.utils.webservice.ErrorCode;
import app.market.utils.webservice.ErrorCodeEnum;
import app.market.utils.webservice.WebServiceURI;
import app.market.webservice.WebserviceRestBaseController;

@RestController
public class DictionaryRestController extends WebserviceRestBaseController {

	private static Logger logger = LoggerFactory.getLogger(DictionaryRestController.class);

    @Autowired
    private DictionaryCore dicCore;

    @RequestMapping(value = WebServiceURI.REST_DICTIONARY_COLLECTION_LF, method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
	public String selectDictionary(@PathVariable(ApiParam.API_APP_PARAM_DICTIONARY_TYPEID) String dicTypeId,
			HttpServletResponse response) {
		ResponseErrors errors = new ResponseErrors();
		String responseStr = null;

		try {
			List<Dictionary> dic = dicCore.selectItemsByTypeId(dicTypeId);
			responseStr = JsonMapperUtils.writeValueAsString(dic);
		} catch (Exception e) {
			response.setStatus(Constants.STATUS_ERROR);
			errors.setcode(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
			errors.setMessage(e.getMessage());
		}

		if (StringUtils.isEmpty(responseStr)) {
			responseStr = JsonMapperUtils.writeValueAsString(errors);
		}

		logger.debug("IDにより、アプリ情報を検索  --E-- appId=" + dicTypeId);
		return responseStr;
	}

    /**
     * Add Or Update Type/Device Type
     * @return
     */
    @RequestMapping(value = WebServiceURI.REST_DICTIONARY_INFO_LF, method = RequestMethod.POST, produces = APPLICATION_JSON_UTF8_VALUE)
   	public String saveDictionary(@RequestBody Dictionary record, HttpServletResponse response) {
    	String responseStr = null;
    	try {
    		//Verify whether the name has already existed.
    		DictionaryExample verifyLabel = new DictionaryExample();
    		Criteria verify = verifyLabel.createCriteria();
    		verify.andDicLabelEqualTo(record.getDicLabel());
    		verify.andDicTypeEqualTo(record.getDicType());
    		int num = dicCore.countByExample(verifyLabel);
    		//Name has already existed.
    		if (num > 0) {
    			responseStr = comm.getResponseError(response, Constants.STATUS_ALREADY_EXISTS,
    					new ErrorCode(ErrorCodeEnum.TYPENAME_ALREADY_EXISTS, KeysConstants.TYPENAME_ALREADY_EXISTS));
    			return responseStr;
			}
			if (StringUtils.isEmpty(record.getDicValue())) {
				// create new type
				int i = dicCore.insert(record);
				if (i == 1) {
					responseStr = JsonMapperUtils.writeValueAsString(i);
				}
			} else {
				// update type
				DictionaryExample example = new DictionaryExample();
				Criteria uc = example.createCriteria();
				uc.andDicValueEqualTo(record.getDicValue());
				uc.andDicTypeEqualTo(record.getDicType());
				int i = dicCore.updateByExample(record, example);
				if (i == 1) {
					responseStr = JsonMapperUtils.writeValueAsString(i);
				}
			}
		} catch (Exception e) {
			responseStr = comm.getResponseError(response, Constants.STATUS_BAD_REQUEST,
					new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM,KeysConstants.MISSING_NECESSARY_QUERYPARAM));
		}
		return responseStr;

    }
}
