/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.webservice.resource;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.market.LogUtil;
import app.market.core.resource.ResourceCore;
import app.market.model.resource.Resource;
import app.market.token.service.RedisTokenManager;
import app.market.utils.constants.Constants;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.property.KeysConstants;
import app.market.utils.property.MessageUtil;
import app.market.utils.token.TokenModel;
import app.market.utils.webservice.WebServiceURI;
import app.market.webservice.WebserviceRestBaseController;

@RestController
public class ResourceRestController extends WebserviceRestBaseController {

	private static Logger logger = LoggerFactory.getLogger(ResourceRestController.class);

    @Autowired
    private RedisTokenManager tokenManager;

    @Autowired
    private ResourceCore resourceCore;

    @RequestMapping(value = WebServiceURI.REST_RESOURCE_GET_MENU_RESOURCES_BY_LOGINID_LF, method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public String selectMenuResourceByLoginId(@RequestHeader(Constants.TOKEN_AUTHORIZATION) String token) {
        JsonResult jr = new JsonResult();
        try {
            // read cache resource
            TokenModel model = tokenManager.getToken( token );
            List<Resource> list = new ArrayList<Resource>();
            for (Object obj : model.getResourceList()) {
                list.add( (Resource) obj );
            }
            jr.setData( JsonMapperUtils.writeValueAsString( list ) );
        } catch ( Exception e ) {
        	LogUtil.printCatchLog(logger, e);
            jr.setData( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
            jr.setStatus( Constants.STATUS_ERROR );
        }
        return JsonMapperUtils.writeValueAsString( jr );
    }
}
