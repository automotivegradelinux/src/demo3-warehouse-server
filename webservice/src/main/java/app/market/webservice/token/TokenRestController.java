/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.webservice.token;

import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.market.utils.constants.Constants;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.webservice.WebServiceURI;
import app.market.webservice.WebserviceRestBaseController;

@RestController
public class TokenRestController extends WebserviceRestBaseController {

    @RequestMapping(value = WebServiceURI.REST_TOKEN_VALIDATETOKENAUTHORTICATION_LF, method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public String validateTokenAuthortication(@RequestHeader(Constants.TOKEN_AUTHORIZATION) String token) {
        return JsonMapperUtils.writeValueAsString( new JsonResult() );
    }

}
