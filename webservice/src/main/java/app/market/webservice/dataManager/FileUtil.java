/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.webservice.dataManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import app.market.PropertyUtil;

import app.market.model.app.FileInfo;
import app.market.utils.RuntimeUtil;
import app.market.utils.Utils;
import app.market.utils.constants.Constants;
import app.market.utils.datetime.DateTimeUtils;

public class FileUtil {
	private static Logger logger = LoggerFactory.getLogger(FileUtil.class);// TODO DJ log确认要不要打，要打的话按照等级来做，catch中error，有可能出错的地方warn，其他的info

	/**
	 *
	 * @param type
	 * @param appId
	 * @return
	 */
	public static String getUploadPath(String path) {
		String dir = "";

		String uploadPath = PropertyUtil.getFileManagerPropertites("upload_path");
		if (StringUtils.isEmpty(uploadPath)) {
			uploadPath = Constants.UPLOAD_PATH_DEFAULT;
		}

		dir = uploadPath + path;
		File file = new File(dir);
		if (!file.exists()) {
			file.mkdirs();
		}

		return dir;
	}

	/**
	 * relative path
	 * @param type
	 * @param appId
	 * @return
	 */
	public static String getAppPath(String type, String appId){
		if (Utils.strIsEmpty(type)) {
			type = Constants.UPLOAD_PATH_TYPE_DEFAULT;
		}
		if (Utils.strIsEmpty(appId)) {
			appId = Constants.UPLOAD_PATH_APP_DEFAULT;
		}

		String path = Constants.UPLOAD_APP_FILE_PATH + Constants.PATH_SEPARATOR
				+ type + Constants.PATH_SEPARATOR
				+ appId + Constants.PATH_SEPARATOR;
		return path;
	}

	/***
	 *
	 * @param orgfilename
	 * @return
	 */
	public static String createFileName(String orgfilename) {
		String fileName = UUID.randomUUID().toString();

		if(StringUtils.isNotEmpty(orgfilename)){
			fileName += Constants.FILE_SEPARATOR + orgfilename;
		}

		return fileName;
	}

	/***
	 * generate file name
	 * @param path
	 * @return orgfileName
	 */
	public static String getOrgFileName(String path) {
		String orgfileName = path;
		//String fileName = path.substring(path.lastIndexOf(Constants.PATH_SEPARATOR) + 1);
		String fileName = Utils.getFileNameFromPath(path);
		if(fileName != null){
			try {
				int separatorIndex = UUID.randomUUID().toString().length() + Constants.FILE_SEPARATOR.length();
				orgfileName = fileName.substring(separatorIndex);
			} catch (Exception e) {
				logger.debug("getOrgFileName:"+e.getMessage());
			}
		}
		return orgfileName;
	}

	public static String encodeStr(String str) {
		try {
			return new String(str.getBytes(Constants.CHARACTER_ISO), Constants.CHARACTER_UTF8);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Uncompress File
	 * @param filePath:Compress File's Ordinary Path
	 * @return uncompressPath: Be Uncompressed File's Temperature Save Path
	 * @throws Exception
	 */
	public static String uncompress(String filePath) throws Exception {
		String ret = null;
		String nowTime =DateTimeUtils.getCurrectDate(DateTimeUtils.DATE_FORMAT_YYYYMMDDHHMMSS);
		String uncompressPath= getUploadPath(Constants.UPLOAD_APP_UNCOMPRESS_PATH) + Constants.PATH_SEPARATOR + nowTime;

		String cmd = "unzip " + filePath + " -d " +  uncompressPath;

		if(RuntimeUtil.linuxCmdExec(cmd)) {
			ret = uncompressPath;
		}
		return ret;
	}

	/**
	 * XML File Parse
	 * @param path:Be Uncompressed File's Temperature Save Path
	 * @param fileInfo
	 */
	public static void parseConfigInfo(String path, FileInfo fileInfo) {
		String configId ="";
		String configVersionName="";
		String configAppName = "";
		String iconPath = "";
		try {
			Document doc = XmlFactory.parse(path + Constants.UPLOAD_APP_UNCOMPRESS_FILE_CONFIG);
			Element root = doc.getRootElement();

			configId = root.attributeValue(Constants.CONFIG_APP_PARAM_ID);
			configVersionName = root.attributeValue(Constants.CONFIG_APP_PARAM_VERSION);
			configAppName = root.elementText(Constants.CONFIG_APP_PARAM_NAME);
			if (root.element(Constants.CONFIG_APP_PARAM_ICON) != null) {
				iconPath = root.element(Constants.CONFIG_APP_PARAM_ICON).attributeValue(Constants.CONFIG_APP_PARAM_SRC);
			}

			logger.debug("iconPath="+iconPath);
		} catch (Exception e) {
			logger.debug("getConfigInfo"+e.getMessage());
		}
		fileInfo.setConfigAppId(configId);
		fileInfo.setConfigVersionName(configVersionName);
		fileInfo.setConfigAppName(configAppName);
		fileInfo.setIconPath(iconPath);
	}

	/**
	 * save icon path
	 * @param path:Be Uncompressed File's Temperature Save Path
	 * @param iconPath:icon's name in config.xml file
	 * @throws FileNotFoundException
	 * @return Icon's Save Path to server
	 */
	public static String saveAppIcon(String path, String iconPath) throws FileNotFoundException {
		String ret = "";
		String orgPath = path + Constants.PATH_SEPARATOR + iconPath;
		String destPath = getImagePath(iconPath);

		logger.debug("destPath=" + destPath);
		logger.debug("orgPath=" + orgPath);

		String cmd = "cp " + orgPath + " " + destPath;
		logger.debug("cp_cmd=" + cmd);

		if(RuntimeUtil.linuxCmdExec(cmd)) {
			ret = FileUtil.getImageAccessPath(destPath);//http:\\\\localhost:8080\\resource/appIcon/xxxxx.png
			logger.debug("cp_ret=" + ret);
		}
		return ret;
	}

	/**
	 * get icon path
	 * @param filename:Icon's name
	 * @return Relative Path :appIcon/xxxxx.png
	 */
	public static String getImagePath(String filename) {
		//get image file name
		if(filename.contains(Constants.PATH_SEPARATOR)) {
			//filename = filename.substring(filename.lastIndexOf(Constants.PATH_SEPARATOR) + 1);
			filename = Utils.getFileNameFromPath(filename);
		}

		String imagePath = getUploadPath(Constants.UPLOAD_APP_RES_PATH_ICON);

		String fullPath = imagePath + Constants.PATH_SEPARATOR + createFileName(filename);
		return fullPath;
	}

	/**
	 * getImageAccessPath
	 * @param fullFilePath
	 * @return
	 */
	public static String getImageAccessPath(String fullFilePath) {
		String basePath = getUploadPath("");
		String httpPathHeader = PropertyUtil.getFileManagerPropertites("upload_icon_path");
		String path = fullFilePath.replace(basePath, httpPathHeader);

		logger.debug("getImageAccessPath=" + path);
		return path;
	}

	/**
	 *
	 * @param filePath:Be Uncompressed File's Temperature Save Path
	 * @return result
	 */
	public static int removeFile(String filePath) {
		String cmd = "rm -rf " + filePath;
		int ret = 0;
		System.out.println(cmd);
		if(RuntimeUtil.linuxCmdExec(cmd)) {
			ret = 1;
		}
		return ret;
	}
}
