/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.webservice.user;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSONObject;

import app.market.LogUtil;
import app.market.common.comm;
import app.market.core.app.AppCore;
import app.market.core.resource.ResourceCore;
import app.market.core.user.UserCore;
import app.market.model.app.AppExample;
import app.market.model.resource.Resource;
import app.market.model.user.User;
import app.market.model.user.UserExample;
import app.market.model.user.UserExample.Criteria;
import app.market.token.service.RedisTokenManager;
import app.market.utils.constants.Constants;
import app.market.utils.datetime.DateTimeUtils;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.property.KeysConstants;
import app.market.utils.token.TokenModel;
import app.market.utils.webservice.ApiParam;
import app.market.utils.webservice.ErrorCode;
import app.market.utils.webservice.ErrorCodeEnum;
import app.market.utils.webservice.WebServiceURI;
import app.market.webservice.WebserviceRestBaseController;

@RestController
public class UserRestController extends WebserviceRestBaseController {

    private static Logger logger = LoggerFactory.getLogger( UserRestController.class );
    @Autowired
    private AppCore appCore;

    @Autowired
    private UserCore userCore;

    @Autowired
    private ResourceCore resourceCore;

    @Autowired
    private RedisTokenManager tokenManager;

    /**
     * login
     *
     * @return
     */
	@RequestMapping(value = WebServiceURI.REST_USER_SELECTLOGINUSER_LF, method = RequestMethod.POST, produces = APPLICATION_JSON_UTF8_VALUE)
	public String selectLoginUser(@RequestBody String requestJson, HttpServletResponse response) {
		logger.debug("login RequestBody:" + requestJson);

		JSONObject jObj = JsonMapperUtils.getJsonObject(requestJson);
		String loginId = jObj.getString(ApiParam.API_APP_PARAM_LOGINID);
		String userPw = jObj.getString(ApiParam.API_APP_PARAM_PASSWORD);

		// get token from cache
		if (tokenManager.getCacheModel(loginId) != null) {
			// again login
			tokenManager.deleteToken(loginId);
		}

		JSONObject responseData = new JSONObject();
		String responseStr = null;

		UserExample userExample = new UserExample();
		Criteria uc = userExample.createCriteria();
		uc.andUserNameEqualTo(loginId);
		uc.andUserPwEqualTo(userPw);

		try {
			// get user information
			User user = userCore.selectByExample(userExample);
			responseStr = generateToken(user, loginId, false);
			if (responseStr != null) {
				response.setStatus(Constants.STATUS_SUCCESS);
			} else {
				responseStr =  comm.getResponseError(response, Constants.STATUS_UNAUTHORIZED,
						new ErrorCode(ErrorCodeEnum.LOGIN_FAILD,KeysConstants.LOGIN_LOGINID_IS_NOT_EXIST));
			}
		} catch (Exception e) {
			responseStr = comm.getResponseException(response, e);
		}

		logger.debug("login done,loginId:" + loginId);
		return responseStr;
	}

    /**
     * get access token
     * @param refreshToken
     * @param response
     * @return
     */
	@RequestMapping(value = WebServiceURI.REST_TOKEN_LF, method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
	public String getAccessToken(@PathVariable(ApiParam.API_APP_PARAM_REFRESHTOKEN) String refreshToken, HttpServletResponse response) {
		logger.debug("getAccessToken refreshToken:" + refreshToken);

		String  responseStr = null;
		String loginId = tokenManager.getRefreshtokenValue(refreshToken);

		if (loginId == null) {
			responseStr = comm.getResponseError(response, Constants.STATUS_UNAUTHORIZED,
					new ErrorCode(ErrorCodeEnum.EXPIRED_REFRESH_TOKEN, KeysConstants.EXPIRED_REFRESH_TOKEN));
			return responseStr;
		}

		try {
			User user = userCore.selectUserByUserName(loginId);
			//user.setUserPw("");
			responseStr = generateToken(user, loginId, true);
			if (responseStr != null) {
				response.setStatus(Constants.STATUS_SUCCESS);
			} else {
				responseStr =  comm.getResponseError(response, Constants.STATUS_UNAUTHORIZED,
						new ErrorCode(ErrorCodeEnum.EXPIRED_REFRESH_TOKEN,KeysConstants.EXPIRED_REFRESH_TOKEN));
			}
		} catch (Exception e) {
			responseStr = comm.getResponseException(response, e);
		}

		logger.debug("getAccessToken loginId:" + loginId);
		return responseStr;
	}

    /**
     * get user collection
     *
     * @param userName
     * @param mail
     * @param sort
     * @param order
     * @param offset
     * @param limit
     * @return
     * @throws ParseException
     */
    @RequestMapping(value = WebServiceURI.REST_USER_SELECTPAGINATIONDATABYEXAMPLE_LF, method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public String selectPaginationDataByExample(
	@RequestParam(value = ApiParam.API_USER_PARAM_CREATEDATE, required = false, defaultValue = "") String createDate,
	@RequestParam(value = ApiParam.API_USER_PARAM_AUID, required = false, defaultValue = "") String auId,
	@RequestParam(value = ApiParam.API_PARAM_ORDER, required = false, defaultValue = ApiParam.API_PARAM_VALUE_ORDER_ASC) String order,
	@RequestParam(value = ApiParam.API_PARAM_SORT, required = false, defaultValue = ApiParam.API_PARAM_DEFAULT_SORT_NAME ) String sort,
	@RequestParam(value = ApiParam.API_PARAM_OFFSET, required = false, defaultValue = "") Integer offset,
	@RequestParam(value = ApiParam.API_PARAM_LIMIT, required = false, defaultValue = "") Integer limit,
	@RequestParam(value = ApiParam.API_APP_PARAM_KEYWORD, required = false, defaultValue = "") String keyWord
	, HttpServletResponse response)
    {
    	logger.debug("get user collection --S--");
		if ((!ApiParam.API_PARAM_VALUE_ORDER_ASC.equalsIgnoreCase(order)&& !ApiParam.API_PARAM_VALUE_ORDER_DESC.equalsIgnoreCase(order))
				|| (ApiParam.UserQueryParam.get(sort) == null)
				|| limit < Integer.valueOf(ApiParam.API_PARAM_CRITICAL_LIMIT_MIN) || offset <  Integer.valueOf(ApiParam.API_PARAM_DEFAULT_OFFSET)) {
			return comm.getResponseError(response, Constants.STATUS_BAD_REQUEST,
					new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM, KeysConstants.STATUS_BAD_REQUEST));
		}

        UserExample example = getSearchExample(auId, createDate, keyWord, sort, order);

		String responseStr = null;
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			int counter = userCore.countByExample( example );
			List<User> list = userCore.selectByExample(offset, limit, example);

			map.put(Constants.PAGINATION_COUNTER, counter);
			map.put(Constants.PAGINATION_DATA, list);

			responseStr = JsonMapperUtils.writeValueAsString(map);
		} catch (Exception e) {
			responseStr = comm.getResponseException(response, e);
		}

		logger.debug("get user collection --E--");
        return responseStr;
    }

	private UserExample getSearchExample(String auId, String createDate, String keyWord, String sort, String order) {
		UserExample example = new UserExample();

		//set or condition
		if (StringUtils.isNotEmpty(keyWord)) {
			String likeKeyWord = "%" + keyWord + "%";

			Criteria ucName = example.createCriteria();
			setAndCondition(ucName, auId, createDate);
			ucName.andUserNameLike(likeKeyWord);

			Criteria ucMail = example.createCriteria();
			setAndCondition(ucMail, auId, createDate);
			ucMail.andMailAddressLike(likeKeyWord);
			example.or(ucMail);
		}else{
			Criteria uc = example.createCriteria();
			setAndCondition(uc, auId, createDate);
		}

        // set sort and order
		String sortName = ApiParam.UserQueryParam.get(sort);
		if (StringUtils.isNotEmpty(sortName)) {
			example.setOrderByClause(sortName + " " + order);
		}
		return example;
	}

	/**
	 *
	 * @param uc
	 * @param auId
	 * @param createDate
	 * @param deviceTypeId
	 * @param isPublic
	 * @param appTypeId
	 */
	private void setAndCondition(Criteria uc, String auId, String createDate)
	{
        uc.andIsDelEqualTo( Constants.DATA_VALID );
        uc.andUserNameNotEqualTo(Constants.GUEST_ID);
        uc.andUserNameNotEqualTo(Constants.ADMID_ID);
        if ( StringUtils.isNotEmpty( auId ) ) {
            uc.andAuIdEqualTo( auId );
        }
        if (StringUtils.isNotEmpty( createDate ) ) {
            Date endDate = DateTimeUtils.getDate(DateTimeUtils.DATE_FORMAT_YYYYMMDD,DateTimeUtils.betweenTime(createDate));
     	   Date beginDate = DateTimeUtils.getDate(DateTimeUtils.DATE_FORMAT_YYYYMMDD,createDate);
     	   uc.andCreateDateBetween(beginDate, endDate);
         }
	}

    /**
     * get user information from id
     *
     * @return
     */
    @RequestMapping(value = WebServiceURI.REST_USER_BY_USERID_LF, method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public String selectUserByUserId(@PathVariable("userId") String userId
    		, HttpServletResponse response) {
		logger.debug("get user information from id --S-- userId=" + userId);
		String responseStr = "";

		try {
			User user = userCore.selectUserByUserId( userId );
			if(user == null){
				return responseStr;
			}
			responseStr = JsonMapperUtils.writeValueAsString(user);
		} catch (Exception e) {
			responseStr = comm.getResponseException(response, e);
		}

		logger.debug("get user information from id  --E-- userId=" + userId);
		return responseStr;
    }

    /**
     * get current user
     *
     * @return
     */
    @RequestMapping(value = WebServiceURI.REST_USER_SELECT_CURRENT_LF, method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public String selectCurrentUser(@RequestHeader(Constants.TOKEN_AUTHORIZATION) String token) {
        logger.debug( "get current user start, token=" + token );
        JsonResult jr = new JsonResult();
        try {
            TokenModel tokenModel = tokenManager.getToken( token );
            User user = (User) tokenModel.getUserEntity();
            jr.setData( JsonMapperUtils.writeValueAsString( user ) );
        } catch ( Exception e ) {
        	LogUtil.printCatchLog(logger, e);
            jr.setData( e.getMessage() );
            jr.setStatus( Constants.STATUS_ERROR );
        }
        logger.debug( "get current user done, token=" + token );
        return JsonMapperUtils.writeValueAsString( jr );
    }

    /**
     * save user information
     *
     * @param user
     * @return
     */
    @RequestMapping(value = WebServiceURI.REST_USER_LF, method = RequestMethod.POST, consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public String saveUser(@RequestBody String jsonStr, HttpServletResponse response) {
        logger.debug( "save user start, user=" + jsonStr.toString() );

		if(StringUtils.isEmpty(jsonStr)){
			return comm.getResponseError(response, Constants.STATUS_BAD_REQUEST,
					new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM,KeysConstants.INVALID_QUERYPARAM));
		}

		User user = JsonMapperUtils.readValue(jsonStr, User.class);
		if(user == null){
			return comm.getResponseError(response, Constants.STATUS_BAD_REQUEST,
					new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM,KeysConstants.INVALID_QUERYPARAM));
		}

		//createDate and updateDate
		Date date = new Date();
		if(user.getUserId().isEmpty()){
			user.setCreateDate(date);
			user.setUpdateDate(date);
		}else{
			user.setUpdateDate(date);
		}
		//check parameter
		ErrorCode paramCheckResult = checkParamSave(user);
		if (paramCheckResult != null) {
			return comm.getResponseError(response, Constants.STATUS_BAD_REQUEST, paramCheckResult);
		}
		user.setIsDel(Constants.DATA_VALID);

		JSONObject responseData = new JSONObject();
		String responseStr = null;
		try {
			int r = userCore.saveUser( user );
			if (r == 0) {
				responseStr = comm.getResponseError(response, Constants.STATUS_BAD_REQUEST,
						new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM,KeysConstants.MISSING_NECESSARY_QUERYPARAM));
			} else if(r == -1){
				responseStr = comm.getResponseError(response, Constants.STATUS_ALREADY_EXISTS,
						new ErrorCode(ErrorCodeEnum.RESOURCE_ALREADY_EXISTS,KeysConstants.RESOURCE_ALREADY_EXISTS));
			} else {
				responseData.put(ApiParam.API_RESPONSE_USERID, user.getUserId());
				responseStr = responseData.toJSONString();
			}
		} catch (Exception e) {
			responseStr = comm.getResponseException(response, e);
		}
		logger.debug("save user end --E-- appStr=" + jsonStr);
		return responseStr;
    }

	/**
     * delete user
     *
     * @param userExample
     * @return
     */
    @RequestMapping(value = WebServiceURI.REST_USER_BY_USERID_LF, method = RequestMethod.DELETE, produces = APPLICATION_JSON_UTF8_VALUE)
	public String deleteByUserId(@PathVariable("userId") String id, HttpServletResponse response) {
		logger.debug("delete user start --S--" + id);
		String responseStr = "";

		ErrorCode paramCheckResult = checkParamDel(id);
		if (paramCheckResult != null) {
			return comm.getResponseError(response, Constants.STATUS_ALREADY_EXISTS, paramCheckResult);
		}

		try {
			int r = userCore.deleteByUserId(id);
			if (r == 0) {
				responseStr = comm.getResponseError(response, Constants.STATUS_BAD_REQUEST,
						new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM, KeysConstants.INVALID_OPERATION));
			}
		} catch (Exception e) {
			responseStr = comm.getResponseException(response, e);
		}
		logger.debug("delete user end --E-- appId=" + id);
		return responseStr;
	}

    /**
     * check user have app
     * @param id
     * @return
     */
	private ErrorCode checkParamDel(String id) {
		try {
			AppExample example = new AppExample();
			app.market.model.app.AppExample.Criteria uc = example.createCriteria();
			uc.andDeveloperEqualTo(id);
			uc.andAppIsPublicEqualTo(ApiParam.API_PARAM_DEFAULT_IS_PUBLIC);
			if (appCore.countByExample(example) > 0) {
				return new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM, KeysConstants.USER_NOT_DELETE);
			}
		} catch (Exception e) {
			return new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM, KeysConstants.INVALID_QUERYPARAM);
		}

		return null;
	}

	/**
     * check parameter for update/create application
     * @param user
     * @return
     */
    private ErrorCode checkParamSave(User user) {
		if (StringUtils.isEmpty(user.getUserId())) {// create application
			if (StringUtils.isEmpty(user.getUserPw().replace(" ", "")) || user.getCreateDate() == null
					|| StringUtils.isEmpty(user.getUserName().replace(" ", ""))|| StringUtils.isEmpty(user.getMailAddress().replace(" ", ""))
					|| StringUtils.isEmpty(user.getAuId().replace(" ", ""))){
				return new ErrorCode(ErrorCodeEnum.MISSING_NECESSARY_QUERYPARAM,
						KeysConstants.MISSING_NECESSARY_QUERYPARAM);
			}
			if (user.getUserPw().indexOf(" ") != -1) {
				return new ErrorCode(ErrorCodeEnum.MISSING_NECESSARY_QUERYPARAM,
						KeysConstants.USER_PASSWORD_IS_NOT_SPACES);
			}
		} else if (user.getUpdateDate() == null) {// update application
			return new ErrorCode(ErrorCodeEnum.MISSING_NECESSARY_QUERYPARAM,
					KeysConstants.MISSING_NECESSARY_QUERYPARAM);
		}

		//check auId
		try{
		    int auId = Integer.valueOf(user.getAuId());
		    if(auId < ApiParam.UserAuId.ADIMIN.getId() || auId > ApiParam.UserAuId.NORMAL.getId()){
		    	return new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM, KeysConstants.INVALID_QUERYPARAM);
		    }
		}catch (Exception e) {
			return new ErrorCode(ErrorCodeEnum.INVALID_QUERYPARAM, KeysConstants.INVALID_QUERYPARAM);
		}
		return null;
	}

	/**
	 * generate token
	 * @param user
	 * @param loginId
	 * @param isRefreshToken
	 * @return token,refresh token
	 */
	private String generateToken(User user, String loginId, boolean isRefreshToken) {
		JSONObject responseData = new JSONObject();
		String responseStr = null;

		if (user != null) {
			// get resource
			List<Resource> list = resourceCore.selectResourcesByLoginId(loginId);
			// set token
			String accessToken = loginId + Constants.TOKEN_SEPARATOR + tokenManager.generateToken(loginId, user, list, false).getToken();
			responseData.put(ApiParam.API_RESPONSE_TOKEN, accessToken);

			if (!isRefreshToken) {
				String refreshToken = loginId + Constants.TOKEN_SEPARATOR + tokenManager.generateRefreshToken(loginId);
				responseData.put(ApiParam.API_RESPONSE_REFRESHTOKEN, refreshToken);
			}

			responseStr = responseData.toJSONString();
		}

		return responseStr;
	}
}

