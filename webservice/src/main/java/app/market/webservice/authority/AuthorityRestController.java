/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.webservice.authority;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.market.common.comm;
import app.market.core.authority.AuthorityCore;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.property.Option;
import app.market.utils.webservice.WebServiceURI;
import app.market.webservice.WebserviceRestBaseController;

@RestController
public class AuthorityRestController extends WebserviceRestBaseController {

	private static Logger logger = LoggerFactory.getLogger( AuthorityRestController.class );

    @Autowired
    private AuthorityCore authorityCore;

    /**
     * get authority list
     *
     * @param userExample
     * @return
     */
    @RequestMapping(value = WebServiceURI.REST_AUTHORITY_GET_LIST_OPTION_LF, method = RequestMethod.GET, produces = APPLICATION_JSON_UTF8_VALUE)
    public String selectAuthorityListOption(HttpServletResponse response) {
		logger.debug(" selectAuthorityListOption --S--");
		String responseStr = null;

		try {
			List<Option> auList = authorityCore.getAuListOption();
			responseStr = JsonMapperUtils.writeValueAsString(auList);
		} catch (Exception e) {
			responseStr = comm.getResponseException(response, e);
		}

		logger.debug("selectAuthorityListOption  --E--");
		return responseStr;
    }

}
