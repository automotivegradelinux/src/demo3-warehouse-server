/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import app.market.utils.constants.Constants;

public class PropertyUtil {

    public static Properties defaultProp;
    public static Properties fileProp;
    public static Properties redisProp;

	private static Properties getPropertyInstance(String fileName) {
		Properties prop = new Properties();
		try {
			prop.load(new InputStreamReader(PropertyUtil.class.getClassLoader().getResourceAsStream(fileName),
					Constants.CHARACTER_UTF8));
		} catch (FileNotFoundException e) {
			throw new RuntimeException();
		} catch (IOException e) {
			throw new RuntimeException();
		}
		return prop;
	}

	/**
	 * get Redis property
	 *
	 * @param key
	 * @return String
	 */
	public static String getRedisPropertites(String key) {
		if (redisProp == null) {
			redisProp = getPropertyInstance(Constants.PROPERTIES_FILE_NAME_REDIS);
		}
		return redisProp.getProperty(key);
	}

	/**
	 * get file property
	 *
	 * @param key
	 * @return String
	 */
	public static String getFileManagerPropertites(String key) {
		if (fileProp == null) {
			fileProp = getPropertyInstance(Constants.PROPERTIES_FILE_NAME_FILE);
		}
		return fileProp.getProperty(key);
	}

	/**
	 * get propertites
	 *
	 * @param key
	 * @return String
	 */
	public static String getPropertites(String key) {
		if (defaultProp == null) {
			defaultProp = getPropertyInstance(Constants.PROPERTIES_FILE_NAME_DEFAULT);
		}
		return defaultProp.getProperty(key);
	}
}
