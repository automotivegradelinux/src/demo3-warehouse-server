/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.common;

import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;

import app.market.model.errors.ResponseErrors;
import app.market.utils.constants.Constants;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.webservice.ErrorCode;

public class comm {

	public static String getResponseError(HttpServletResponse response, int httpCode, ErrorCode errorcode)
	{
		//set Response code
		ResponseErrors errors = new ResponseErrors();
		response.setStatus(httpCode);

		//set errors message
		errors.setcode(errorcode.getCodeStr().toString());
		errors.setMessage(errorcode.getMessage());
		return JsonMapperUtils.writeValueAsString(errors);
	}

	public static String getResponseException(HttpServletResponse response, Exception e) {
		ResponseErrors errors = new ResponseErrors();
		response.setStatus(Constants.STATUS_ERROR);
		errors.setcode(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
		errors.setMessage(e.getMessage());
		return JsonMapperUtils.writeValueAsString(errors);

	}


}
