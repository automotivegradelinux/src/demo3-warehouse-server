/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.controller.login;

import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import app.market.utils.Md5Util;
import app.market.utils.constants.Constants;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.property.KeysConstants;
import app.market.utils.property.MessageUtil;
import app.market.utils.webservice.ApiParam;
import app.market.web.controller.ControllerMapping;
import app.market.web.controller.PageMapping;
import app.market.web.controller.SpringBaseController;
import app.market.web.services.login.LoginService;

/**
 * login
 *
 * @author Toyota
 * @date 2017/10/10
 */
@Controller
@RequestMapping(value = "login")
public class LoginController extends SpringBaseController {

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "")
    public ModelAndView init() {
        return new ModelAndView( PageMapping.LOGIN.toString(), null );
    }

    /**
     * signin
     *
     * @param loginId
     * @param userPw
     * @param httpSession
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "signin")
    @ResponseBody
    public String login(@RequestParam(value = "loginId") String loginId,
    		@RequestParam(value = "userPw") String userPw,
    		HttpSession session) throws Exception {
    	//MD5 encrypt
    	String userPwMd = Md5Util.md5(userPw);
        List<String> errList = new LinkedList<String>();
        if ( StringUtils.isEmpty( loginId ) ) {
            errList.add( MessageUtil.getPropertites( KeysConstants.LOGIN_LOGINID_IS_NOT_EMPTY ) );
        }
        if ( StringUtils.isEmpty( userPw ) ) {
            errList.add( MessageUtil.getPropertites( KeysConstants.LOGIN_PASSWORD_IS_NOT_EMPTY ) );
        }
        if ( errList.size() > 0 ) {
            return JsonMapperUtils.getJsonString( Constants.STATUS_UNAUTHORIZED, null, errList );
        }

        try {
        	String port = (String)session.getAttribute(Constants.SESSION_PORT);
        	String systemToken = (String)session.getAttribute(Constants.SESSION_SYSTEM_TOKEN);
        	String installPath = (String)session.getAttribute(Constants.SESSION_INSTALL_PATH);
            String jsonStr = loginService.loginUser( loginId, userPwMd, session);
            Map<String,String> value = JSON.parseObject(jsonStr, Map.class);
            boolean result = JsonMapperUtils.getResult(jsonStr);
            if(result){
            	String message = (String)JsonMapperUtils.getMessage(jsonStr);
            	JSONObject jsonObj = JsonMapperUtils.getJsonObject(message);
            	String token = jsonObj.getString(ApiParam.API_RESPONSE_TOKEN);
            	String refreshtoken = jsonObj.getString(ApiParam.API_RESPONSE_REFRESHTOKEN);
            	session.setAttribute( Constants.SESSION_TOKEN, token );
            	session.setAttribute( Constants.SESSION_REFRESH_TOKEN, refreshtoken );

            	if(StringUtils.isNotEmpty(port) && StringUtils.isNotEmpty(systemToken) && StringUtils.isNotEmpty(installPath)){
            		String param = "?" + Constants.ACCESS_PORT + "=" + port + "&" + Constants.ACCESS_SYSTEM_TOKEN + "=" + systemToken + "&" + Constants.ACCESS_INSTALL_PATH + "=" + installPath;
            		value.put("forward",ControllerMapping.MAINDEV.toString() + param);
            	}else{
            		value.put("forward",ControllerMapping.MAIN.toString());
            	}
            	jsonStr = JSON.toJSONString(value);
            }
            return jsonStr;
        } catch ( Exception e ) {
        	e.getMessage();
            throw new RuntimeException( e.getMessage() );
        }
    }

    /**
     * login screen init
     *
     * @param httpSession
     * @return
     */
    @RequestMapping(value = "init")
    @ResponseBody
    public String init(HttpSession httpSession) {
        // Enumeration<String> attributes = httpSession.getAttributeNames();
        // while ( attributes.hasMoreElements() ) {
        // String name = attributes.nextElement();
        // httpSession.removeAttribute( name );
        // }
       /* httpSession.invalidate();*/
        return JsonMapperUtils.getJsonString( Constants.STATUS_SUCCESS, ControllerMapping.LOGIN.toString(), null );
    }

    /**
     * logout
     *
     * @param httpSession
     * @return
     */
    @RequestMapping(value = "logout")
    @ResponseBody
	public String logout(HttpSession httpSession) {
		Enumeration<String> attributes = httpSession.getAttributeNames();
		while (attributes.hasMoreElements()) {
			String name = attributes.nextElement();
			httpSession.removeAttribute(name);
		}
		httpSession.invalidate();
		return "";
	}

}