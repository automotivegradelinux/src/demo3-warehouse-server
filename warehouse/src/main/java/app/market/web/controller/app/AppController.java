/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.controller.app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import app.market.LogUtil;
import app.market.model.user.User;
import app.market.utils.SpringWebUtil;
import app.market.utils.constants.Constants;
import app.market.utils.datetime.DateTimeUtils;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.property.KeysConstants;
import app.market.utils.property.MessageUtil;
import app.market.utils.webservice.ApiParam;
import app.market.web.controller.BreadcrumbMapping;
import app.market.web.controller.PageMapping;
import app.market.web.controller.SpringBaseController;
import app.market.web.form.app.AppForm;
import app.market.web.services.app.AppService;
import app.market.web.services.main.MainService;
import app.market.web.services.user.UserService;


/**
 *
 * @author Toyota
 *
 * App Manager
 */
@Controller
@RequestMapping(value = "app")
public class AppController extends SpringBaseController {
    private static Logger logger = LoggerFactory.getLogger( AppController.class );

    @Autowired
    private AppService appService;

    @Autowired
    private MainService mainService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "")
    public ModelAndView init(@RequestParam(value = "token", required = false) String token, HttpSession session) throws Exception {
        logger.debug( "APPinit--Class: " + this.getClass().getName() + "--method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() );
        LinkedHashMap<String, Object> model = new LinkedHashMap<>();
        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            model.put( MODEL_ERRORS, jr.getData().toString() );
            return new ModelAndView( PageMapping.APP_LIST.toString(), model );
        }
        try {
        	// Get App Option
			appService.getAppTypeOption(model, true, session);
			appService.getDeviceTypeOption(model, true, session);
        } catch ( Exception e ) {
        	model.put( MODEL_ERRORS, e.getMessage() );
            return new ModelAndView( PageMapping.APP_LIST.toString(), model );
        }
        // Search User name
        if(!StringUtils.isEmpty(token)){
			User user = userService.selectCurrentUser(session);
        	model.put( "username", user.getUserName() );
        	model.put( "userid",user.getUserId() );
        	model.put( "auid", user.getAuId());
        }
        model.put( "menuPathString", mainService.selectMenuResourceByLoginId(session) );
        return new ModelAndView( PageMapping.APP_LIST.toString(), model );
    }

    /**
     * App Manager Detail
     *
     * @param appId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "more")
    @ResponseBody
    public ModelAndView more(@RequestParam(value = "appId", required = false) String appId
    		, @RequestParam(value = "token", required = false) String token
    		, HttpSession session) throws Exception {
        logger.debug( "APP modify--Class: " + this.getClass().getName() + "--method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() );

        Map<String, Object> model = new LinkedHashMap<String, Object>();

        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            model.put( MODEL_ERRORS, jr.getData().toString() );
            return new ModelAndView( PageMapping.APP_LIST.toString(), model );
        }
        AppForm apForm = new AppForm();
        try {
        	// Search AppForm By AppId
			apForm = appService.searchFormById(appId, session);
            // Get App Option
            appService.getAppTypeOption( model, true, session );
            apForm.getBreadcrumb().addAll( BreadcrumbMapping.getBreadcrumb(BreadcrumbMapping.APP_DETAIL).getBreadcrumb() );
        } catch ( Exception e ) {
            model.put( MODEL_ERRORS, e.getMessage() );
            return new ModelAndView( PageMapping.APP_LIST.toString(), model );
        }

        // Get App Option
        appService.getAppTypeOption( model, true, session );
        String date = DateTimeUtils.getDate(DateTimeUtils.DATE_FORMAT_YYYYMMDD, apForm.getCreateDate());
        model.put( "appInfo", apForm );
        model.put("date", date);
        model.put( Constants.TOKEN_AUTHORIZATION,token);
        logger.debug( "APP modify" );
        return new ModelAndView( PageMapping.APP_MODIFY.toString(), model );
    }

    /**
     * App Manager initCreate
     *
     * @param appId
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "initCreate")
    public ModelAndView initCreate(@RequestParam(value = "appId", required = false) String appId,
            @RequestParam(value = "token", required = false) String token, HttpSession session) throws Exception {
        logger.debug( "APP save--Class: " + this.getClass().getName() + "--method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() );

        Map<String, Object> model = new LinkedHashMap<String, Object>();

        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            model.put( MODEL_ERRORS, jr.getData().toString() );
            return new ModelAndView( PageMapping.APP_LIST.toString(), model );
        }

        AppForm apForm;
        String breadcrumb;
        try {
        	// Get App Option
            appService.getDeviceTypeOption(model, false, session);
        	apForm = new AppForm();
            breadcrumb = BreadcrumbMapping.APP_INSERT;
        	apForm.getBreadcrumb().addAll( BreadcrumbMapping.getBreadcrumb(breadcrumb).getBreadcrumb() );
        } catch (Exception e) {
			LogUtil.printCatchLog(logger, e);
			model.put(MODEL_ERRORS, e.getMessage());
			return new ModelAndView(PageMapping.APP_LIST.toString(), model);
		}

        // Search Userid
        model.put( "appInfo", apForm );
        model.put( "userid", userService.selectCurrentUser( session ).getUserId() );
        logger.debug( "APP save" );
        return new ModelAndView( PageMapping.CREATE_APP.toString(), model );
    }

	/**
	 * App Manager createApp
	 *
	 * @param formString
	 * @return
	 */
	@RequestMapping(value = "createApp")
	@ResponseBody
	public String createApp(@RequestParam(value = "file",required = false) MultipartFile file,
			@RequestParam(value = "form") String formString, HttpSession session) {
		logger.debug("APP update start --Class: " + this.getClass().getName() + "--method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
		logger.debug("formString= " + formString);

		List<String> errorList = new ArrayList<String>();

		// Unauthorized Exception
		JsonResult jr = appService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			return writeErrorList(jr, errorList);
		}
		AppForm appForm = JsonMapperUtils.readValue(formString, AppForm.class);
		checkUpdateInfo(appForm, errorList, file);

		String ret="";
		if (CollectionUtils.isEmpty(errorList)) {
			try {
				String token = SpringWebUtil.getRequest().getParameter("token");
				User curUser = userService.selectCurrentUser(session);
				appForm.setDeveloper(curUser.getUserId());
				ret = appService.save(appForm, session);
			} catch (Exception e) {
				logger.error(e.getMessage());
				ret = JsonMapperUtils.getJsonString(Constants.STATUS_ERROR, null, errorList);
			}
		} else {
			ret = JsonMapperUtils.getJsonString(Constants.STATUS_ERROR, null, errorList);
		}
		logger.debug("APP update stop");
		return ret;
	}

	/**
	 * App Manager initInfo
	 *
	 * @param appId
	 * @param isCreate
	 * @return
	 */
	@RequestMapping(value = "initInfo")
	public ModelAndView initInfo(String appId, int modifyFlag, HttpSession session){
		LinkedHashMap<String, Object> model = new LinkedHashMap<>();
		AppForm appForm = new AppForm();
		String breadcrumb;

		// Unauthorized Exception
		JsonResult jr = appService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			model.put(MODEL_ERRORS, jr.getData());
			//return writeErrorList(jr, jr.getData());
		}else {
			try{
				appForm = appService.searchFormById(appId, session);
				appService.getAppTypeOption( model, false, session);
		        appService.getIsPublicOption(model, false, session);

	            if(modifyFlag == Constants.APP_CREATE_DETAIL){
	            	//create
	            	breadcrumb = BreadcrumbMapping.APP_INSERT;
	            	appForm.getBreadcrumb().addAll( BreadcrumbMapping.getBreadcrumb(breadcrumb).getBreadcrumb() );
	            }else{
	            	if(modifyFlag == Constants.APP_MODIFY){
	            		//list modfiy
	            		breadcrumb = BreadcrumbMapping.APP_MODIFY;
	        	        appForm.getBreadcrumb().addAll( BreadcrumbMapping.getBreadcrumb(breadcrumb).getBreadcrumb() );
	            	}else if(modifyFlag == Constants.APP_DETAIL_MODIFY){
	            		//detail modfiy
	            		breadcrumb = BreadcrumbMapping.APP_DETAIL_MODIFY;
	        	        appForm.getBreadcrumb().addAll( BreadcrumbMapping.getBreadcrumb(breadcrumb).getBreadcrumb() );
	            	}
	                String url = appForm.getBreadcrumb().get(1).getUrl();
	                url = url+"?"+"appId="+appId;
	                appForm.getBreadcrumb().get(1).setUrl(url);
	            }
			}catch (Exception e) {
				logger.error(e.getMessage());
				model.put(MODEL_ERRORS, e.getMessage());
			}
			model.put("appInfo", appForm);
		}

		return new ModelAndView( PageMapping.SAVE_APP_INFO.toString(), model );
	}

	/**
	 * App Manager saveAppInfo
	 *
	 * @param formString
	 * @return
	 */
	@RequestMapping(value = "saveInfo")
	@ResponseBody
	public String saveInfo(@RequestParam(value = "file",required = false) MultipartFile file,
			@RequestParam(value = "form") String formString, HttpSession session) {
		logger.debug("APP update start --Class: " + this.getClass().getName() + "--method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
		logger.debug("formString= " + formString);

		List<String> errorList = new ArrayList<String>();

		// Unauthorized Exception
		JsonResult jr = appService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			return writeErrorList(jr, errorList);
		}
		AppForm appForm = JsonMapperUtils.readValue(formString, AppForm.class);
		checkUpdateInfo(appForm, errorList, file);
		String ret = "";
		if (CollectionUtils.isEmpty(errorList)) {
			try {
				User curUser = userService.selectCurrentUser(session);
				appForm.setIsDel("0");
				appForm.setDeveloper(curUser.getUserId());
				ret = appService.save(appForm, session);
				if(JsonMapperUtils.getResult(ret)) {
					if (file != null) {
						// Picture name is fileName
						String fileName = file.getOriginalFilename();
						appForm.setImagePath(null);
						ret = appService.upload(appForm, file, fileName, true, session);
					}
					ret = appService.saveVersion(appForm, session);
				}
			} catch (Exception e) {
				logger.error(e.getMessage());
				ret = JsonMapperUtils.getJsonString(Constants.STATUS_ERROR, null, errorList);
			}
		} else {
			ret = JsonMapperUtils.getJsonString(Constants.STATUS_ERROR, null,errorList);
		}
		logger.debug("APP update stop");
		return ret;
	}

	/**
	 * App Manager initCheck
	 *
	 * @param formString
	 * @return
	 */
	@RequestMapping(value = "initCheck")
	public ModelAndView initCheck(String appId, HttpSession session){
		LinkedHashMap<String, Object> model = new LinkedHashMap<>();

		// Unauthorized Exception
		JsonResult jr = appService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			model.put(MODEL_ERRORS, jr.getData());
			//return writeErrorList(jr, jr.getData());
		}
		AppForm appForm = new AppForm();
		String date = "";
		try{
			appForm = appService.searchFormById(appId, session);
			date = DateTimeUtils.getDate(DateTimeUtils.DATE_FORMAT_YYYYMMDD, appForm.getCreateDate());
			appForm.getBreadcrumb().addAll( BreadcrumbMapping.getBreadcrumb(BreadcrumbMapping.APP_INSERT).getBreadcrumb() );
		}catch (Exception e) {
			logger.error(e.getMessage());
			model.put(MODEL_ERRORS, e.getMessage());
		}
		model.put("date", date);
		model.put("appInfo", appForm);
		return new ModelAndView( PageMapping.CHECK_APP_INFO.toString(), model );
	}

	/**
	 * App Manager check
	 *
	 * @param
	 * @return
	 */
	@RequestMapping(value = "check")
	@ResponseBody
	public String check(String appId){
		String ret = JsonMapperUtils.getJsonString(Constants.STATUS_SUCCESS, PageMapping.APP.toString(), null);
		return ret;
	}

	/**
     * App Manager Search
     *
     * @param draw
     * @param length
     * @param start
     * @param sort
     * @param appDeveloper
     * @param appTypeId
     * @param appName
     * @param order
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "search")
    @ResponseBody
    public String search(@RequestParam(value = "draw") String draw, @RequestParam(value = "length") int length,
            @RequestParam(value = "start") int start, @RequestParam(value = "orderColumn") String sort,
            @RequestParam(value = "typeId") String appTypeId,@RequestParam(value = "orderDir") String order,
            @RequestParam(value = "deviceTypeId") String appDeviceTypeId,@RequestParam(value = "token", required = false) String token,
            @RequestParam(value = "keyWord") String keyWord, HttpSession session) throws Exception {
        logger.debug( "Search list start --Class: " + this.getClass().getName() + "--method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() + keyWord );

        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            return writeDataTableMap( jr, draw );
        }
        // Assign value To AppForm
        AppForm form = new AppForm();
        form.setDraw( draw );
        form.setAppDeviceTypeId(appDeviceTypeId);
        form.setTypeId(appTypeId);
        form.setOffset( start );
        form.setLimit( length );
        form.setSort( "createDate" );
        form.setOrder( ApiParam.API_PARAM_VALUE_ORDER_DESC );
        form.setIsDel( "0" );
        form.setKeyWord(keyWord);
		User user = userService.selectCurrentUser(session);
        if(user == null){
        	form.setAppIsPublic(ApiParam.API_PARAM_DEFAULT_IS_PUBLIC);
        }
        // Page Search
        String ret = appService.selectPaginationData( form, session );
        logger.debug( "Search list stop" );
        return ret;
    }

   @RequestMapping(value = "downloadenter")
	@ResponseBody
	public void downloadtest() {
		logger.debug( "download done" );
	}
    /**
     * App Manager Download File
     *
     * @param appId
     * @param typeId
     * @param filePath
     * @param response
     * @throws HttpException
     * @throws FileNotFoundException
     */
	@RequestMapping(value = "download")
	@ResponseBody
	public void download(@RequestParam(value = "appId", required = false) String appId,
			@RequestParam(value = "typeId", required = false) String typeId,
			@RequestParam(value = "filePath", required = false) String filePath,
			HttpServletResponse response, HttpSession session)
			throws HttpException, FileNotFoundException {
        logger.debug( "download--Class: " + this.getClass().getName() + "--method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() + filePath );
		// Assign value To AppForm
		AppForm form = new AppForm();
		form.setAppId(appId);
		form.setTypeId(typeId);
		form.setVerFilePath(filePath);

		try {
			// App Download
			ResponseEntity<byte[]> rEntity = appService.download(form, session);
			response.setContentType(rEntity.getHeaders().getContentType().toString());
			response.setContentLength((int) rEntity.getHeaders().getContentLength());
			String headerKey = "Content-Disposition";
			String contentDisponsition = rEntity.getHeaders().get(headerKey).get(0);
			response.setHeader(headerKey, contentDisponsition);

			OutputStream outStream = response.getOutputStream();
			outStream.write(rEntity.getBody(), 0, rEntity.getBody().length);
			outStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.debug( "download done" );
	}
	/**
	 * App Manager uploadFile
	 *
	 * @param formString
	 * @return
	 */
	@RequestMapping(value = "uploadFile")
	@ResponseBody
	public String uploadFile(@RequestParam(value = "file",required = false) MultipartFile file,
			@RequestParam(value = "form") String formString, HttpSession session) {
		logger.debug("APP update start --Class: " + this.getClass().getName() + "--method: " + Thread.currentThread().getStackTrace()[1].getMethodName());
		logger.debug("formString= " + formString);
		String ret ="";

		AppForm appForm = JsonMapperUtils.readValue(formString, AppForm.class);
		List<String> errorList = new ArrayList<String>();
		// Unauthorized Exception
		JsonResult jr = appService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			return writeErrorList(jr, errorList);
		}
		try {
			String fileName = appForm.getVerFilePath();
			ret = appService.upload(appForm, file, fileName, false, session);
		} catch (Exception e) {
			logger.error(e.getMessage());
			ret = JsonMapperUtils.getJsonString(Constants.STATUS_ERROR, null, e.getMessage());
		}
		logger.debug("APP update stop");
		return ret;
	}

    /**
     * App Manager Delete
     *
     * @param id
     * @return
     */
    @RequestMapping(value = "delete")
    @ResponseBody
    public String delete(@RequestParam(value = "id") String id, HttpSession session) {
        logger.debug( "APP delete start, id=" + id );
        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            return writeErrorString( jr );
        }
        // App Delete By AppId。
        String ret = appService.delete( id, session );
        logger.debug( "APP delete stop" );
        return JsonMapperUtils.getJsonString( ret, null, "" );
    }

	/**
	 * App Manager type
	 *
	 * @param
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "type")
	@ResponseBody
	public ModelAndView type(String type, HttpSession session) throws Exception{
		LinkedHashMap<String, Object> model = new LinkedHashMap<>();
        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            model.put( MODEL_ERRORS, jr.getData().toString() );
            return new ModelAndView( PageMapping.APP_TYPE.toString(), model );
        }

		  try {
	        	// Get App Option
	            appService.getAppTypeOption( model, false, session );
	            appService.getDeviceTypeOption(model, false, session);
	        } catch ( Exception e ) {
	        	model.put( MODEL_ERRORS, e.getMessage() );
	            return new ModelAndView( PageMapping.APP_TYPE.toString(), model );
	        }

		return new ModelAndView( PageMapping.APP_TYPE.toString(), model );


	}

	/**
	 * Add Or Update Type/Device Type
	 *
	 * @param dicType
	 * @param dicValue
	 * @param dicLabel
	 * @return
	 */
	@RequestMapping(value = "saveType")
	@ResponseBody
	public String saveType(@RequestParam(value = "type", required = false) String dicType,
			@RequestParam(value = "typeValue", required = false) String dicValue,
			@RequestParam(value = "typeLabel", required = false) String dicLabel,
			HttpSession session) {
		String ret = "";
		// Unauthorized Exception
		JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
        	return JsonMapperUtils.getJsonString(jr.getStatus(), null, jr.getData());
        }
		try {
			ret = appService.saveDictionary(dicType, dicValue, dicLabel, session);
		} catch (Exception e) {
			logger.error(e.getMessage());
			ret = JsonMapperUtils.getJsonString(Constants.STATUS_ERROR, null, e.getMessage());
		}
		logger.debug("Add Or Update Type/Device Type stop");
		return ret;
	}

	/**
	 * check update info
	 * @param appForm
	 * @return
	 */
	private List<String> checkUpdateInfo(AppForm appForm, List<String> errorList, MultipartFile imageFile) {

		if(StringUtils.isEmpty(appForm.getAppId())){
			if (StringUtils.isEmpty(appForm.getAppDeviceTypeId())) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.APP_DEVICETYPE_IS_NOT_EMPTY));
			}
		}else {
			if(StringUtils.isEmpty(appForm.getVerFilePath())){
				errorList.add(MessageUtil.getPropertites(KeysConstants.APP_FILEPATH_IS_NOT_EMPTY));
			}
			if (StringUtils.isEmpty(appForm.getTypeId())) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.APP_TYPEID_IS_NOT_EMPTY));
			}
			if (appForm.getVerFilePath().length() > ApiParam.API_PARAM_VERFILEPATH_LENGTH) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.APP_FILEPATH_MAX_ERROR));
			}
			if (StringUtils.isEmpty(appForm.getAppAbstract().replace(" ", ""))) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.APP_ABSTRACT_IS_NOT_EMPTY));
			}
			if (appForm.getAppAbstract().length() > ApiParam.API_PARAM_APPABSTRACT_LENGTH) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.APP_ABSTRACT_MAX_ERROR));
			}
	        if(StringUtils.isEmpty(appForm.getAppName())) {
	        	errorList.add(MessageUtil.getPropertites(KeysConstants.APP_APPNAME_IS_NOT_EMPTY));
	        }
	        if(imageFile == null && StringUtils.isEmpty(appForm.getImagePath())) {
	        	errorList.add(MessageUtil.getPropertites(KeysConstants.APP_IMAGRPATH_IS_NOT_EMPTY));
	        }
		}

		return errorList;
	}
}