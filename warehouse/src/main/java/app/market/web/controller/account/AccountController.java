/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.controller.account;

import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import app.market.LogUtil;
import app.market.utils.Md5Util;
import app.market.utils.constants.Constants;
import app.market.utils.datetime.DateTimeUtils;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.property.KeysConstants;
import app.market.utils.property.MessageUtil;
import app.market.utils.webservice.ApiParam;
import app.market.web.controller.BreadcrumbMapping;
import app.market.web.controller.PageMapping;
import app.market.web.controller.SpringBaseController;
import app.market.web.form.account.AccountForm;
import app.market.web.services.account.AccountService;

/**
 *
 * @author Toyota
 *
 *         User Manager
 *
 */
@Controller
@RequestMapping(value = "account")
public class AccountController extends SpringBaseController {
	private static Logger logger = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private AccountService accountService;

	/**
	 * User Manager Init
	 *
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "")
	public ModelAndView init(HttpSession session) throws Exception {
		LinkedHashMap<String, Object> model = new LinkedHashMap<>();
		// Unauthorized Exception
		JsonResult jr = accountService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			model.put(MODEL_ERRORS, jr.getData().toString());
			return new ModelAndView(PageMapping.ACCOUNT_LIST.toString(), model);
		}
		try {
			accountService.getAuthorityList(model, true, session);

		} catch (Exception e) {
			LogUtil.printCatchLog(logger, e);
			e.printStackTrace();
		}
		return new ModelAndView(PageMapping.ACCOUNT_LIST.toString(), model);
	}

	/**
	 * User Manager modify
	 *
	 * @param userId
	 * @return
	 * @throws Exception
	 *
	 */
	@RequestMapping(value = "modify")
	@ResponseBody
	public ModelAndView modify(@RequestParam(value = "userId", required = false) String userId,
			@RequestParam(value = "isDetail", required = false) boolean isDetail, HttpSession session)
			throws Exception {
		logger.debug("Modify User, start search--Class: " + this.getClass().getName() + "--method: "
				+ Thread.currentThread().getStackTrace()[1].getMethodName());
		Map<String, Object> modal = new LinkedHashMap<String, Object>();
		// Unauthorized Exception
		JsonResult jr = accountService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			modal.put(MODEL_ERRORS, jr.getData().toString());
			return new ModelAndView(PageMapping.ACCOUNT_LIST.toString(), modal);
		}

		AccountForm acForm;
		String breadcrumb;
		String viewName;
		try {
			if (StringUtils.isNotEmpty(userId)) {
				// Search AccountForm By UserId
				acForm = accountService.searchFormById(userId, session);
				String createDate = DateTimeUtils.getDate(DateTimeUtils.DATE_FORMAT_YYYYMMDDHHMMSS_DASH,
						acForm.getCreateDate());
				String updateDate = DateTimeUtils.getDate(DateTimeUtils.DATE_FORMAT_YYYYMMDDHHMMSS_DASH,
						acForm.getUpdateDate());
				modal.put("updateDate", updateDate);
				modal.put("createDate", createDate);

				if (isDetail) {
					breadcrumb = BreadcrumbMapping.USER_DETAIL;
				} else {
					breadcrumb = BreadcrumbMapping.USER_MODIFY;
				}
			} else {
				acForm = new AccountForm();
				breadcrumb = BreadcrumbMapping.USER_INSERT;
			}
			// Get Account List
			acForm.getBreadcrumb().addAll(BreadcrumbMapping.getBreadcrumb(breadcrumb).getBreadcrumb());
		} catch (Exception e) {
			LogUtil.printCatchLog(logger, e);
			modal.put(MODEL_ERRORS, e.getMessage());
			return new ModelAndView(PageMapping.ACCOUNT_LIST.toString(), modal);
		}
		// Get Account List
		accountService.getAuthorityList(modal, false, session);
		modal.put("appInfo", acForm);
		if (isDetail) {
			viewName = PageMapping.ACCOUNT_DETAIL.toString();
		} else {
			viewName = PageMapping.ACCOUNT_MODIFY.toString();
		}

		logger.debug("Modify User, stop search");
		return new ModelAndView(viewName, modal);
	}

	/**
	 * User Manager Search
	 *
	 * @param draw
	 * @param length
	 * @param start
	 * @param sort
	 * @param mailAddress
	 * @param userName
	 * @param order
	 * @return
	 * @throws ParseException
	 */
	@RequestMapping(value = "search")
	@ResponseBody
	public String search(@RequestParam(value = "draw") String draw, @RequestParam(value = "length") int length,
			@RequestParam(value = "start") int start, @RequestParam(value = "orderColumn") String sort,
			@RequestParam(value = "keyWord") String keyWord, @RequestParam(value = "orderDir") String order,
			@RequestParam(value = "auId") String auId, @RequestParam(value = "createDate") String createDate,
			HttpSession session) throws ParseException {
		logger.debug("Start search list--Class: " + this.getClass().getName() + "--method: "
				+ Thread.currentThread().getStackTrace()[1].getMethodName() + keyWord);
		Map<String, Object> modal = new LinkedHashMap<String, Object>();
		// Unauthorized exception
		JsonResult jr = accountService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			return writeDataTableMap(jr, draw);
		}
		String ret = "";
		try {
			AccountForm form = new AccountForm();
			// Assign value To Form
			form.setDraw(draw);
			form.setKeyWord(keyWord);
			form.setAuId(auId);
			form.setOffset(start);
			form.setLimit(length);
			sort = ApiParam.UserQueryParamWeb.getFieldNameById(Integer.valueOf(sort));
			form.setSort(sort);
			form.setOrder(order);
			form.setIsDel("0");
			// Page Search
			ret = accountService.selectPaginationData(form, session);

			// Get Account List
			accountService.getAuthorityList(modal, true, session);
		} catch (Exception e) {
			LogUtil.printCatchLog(logger, e);
			e.printStackTrace();
		}
		logger.debug("Stop search list");
		return ret;
	}

	/**
	 * User Manager update and save
	 *
	 * @param formString
	 * @return
	 */
	@RequestMapping(value = "update")
	@ResponseBody
	public String update(@RequestParam(value = "form") String formString, HttpSession session)
			throws NoSuchAlgorithmException {
		logger.debug("Start save user--Class: " + this.getClass().getName() + "--method: "
				+ Thread.currentThread().getStackTrace()[1].getMethodName());
		String ret = "";
		List<String> errorList = new ArrayList<String>();

		// Unauthorized Exception
		JsonResult jr = accountService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			return writeErrorList(jr, errorList);
		}

		AccountForm form = JsonMapperUtils.readValue(formString, AccountForm.class);
		checkUpdateInfo(form, errorList);

		form.setIsDel("0");
		if (CollectionUtils.isEmpty(errorList)) {
			// Account Save
			if (StringUtils.isEmpty(form.getUserId())) {
				// MD5 encrypt
				String userPwMd = Md5Util.md5(form.getUserPw());
				form.setUserPw(userPwMd);
			}
			ret = accountService.save(form, session);
		} else {
			ret = JsonMapperUtils.getJsonString(Constants.STATUS_ERROR, null, errorList);
		}
		logger.debug("Stop save user");
		return ret;
	}

	/**
	 * User Manager Delete
	 *
	 */
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(@RequestParam(value = "id") String id, HttpSession session) {
		logger.debug("Delete user start, id=" + id);
		// Unauthorized Exception
		JsonResult jr = accountService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			return writeErrorString(jr);
		}
		// Account Delete By UserId
		String ret = accountService.delete(id, session);
		logger.debug("Delete user stop");
		return ret;
	}

	/**
	 * check userinfo
	 * @param errorList
	 *
	 */
	private List<String> checkUpdateInfo(AccountForm form, List<String> errorList) {


		/**
		 * @ can only have numbers、letters、underscores、dots、minus numbers. There is only
		 * one between @ and dots numbers、letters、underscores、dots、minus numbers,And two
		 * points can't be next to. The content after dots can only is
		 * letters、numbers,the length is less than 1 less than 7.
		 */
		String szReg = "^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.[a-zA-Z0-9]{2,6}$";
		Pattern pattern = Pattern.compile(szReg);
		Matcher matcher = pattern.matcher(form.getMailAddress());

		// Input Check
		if (StringUtils.isEmpty(form.getUserName().replace(" ", ""))) {
			errorList.add(MessageUtil.getPropertites(KeysConstants.USER_USERNAME_IS_NOT_EMPTY));
		}
		if (form.getUserName().length() > ApiParam.API_PARAM_USERNAME_LENGTH) {
			errorList.add(MessageUtil.getPropertites(KeysConstants.USER_USERNAME_MAX_ERROR));
		}
		// save Need password verification,update not Need password verification
		if (StringUtils.isEmpty(form.getUserId())) {
			if (form.getUserPw().indexOf(" ") != -1) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.USER_PASSWORD_IS_NOT_SPACES));
			}
			if (StringUtils.isEmpty(form.getUserPw())) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.USER_PASSWORD_IS_NOT_EMPTY));
			}
			if (form.getUserPw().length() > 0 && form.getUserPw().length() < ApiParam.API_PARAM_USERPASSMIN_LENGTH) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.USER_PASSWORD_ERROR));
			}
			if (form.getUserPw().length() > ApiParam.API_PARAM_USERPASSMAX_LENGTH) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.USER_PASSWORD_MAX_ERROR));
			}
			if (StringUtils.isEmpty(form.getReuserPw())) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.USER_REPASSWORD_IS_NOT_EMPTY));
			}
			if (form.getReuserPw().length() > 0 && !StringUtils.equals(form.getUserPw(), form.getReuserPw())) {
				errorList.add(MessageUtil.getPropertites(KeysConstants.USER_NEW_REPASSWORD_IS_NOT_EQUALS));
			}
		}

		if (StringUtils.isEmpty(form.getMailAddress())) {
			errorList.add(MessageUtil.getPropertites(KeysConstants.USER_MAILADDRESS_IS_NOT_EMPTY));
		}
		if (form.getMailAddress().length() > ApiParam.API_PARAM_MAILADDRESS_LENGTH) {
			errorList.add(MessageUtil.getPropertites(KeysConstants.USER_MAILADDRESS_MAX_ERROR));
		}
		if (!matcher.matches() && form.getMailAddress().length() > 0) {
			errorList.add(MessageUtil.getPropertites(KeysConstants.USER_MAILADDRESS_IS_NOT_EQUALS));
		}
		if (StringUtils.isEmpty(form.getAuId())) {
			errorList.add(MessageUtil.getPropertites(KeysConstants.AUTHORITY_ID_IS_NOT_EMPTY));
		}
		return errorList;
	}
}
