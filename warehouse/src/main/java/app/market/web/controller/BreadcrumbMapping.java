/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.controller;

import org.apache.commons.lang3.StringUtils;

import app.market.web.form.breadcrumb.BreadcrumbFrom;
import app.market.web.form.breadcrumb.BreadcrumbSubFrom;

public class BreadcrumbMapping {

	public static final String APP_DETAIL = "APP,APP_DETAIL";
	public static final String APP_DETAIL_MODIFY = "APP,APP_DETAIL,APP_DETAIL_MODIFY";
	public static final String APP_INSERT = "APP,APP_INSERT";
	public static final String APP_MODIFY = "APP,APP_MODIFY";
	public static final String USER_INSERT = "USER,USER_INSERT";
	public static final String USER_DETAIL = "USER,USER_DETAIL";
	public static final String USER_MODIFY = "USER,USER_MODIFY";
	public static BreadcrumbFrom getBreadcrumb(String name) {
		BreadcrumbFrom breadcrumb = new BreadcrumbFrom();
		String[] names = name.split(",");
		for (int i = 0; i < names.length; i++) {
			BreadcrumbSubFrom sub = BreadcrumbEnum.getBreadcrumbSubFrom(names[i]);
			breadcrumb.getBreadcrumb().add(sub);
			if (i == names.length - 1) {
				sub.setCurrent(true);
			}
		}
		return breadcrumb;
	}

	public enum BreadcrumbEnum {
		USER("USER", "User", "account"),
		USER_DETAIL("USER_DETAIL", "Detail", "url"),
		USER_INSERT("USER_INSERT", "Create", "url"),
		USER_MODIFY("USER_MODIFY", "Modify", "url"),
		APP("APP", "App", "app"),
		APP_DETAIL("APP_DETAIL", "Detail", "app/more"),
		APP_DETAIL_MODIFY("APP_DETAIL_MODIFY", "Modify", "url"),
		APP_MODIFY("APP_MODIFY", "Modify", "url"),
		APP_INSERT("APP_INSERT", "Create", "url");

		private String key;
		private String name;
		private String url;

		private BreadcrumbEnum(String key, String name, String value) {
			this.key = key;
			this.name = name;
			this.url = value;
		}

		public static BreadcrumbSubFrom getBreadcrumbSubFrom(String key) {
			for (BreadcrumbEnum c : BreadcrumbEnum.values()) {
				if (StringUtils.equalsIgnoreCase(c.getKey(), key)) {
					BreadcrumbSubFrom sub = new BreadcrumbSubFrom();
					sub.setName(c.getName());
					sub.setUrl(c.getUrl());
					return sub;
				}
			}
			return null;
		}

		public String getKey() {
			return key;
		}

		public void setKey(String key) {
			this.key = key;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}

	}

}
