/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import app.market.utils.datatable.DataTableMap;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;

/**
 * Base Controller
 *
 * @author Toyota
 */
public abstract class SpringBaseController implements HandlerExceptionResolver {

    private static Logger logger = LoggerFactory.getLogger( SpringBaseController.class );

    protected final static String MODEL_ERRORS = "modelErrors";

    @Override
    public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler,
            Exception ex) {
        Map<String, Object> model = new HashMap<String, Object>();
        if ( ex instanceof IOException ) {
            logger.info( ex.getMessage() );
            model.put( MODEL_ERRORS, "No permission to read the record of this application！" );
            return new ModelAndView( "other/500", model );

        } else if ( ex instanceof SQLException ) {
            logger.info( ex.getMessage() );
            model.put( MODEL_ERRORS, "The database error occurred." );
            return new ModelAndView( "other/500", model );

        } else if ( ex instanceof HttpClientErrorException ) {
            logger.info( ex.getMessage() );
            model.put( MODEL_ERRORS, ex.getMessage() );
            return new ModelAndView( PageMapping.LOGIN.toString(), model );

        } else if ( ex instanceof RuntimeException ) {
            logger.info( ex.getMessage() );
            model.put( MODEL_ERRORS, ex.getMessage() );
            return new ModelAndView( "other/500", model );

        } else if ( ex instanceof NullPointerException ) {
            logger.info( ex.getMessage() );
            model.put( MODEL_ERRORS, ex.getMessage() );
            return new ModelAndView( "other/500", model );

        } else if ( ex instanceof Exception ) {
            logger.info( ex.getMessage() );
            model.put( MODEL_ERRORS, "A server error occurred. Please contact the administrator!" );
           // model.put( MODEL_ERRORS, "Your session has time out, please log on again.");
            return new ModelAndView( "other/500", model );
        }
        return null;
    }

    protected String writeDataTableMap(JsonResult jr, String draw) {
        DataTableMap map = new DataTableMap( draw, null );
        map.setErrorMsg( jr.getStatus(), jr.getData().toString() );
        return JsonMapperUtils.writeValueAsString( map.getMapData() );
    }

    protected String writeErrorList(JsonResult jr, List<String> errorList) {
        errorList.add( jr.getData().toString() );
        return JsonMapperUtils.getJsonString( jr.getStatus(), null, errorList );
    }

    protected String writeErrorString(JsonResult jr) {
        return JsonMapperUtils.getJsonString( jr.getStatus(), null, jr.getData().toString() );
    }

}
