/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.controller;

public enum PageMapping {

    LOGIN {
        // login screen
        @Override
        public String toString() {
            return "index";
        }
    },
    MAIN {
        // main screen
        @Override
        public String toString() {
            return "main";
        }
    },
    MAINDEV {
        // mainDev screen
        @Override
        public String toString() {
            return "mainDev";
        }
    },
    APP {
        // app screen
        @Override
        public String toString() {
            return "app";
        }
    },
    ACCOUNT_LIST {
        // account list screen
        @Override
        public String toString() {
            return "account/list";
        }
    },
    ACCOUNT_DETAIL {
        // account detail screen
        @Override
        public String toString() {
            return "account/detail";
        }
    },
    ACCOUNT_MODIFY {
        // account modify screen
        @Override
        public String toString() {
            return "account/modify";
        }
    },
    ACCOUNT_REGISTER {
        // account register screen
        @Override
        public String toString() {
            return "account/register";
        }
    },
    APP_LIST {
        // application list screen
        @Override
        public String toString() {
            return "app/list";
        }
    },
    APP_TYPE {
        // application type screen
        @Override
        public String toString() {
            return "app/type";
        }
    },
    APP_MODIFY {
        // application modify screen
        @Override
        public String toString() {
            return "app/modify";
        }
    },
    M3_LIST {
        // app dev list screen
        @Override
        public String toString() {
            return "appDev/listDev";
        }
    },
    M3_LOCAL_LIST {
        // app dev local list screen
        @Override
        public String toString() {
            return "appDev/localListDev";
        }
    },
    M3_SEARCH {
        // app dev search screen
        @Override
        public String toString() {
            return "appDev/searchDev";
        }
    },
    M3_DETAIL {
        // app dev detail screen
        @Override
        public String toString() {
            return "appDev/detailDev";
        }
    },
    CREATE_APP {
        // add application screen
        @Override
        public String toString() {
            return "app/createApp";
        }
    },
    SAVE_APP_INFO {
        // save application information
        @Override
        public String toString() {
            return "app/saveAppInfo";
        }
    },
    CHECK_APP_INFO {
        // check application information
        @Override
        public String toString() {
            return "app/checkAppInfo";
        }
    },
    OTHER_404 {
        // 404 screen
        @Override
        public String toString() {
            return "other/404";
        }
    },
    OTHER_500 {
        // 500 screen
        @Override
        public String toString() {
            return "other/500";
        }
    },
    INIT_INFO {
        //
        @Override
        public String toString() {
            return "/app/initInfo";
        }
    },
    INIT_CHECK{
    	@Override
        public String toString() {
            return "/app/initCheck";
        }
    };

    public static String redirect(String pageId, String... args) {
        String url = "redirect:" + pageId;
        if ( args.length > 0 ) {
            url += "?";
            for (int i = 0; i < args.length; i++) {
                if ( i == 0 ) {
                    url = url + "p" + i + "=" + args[i];
                } else {
                    url = url + "&p" + i + "=" + args[i];
                }
            }
        }
        return url;
    }
}
