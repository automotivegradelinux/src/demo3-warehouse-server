/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.services.main.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.codehaus.jackson.type.JavaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import app.market.model.resource.Resource;
import app.market.utils.constants.Constants;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.rest.RestTemplateUtil;
import app.market.WebServiceClient;
import app.market.web.services.SpringBaseService;
import app.market.web.services.main.MainService;

@Service
public class MainServiceImpl extends SpringBaseService implements MainService {

    private static Logger logger = LoggerFactory.getLogger( MainServiceImpl.class );

    @Override
    public JsonResult validateAuthentication1(HttpSession session) {
        return super.validateAuthentication(session);
    }

    @Override
    public String selectMenuResourceByLoginId(HttpSession session) throws Exception {
        String path = "";
        try {
            RestTemplateUtil restTemplate = new RestTemplateUtil();
            String jsonStr = restTemplate.get( WebServiceClient.REST_RESOURCE_GET_MENU_RESOURCES_BY_LOGINID, session);
            JsonResult jr = JsonMapperUtils.readValue( jsonStr, JsonResult.class );
            if ( Constants.STATUS_SUCCESS == jr.getStatus() ) {
                JavaType javaType = JsonMapperUtils.getInstance().getTypeFactory().constructParametricType( ArrayList.class, Resource.class );
                List<Resource> list = JsonMapperUtils.readValue( jr.getData().toString(), javaType );
                for (Resource r : list) {
                    path += r.getAccessPath();
                }
            } else {
                throw new Exception( jr.getData().toString() );
            }
        } catch ( Exception e ) {
            logger.debug( this.getClass().getName() + ".selectMenuResourceByLoginId(token) failed." );
            logger.debug( e.getMessage() );
            super.exceptionHandler( e );
        }
        return path;
    }

}
