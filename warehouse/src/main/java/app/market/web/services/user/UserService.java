/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.services.user;

import javax.servlet.http.HttpSession;

import app.market.model.user.User;
import app.market.utils.json.JsonResult;
import app.market.web.form.user.UserForm;

public interface UserService {

    User selectCurrentUser(HttpSession session) throws Exception;

    User selectUserByUserName(String userName, String token, HttpSession session) throws Exception;

    String saveInfo(UserForm form, HttpSession session);

    String saveUser(UserForm form, HttpSession session);

    JsonResult validateAuthentication1(HttpSession session);
}
