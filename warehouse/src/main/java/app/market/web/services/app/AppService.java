/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.services.app;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import app.market.utils.json.JsonResult;
import app.market.web.form.app.AppForm;

public interface AppService {

	/**
	 * Authentication
	 *
	 * @return
	 */
    JsonResult validateAuthentication1(HttpSession session);

    /**
     * Search AppForm By AppId
     *
     * @param id
     * @return
     * @throws Exception
     */
    AppForm searchFormById(String id, HttpSession session) throws Exception;

    /***
     * Search AppForm By AppIdCustom
     * @param id
     * @return
     * @throws Exception
     */
    AppForm searchFormByCustomId(String id, HttpSession session) throws Exception;

   /**
    * Page Search
    *
    * @param form
    * @return
    */
    String selectPaginationData(AppForm form, HttpSession session);

    /**
     * App Delete
     *
     * @param id
     * @return
     */
    String delete(String id, HttpSession session);

    /**
     * App Save
     *
     * @param form
     * @return
     */
    String save(AppForm form, HttpSession session);

    /**
     * Get App　type  Option
     *
     * @param model
     * @param hasSpace
     * @throws Exception
     */
    void getAppTypeOption(Map<String, Object> model, boolean hasSpace, HttpSession session) throws Exception;

    /**
     * Get App devicetype Option
     *
     * @param model
     * @param hasSpace
     * @throws Exception
     */
	void getDeviceTypeOption(Map<String, Object> model, boolean hasSpace, HttpSession session) throws Exception;

    /**
     * App Upload
     *
     * @param form
     * @param file
     * @return
     */
    String upload(AppForm form, MultipartFile file, String fileName, boolean isImage, HttpSession session);

    /**
     * App Version Save
     *
     * @param form
     * @return
     */
    String saveVersion(AppForm form, HttpSession session);

    /**
     * App Download
     *
     * @param form
     * @return
     */
    ResponseEntity<byte[]> download(AppForm form, HttpSession session);

	void getIsPublicOption(Map<String, Object> model, boolean hasSpace, HttpSession session) throws Exception;

	String saveDictionary(String dicType, String dicValue, String dicLabel, HttpSession session);
}
