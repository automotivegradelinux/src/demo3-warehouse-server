/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.services.account.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.alibaba.fastjson.JSONObject;

import app.market.WebServiceClient;
import app.market.model.user.User;
import app.market.utils.Utils;
import app.market.utils.constants.Constants;
import app.market.utils.datetime.DateTimeUtils;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.property.KeysConstants;
import app.market.utils.property.MessageUtil;
import app.market.utils.property.Option;
import app.market.utils.property.PropertyUtil;
import app.market.utils.rest.RestTemplateUtil;
import app.market.utils.webservice.ApiParam;

import app.market.web.controller.ControllerMapping;
import app.market.web.form.account.AccountForm;
import app.market.web.services.SpringBaseService;
import app.market.web.services.account.AccountService;

@Service
public class AccountServiceImpl extends SpringBaseService implements AccountService {
	private static Logger logger = LoggerFactory.getLogger(AccountServiceImpl.class);

	/**
	 * Authentication
	 *
	 * @return
	 */
	@Override
	public JsonResult validateAuthentication1(HttpSession session) {
		return super.validateAuthentication(session);
	}

	 /**
     *
     * Page Search
     *
     * @param form
     * @return
     */
	@Override
	public String selectPaginationData(AccountForm form, HttpSession session) {
		logger.debug("account list start --Class: " + this.getClass().getName() + "--method: "
				+ Thread.currentThread().getStackTrace()[1].getMethodName() + form.getUserId());
		String date = null;
		if(form.getCreateDate() != null){
			 date = DateTimeUtils.getDate(DateTimeUtils.DATE_FORMAT_YYYYMMDD,form.getCreateDate());
		}
        Map<String, Object> param = new HashMap<>();
        param.put( ApiParam.API_USER_PARAM_USERNAME, form.getUserName() );
        param.put( ApiParam.API_USER_PARAM_AUID, form.getAuId() );
        param.put( ApiParam.API_USER_PARAM_MAIL, form.getMailAddress() );
        param.put( ApiParam.API_USER_PARAM_CREATEDATE, date );
        param.put( ApiParam.API_PARAM_ORDER, form.getOrder() );
        param.put( ApiParam.API_PARAM_SORT, form.getSort());
        param.put( ApiParam.API_PARAM_OFFSET, form.getOffset() );
        param.put( ApiParam.API_PARAM_LIMIT, form.getLimit() );
        param.put( ApiParam.API_APP_PARAM_KEYWORD, form.getKeyWord() );
        return super.selectPaginationData( WebServiceClient.REST_USER_SELECTPAGINATIONDATABYEXAMPLE, form, null, param, session);
	}

	/**
     * Account Save
     *
     * @param form
     * @return
     */
	@Override
	public String save(AccountForm form, HttpSession session) {
		logger.debug("save account start");
		User record = new User();
		String ret = JsonMapperUtils.getJsonString(Constants.STATUS_SUCCESS, null, null);
		List<String> msgList = new ArrayList<String>();
		try {
			Utils.reflect(form, record);
			RestTemplateUtil restTemplate = new RestTemplateUtil();
			// Resttemplate Operation
			String jsonStr = restTemplate.post(WebServiceClient.REST_USER, record, session);

			JSONObject json = JsonMapperUtils.getJsonObject(jsonStr);
			String userId = json.getString(ApiParam.API_RESPONSE_APPID);
			form.setUserId(userId);

			msgList.add(MessageUtil.getPropertites(KeysConstants.USER_SAVE_IS_SUCCESS));
			ret = JsonMapperUtils.getJsonString(Constants.STATUS_SUCCESS, ControllerMapping.ACCOUNT.toString(), msgList);
		} catch (HttpClientErrorException e) {
			if (e.getStatusCode().value() == Constants.STATUS_BAD_REQUEST) {
				msgList.add(MessageUtil.getPropertites(KeysConstants.INVALID_QUERYPARAM));
				ret = JsonMapperUtils.getJsonString(Constants.STATUS_ERROR, null, msgList);
			}
			if (e.getStatusCode().value() == Constants.STATUS_ALREADY_EXISTS) {
				msgList.add(MessageUtil.getPropertites(KeysConstants.RESOURCE_ALREADY_EXISTS));
				ret = JsonMapperUtils.getJsonString(Constants.STATUS_ALREADY_EXISTS, null, msgList);
			}
			return JsonMapperUtils.getJsonString(Constants.STATUS_ERROR, null, msgList);
		} catch (Exception e) {
			ret = exceptionHandler(e, msgList);
		}
		logger.debug("save account stop");
		return ret;
	}

	/**
     * Account Delete
     *
     * @param id
     * @return
     */
	@Override
	public String delete(String id, HttpSession session) {
		logger.debug("delete account start");
		List<String> msgList = new ArrayList<String>();
        try {
            RestTemplateUtil restTemplate = new RestTemplateUtil();
            // Rest template operation
            restTemplate.delete( WebServiceClient.REST_USER_BY_USERID, id, session );
        } catch (HttpClientErrorException e ) {
        	if(e.getStatusCode().value() == Constants.STATUS_ALREADY_EXISTS){
        		msgList.add(MessageUtil.getPropertites(KeysConstants.USER_NOT_DELETE));
				return JsonMapperUtils.getJsonString(Constants.STATUS_ALREADY_EXISTS, null, msgList);
        	}
        	return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, "" );
        } catch (Exception e) {
			e.printStackTrace();
			return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, "" );
		}
        logger.debug("delete account stop ");
        return JsonMapperUtils.getJsonString( Constants.STATUS_SUCCESS, null, "" );
	}

	/**
     * Get Account List
     *
     * @param model
     * @param hasSpace
     * @throws Exception
     */
    @Override
    public void getAuthorityList(Map<String, Object> model,boolean hasSpace, HttpSession session) throws Exception {
        logger.debug( "getAccounList" );
        try{
		RestTemplateUtil restTemplate = new RestTemplateUtil();
        List<Option> list = new ArrayList<Option>();
        // Resttemplate operation
        String jsonStr = restTemplate.get( WebServiceClient.REST_AUTHORITY_GET_LIST_OPTION, session );
        @SuppressWarnings("unchecked")
		List<Option> typelist = (List<Option>)JsonMapperUtils.getJsonList(jsonStr, Option.class);

        if ( hasSpace ) {
            Option option = new Option( PropertyUtil.DEFAULLABEL, "" );
            list.add( option );
        }
        for (Option type : typelist) {
            Option option1 = new Option( type.getLabel(), type.getValue().toString() );
            list.add( option1 );
        }
        model.put( "authorityList", list );
        } catch (Exception e) {
			logger.debug(e.getMessage());
			model.put("authorityList", new ArrayList<Option>());
		}
    }


    /**
     * Search AccountForm By UserId
     *
     * @param id
     * @return
     * @throws Exception
     */
	@Override
	public AccountForm searchFormById(String id, HttpSession session) throws Exception {
		AccountForm acForm = new AccountForm();
		try {
			RestTemplateUtil restTemplate = new RestTemplateUtil();
			// Rest template Operation
			String jsonStr = restTemplate.get(WebServiceClient.REST_USER_BY_USERID, session, new String[] { id });
			User user = JsonMapperUtils.readValue( jsonStr, User.class );
			Utils.reflect(user, acForm);
		} catch (Exception e) {
			logger.debug(this.getClass().getName() + ".searchFormById(id) failed.");
			logger.debug(e.getMessage());
			super.exceptionHandler(e);
		}
		return acForm;
	}

}
