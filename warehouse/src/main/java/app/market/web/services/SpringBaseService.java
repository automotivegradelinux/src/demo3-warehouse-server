/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.client.HttpClientErrorException;

import com.alibaba.fastjson.JSONObject;

import app.market.WebServiceClient;
import app.market.model.app.App;
import app.market.utils.constants.Constants;
import app.market.utils.datatable.DataTableMap;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.property.KeysConstants;
import app.market.utils.property.MessageUtil;
import app.market.utils.rest.RestTemplateUtil;
import app.market.utils.webservice.ApiParam;

public abstract class SpringBaseService {

    protected String convertEmptyStr(Object obj) {
        if ( obj == null ) {
            return Constants.EMPTY_STRING_FALG;
        }
        if ( obj instanceof String ) {
            String s = (String) obj;
            return StringUtils.isEmpty( s ) ? Constants.EMPTY_STRING_FALG : s;

        } else if ( obj instanceof Integer ) {
            Integer i = (Integer) obj;
            return i == null ? Constants.EMPTY_STRING_FALG : i + "";

        } else {
            return obj.toString();
        }
    }

    protected JsonResult validateAuthentication(HttpSession session) {
        JsonResult jr = new JsonResult();
        RestTemplateUtil restTemplate = new RestTemplateUtil();
        try {
            String jsonStr = restTemplate.get( WebServiceClient.REST_TOKEN_VALIDATETOKENAUTHORTICATION, session, new String[] {} );
            jr = JsonMapperUtils.readValue( jsonStr, JsonResult.class );
            return jr;
        } catch ( Exception e ) {
            if ( e instanceof HttpClientErrorException ) {
                HttpClientErrorException hcee = (HttpClientErrorException) e;
                switch ( hcee.getStatusCode().value() ) {
                case Constants.STATUS_UNAUTHORIZED:
					if (updateAccessToken(jr, session)) {
						jr = validateAuthentication(session);
					} else {
						jr.setStatus(Constants.STATUS_UNAUTHORIZED);
						jr.setData(MessageUtil.getPropertites(KeysConstants.STATUS_UNAUTHORIZED));
					}
					break;
                case Constants.STATUS_TOO_MANY_CONNECTIONS:
                    jr.setStatus( Constants.STATUS_TOO_MANY_CONNECTIONS );
                    jr.setData( MessageUtil.getPropertites( KeysConstants.STATUS_TOO_MANY_CONNECTIONS ) );
                    break;
                case Constants.STATUS_FORBIDDEN:
                    jr.setStatus( Constants.STATUS_FORBIDDEN );
                    jr.setData( MessageUtil.getPropertites( KeysConstants.STATUS_FORBIDDEN ) );
                    break;
                default:
                    jr.setStatus( Constants.STATUS_ERROR );
                    jr.setData( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
                    break;
                }
            } else {
                jr.setStatus( Constants.STATUS_ERROR );
                jr.setData( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
            }
            return jr;
        }
    }

    /**
     * update acess token
     * @param jr
     * @param session
     * @return
     */
	private boolean updateAccessToken(JsonResult jr, HttpSession session) {
		boolean ret = false;
		RestTemplateUtil restTemplate = new RestTemplateUtil();
		try {
			String refreshToken = (String) session.getAttribute(Constants.SESSION_REFRESH_TOKEN);

			String jsonStr = restTemplate.get(WebServiceClient.REST_TOKEN, session, new String[] { refreshToken });
        	JSONObject json = JsonMapperUtils.getJsonObject(jsonStr);
        	String token = json.getString(ApiParam.API_RESPONSE_TOKEN);
        	//String refreshtoken = json.getString(ApiParam.API_RESPONSE_REFRESHTOKEN);
        	session.setAttribute( Constants.SESSION_TOKEN, token );
        	//session.setAttribute( Constants.SESSION_REFRESH_TOKEN, refreshtoken );
        	ret = true;
		} catch (HttpClientErrorException e) {
			switch (e.getStatusCode().value()) {
			case Constants.STATUS_UNAUTHORIZED:
				jr.setStatus(Constants.STATUS_UNAUTHORIZED);
				jr.setData(MessageUtil.getPropertites(KeysConstants.STATUS_UNAUTHORIZED));
				break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

//	private JsonResult updateAccessToken(JsonResult jr) {
//		RestTemplateUtil restTemplate = new RestTemplateUtil();
//		try {
//			String refreshToken = SpringWebUtil.getRequest().getParameter(ApiParam.API_RESPONSE_REFRESHTOKEN);
//			String jsonStr = restTemplate.get(WebServiceClient.REST_TOKEN, new String[] { refreshToken });
//			jr.setStatus( Constants.STATUS_REFRESH_TOKEN );
//			jr.setData( jsonStr);
//		} catch (HttpClientErrorException e) {
//			switch (e.getStatusCode().value()) {
//			case Constants.STATUS_UNAUTHORIZED:
//              jr.setStatus( Constants.STATUS_UNAUTHORIZED );
//              jr.setData( MessageUtil.getPropertites( KeysConstants.STATUS_UNAUTHORIZED ) );
//			break;
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return jr;
//	}
	/**
     * search start
     *
     * @param restURI
     * @param obj
     * @param model
     * @param params
     * @return
     */
    protected String selectPaginationData(String restURI, Object obj, Object model, Map<String, Object> params, HttpSession session) {
        Class<?> form = obj.getClass();
        List<Object> dataList = new ArrayList<Object>();
        DataTableMap map = null;
        List<String> errList = new ArrayList<String>();

		try {
			map = new DataTableMap(form.getMethod("getDraw", null).invoke(obj).toString(), dataList);

			RestTemplateUtil restTemplate = new RestTemplateUtil();
			String jsonStr = restTemplate.getByQueryParam(restURI, params, session);

			JSONObject json = JsonMapperUtils.getJsonObject(jsonStr);
			int counter = json.getInteger(Constants.PAGINATION_COUNTER);
			List<App> list = json.getObject(Constants.PAGINATION_DATA, List.class);

			map.setTotal(counter);
			dataList.addAll(list);
        } catch ( Exception e ) {
        	exceptionHandler( e, map );
        }
        return JsonMapperUtils.writeValueAsString( map.getMapData() );
    }

	protected void exceptionHandler(Exception e, DataTableMap map) {
		if (e instanceof HttpClientErrorException) {
			HttpClientErrorException hcee = (HttpClientErrorException) e;
			if (hcee.getStatusCode().value() == Constants.STATUS_UNAUTHORIZED) {
				map.setErrorMsg(Constants.STATUS_UNAUTHORIZED,
						MessageUtil.getPropertites(KeysConstants.STATUS_UNAUTHORIZED));
			} else if (hcee.getStatusCode().value() == Constants.STATUS_BAD_REQUEST) {
				map.setErrorMsg(Constants.STATUS_BAD_REQUEST, MessageUtil.getPropertites(KeysConstants.STATUS_BAD_REQUEST));
			} else {
				map.setErrorMsg(Constants.STATUS_UNAUTHORIZED, MessageUtil.getPropertites(KeysConstants.PROJECT_ERROR));
			}
		} else {
			// failed
			map.setErrorMsg(Constants.STATUS_ERROR, MessageUtil.getPropertites(KeysConstants.PROJECT_ERROR));
		}
	}

    protected String exceptionHandler(Exception e, List<String> msgList) {
        if ( e instanceof HttpClientErrorException ) {
            HttpClientErrorException hcee = (HttpClientErrorException) e;
            // unauthorized
            if ( hcee.getStatusCode().value() == Constants.STATUS_UNAUTHORIZED ) {
                msgList.add( MessageUtil.getPropertites( KeysConstants.STATUS_UNAUTHORIZED ) );
            } else {
                msgList.add( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
            }
            return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, msgList );
        } else {
            msgList.add( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
            return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, msgList );
        }
    }

    protected void exceptionHandler(Exception e, JsonResult jr) {
        jr.setStatus( Constants.STATUS_ERROR );
        if ( e instanceof HttpClientErrorException ) {
            HttpClientErrorException hcee = (HttpClientErrorException) e;
            // unauthorized
            if ( hcee.getStatusCode().value() == Constants.STATUS_UNAUTHORIZED ) {
                jr.setData( MessageUtil.getPropertites( KeysConstants.STATUS_UNAUTHORIZED ) );
            } else {
                jr.setData( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
            }
        } else {
            jr.setData( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
        }
    }

    protected void exceptionHandler(Exception e) throws Exception {
        if ( e instanceof HttpClientErrorException ) {
            HttpClientErrorException hcee = (HttpClientErrorException) e;
            if ( hcee.getStatusCode().value() == Constants.STATUS_UNAUTHORIZED ) {
                throw new Exception( MessageUtil.getPropertites( KeysConstants.STATUS_UNAUTHORIZED ) );
            } else {
                throw new Exception( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
            }
        } else {
            throw new Exception( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
        }
    }

    public void initPageList(Map<String, Object> model, boolean hasSpace) throws Exception {
        // TODO Auto-generated method stub

    }
}
