/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.services.login.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import com.alibaba.fastjson.JSONObject;

import app.market.utils.constants.Constants;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.property.KeysConstants;
import app.market.utils.property.MessageUtil;
import app.market.utils.rest.RestTemplateUtil;
import app.market.utils.webservice.ApiParam;
import app.market.WebServiceClient;
import app.market.web.controller.ControllerMapping;
import app.market.web.services.login.LoginService;

@Service
public class LoginServiceImpl implements LoginService {

    //private static Logger logger = LoggerFactory.getLogger( LoginServiceImpl.class );

    @Override
    public String loginUser(String loginId, String userPw, HttpSession session) {
    	RestTemplateUtil restTemplate = new RestTemplateUtil();
    	ResponseEntity<String> rEntity;

    	List<String> errList = new ArrayList<String>();
		try {
	        JSONObject postData = new JSONObject();
	        postData.put(ApiParam.API_APP_PARAM_LOGINID, loginId);
	        postData.put(ApiParam.API_APP_PARAM_PASSWORD, userPw);
			rEntity = restTemplate.Post(WebServiceClient.REST_USER_SELECTLOGINUSER, postData, session);

//        	JSONObject json = JsonMapperUtils.getJsonObject(rEntity.getBody());
//        	String token = json.getString(ApiParam.API_RESPONSE_TOKEN);
//        	String refreshtoken = json.getString(ApiParam.API_RESPONSE_REFRESHTOKEN);
        	return JsonMapperUtils.getJsonString( Constants.STATUS_SUCCESS, ControllerMapping.MAIN.toString(), rEntity.getBody());
        } catch ( HttpClientErrorException e ) {
            if ( e.getStatusCode().value() == Constants.STATUS_UNAUTHORIZED ) {
                errList.add( MessageUtil.getPropertites( KeysConstants.LOGIN_LOGINID_IS_NOT_EXIST ) );
            }else if( e.getStatusCode().value() == Constants.STATUS_ERROR){
            	errList.add( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
            }else if( e.getStatusCode().value() == Constants.NOT_FOUND){
            	errList.add( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
            }
            return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, errList );
        } catch ( Exception e ) {
            errList.add( MessageUtil.getPropertites( KeysConstants.USER_LOGINID_IS_FAILED ) );
            return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, errList );
        }
    }

    @Override
    public int deleteUserById(int id) {
        // TODO Auto-generated method stub
        return 0;
    }

}
