/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.services.user.impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import app.market.WebServiceClient;
import app.market.model.user.User;
import app.market.utils.Utils;
import app.market.utils.constants.Constants;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.property.KeysConstants;
import app.market.utils.property.MessageUtil;
import app.market.utils.rest.RestTemplateUtil;
import app.market.web.form.user.UserForm;
import app.market.web.services.SpringBaseService;
import app.market.web.services.user.UserService;

@Service
public class UserServiceImpl extends SpringBaseService implements UserService {

    private static Logger logger = LoggerFactory.getLogger( UserServiceImpl.class );

    @Override
    public User selectCurrentUser(HttpSession session) throws Exception {
        try {
            RestTemplateUtil restTemplate = new RestTemplateUtil();
            String token = (String) session.getAttribute(Constants.SESSION_TOKEN);
            String jsonStr = restTemplate.get( WebServiceClient.REST_USER_SELECT_CURRENT, session, token );
            JsonResult jr = JsonMapperUtils.readValue( jsonStr, JsonResult.class );
            if ( Constants.STATUS_SUCCESS == jr.getStatus() ) {
                return JsonMapperUtils.readValue( jr.getData().toString(), User.class );
            }
            return null;
        } catch ( Exception e ) {
            logger.debug( this.getClass().getName() + ".selectCurrentUser() failed" );
            logger.debug( e.getMessage() );
            super.exceptionHandler( e );
        }
        return null;
    }

    @Override
    public User selectUserByUserName(String userName, String token, HttpSession session) throws Exception {
        try {
            RestTemplateUtil restTemplate = new RestTemplateUtil();
            String jsonStr = restTemplate.get( WebServiceClient.REST_USER_BY_USERNAME, session, userName );
            JsonResult jr = JsonMapperUtils.readValue( jsonStr, JsonResult.class );
            if ( Constants.STATUS_SUCCESS == jr.getStatus() ) {
                return JsonMapperUtils.readValue( jr.getData().toString(), User.class );
            }
            return null;
        } catch ( Exception e ) {
            super.exceptionHandler( e );
        }
        return null;
    }

    @Override
    public String saveUser(UserForm form, HttpSession session) {
        User user = new User();
        List<String> errList = new ArrayList<String>();
        try {
            Utils.reflect( form, user );
            user.setToken( form.getToken() );
            user.setIsDel( "0" );
            RestTemplateUtil restTemplate = new RestTemplateUtil();
            String jsonStr = restTemplate.post( WebServiceClient.REST_USER, user, session );
            JsonResult jr = JsonMapperUtils.readValue( jsonStr, JsonResult.class );
            if ( jr.getStatus() == Constants.STATUS_ERROR ) {
                errList.add( jr.getData().toString() );
                return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, errList );
            }
            errList.add( MessageUtil.getPropertites( KeysConstants.USER_IS_SUCCESS ) );
        } catch ( HttpClientErrorException e ) {
            // unauthorized
            if ( e.getStatusCode().value() == Constants.STATUS_UNAUTHORIZED ) {
                errList.add( MessageUtil.getPropertites( KeysConstants.STATUS_UNAUTHORIZED ) );
            } else {
                errList.add( MessageUtil.getPropertites( KeysConstants.PROJECT_ERROR ) );
            }
            return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, errList );
        } catch ( Exception e ) {
            errList.add( MessageUtil.getPropertites( KeysConstants.USER_SAVE_IS_FAILED ) );
            return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, errList );
        }
        return JsonMapperUtils.getJsonString( Constants.STATUS_SUCCESS, null, errList );
    }

    @Override
    public String saveInfo(UserForm form, HttpSession session) {
        List<String> msgList = new LinkedList<String>();
        try {
            User user = new User();
            Utils.reflect( form, user );
            user.setToken( form.getToken() );
            RestTemplateUtil restTemplate = new RestTemplateUtil();
            String jsonStr = restTemplate.post( WebServiceClient.REST_USER, user, session );
            JsonResult jr = JsonMapperUtils.readValue( jsonStr, JsonResult.class );
            if ( jr.getStatus() == Constants.STATUS_ERROR ) {
                msgList.add( jr.getData().toString() );
                return JsonMapperUtils.getJsonString( Constants.STATUS_ERROR, null, msgList );
            }
            msgList.add( MessageUtil.getPropertites( KeysConstants.USER_INFO_CHANGE_SUCCESS ) );
        } catch ( Exception e ) {
            logger.debug( this.getClass().getName() + ".info() failed." );
            logger.debug( e.getMessage() );
            super.exceptionHandler( e, msgList );
        }
        return JsonMapperUtils.getJsonString( Constants.STATUS_SUCCESS, null, msgList );
    }

    @Override
    public JsonResult validateAuthentication1(HttpSession session) {
        return super.validateAuthentication(session);
    }

}
