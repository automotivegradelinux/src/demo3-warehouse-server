/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.form.app;

import java.util.Date;

import app.market.web.form.PaginationForm;

public class AppForm extends PaginationForm {

    private static final long serialVersionUID = -6199786133496150679L;

    private String appId;

    private String appName;

    private long appSize;

    private String appAbstract;

    private String typeId;

    private String typeName;

    private String developer;

    private String developerName;

    private String verFilePath;

    private String versionName;

    private String appVersionId;

    private Date createDate;

    private Date updateDate;

    private String isDel;

	private String hashcode;

    private String appIsPublic;

    private String appDeviceTypeId;

    private String appDeviceTypeName;

    private String keyWord;

    private String imagePath;

    private String appIdCustom;//config.xml中のid

    private boolean updateFlag;

    private boolean systemApp;

	public String getAppIdCustom() {
		return appIdCustom;
	}

	public void setAppIdCustom(String appIdCustom) {
		this.appIdCustom = appIdCustom;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getAppDeviceTypeName() {
		return appDeviceTypeName;
	}

	public void setAppDeviceTypeName(String appDeviceTypeName) {
		this.appDeviceTypeName = appDeviceTypeName;
	}

    public String getAppDeviceTypeId() {
		return appDeviceTypeId;
	}

	public void setAppDeviceTypeId(String appDeviceTypeId) {
		this.appDeviceTypeId = appDeviceTypeId;
	}

    public String getAppIsPublic() {
		return appIsPublic;
	}

	public void setAppIsPublic(String appIsPublic) {
		this.appIsPublic = appIsPublic;
	}

	public String getAppId() {
        return appId;
    }

    public String getVerFilePath() {
		return verFilePath;
	}

	public void setVerFilePath(String verFilePath) {
		this.verFilePath = verFilePath;
	}

	public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public long getAppSize() {
		return appSize;
	}

	public void setAppSize(long l) {
		this.appSize = l;
	}

	public String getAppAbstract() {
        return appAbstract;
    }

    public void setAppAbstract(String appAbstract) {
        this.appAbstract = appAbstract;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer;
    }

    public String getDeveloperName() {
        return developerName;
    }

    public void setDeveloperName(String developerName) {
        this.developerName = developerName;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }

	public String getHashcode() {
		return hashcode;
	}

	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}

	public String getAppVersionId() {
		return appVersionId;
	}

	public void setAppVersionId(String appVersionId) {
		this.appVersionId = appVersionId;
	}

	public boolean isUpdateFlag() {
		return updateFlag;
	}

	public void setUpdateFlag(boolean updateFlag) {
		this.updateFlag = updateFlag;
	}

	public boolean isSystemApp() {
		return systemApp;
	}

	public void setSystemApp(boolean systemApp) {
		this.systemApp = systemApp;
	}
}
