/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.controllerDev.appDev;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpException;
import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.type.JavaType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSONObject;

import app.market.model.resource.Resource;
import app.market.model.user.User;
import app.market.utils.constants.Constants;
import app.market.utils.datetime.DateTimeUtils;
import app.market.utils.json.JsonMapperUtils;
import app.market.utils.json.JsonResult;
import app.market.utils.webservice.ApiParam;
import app.market.web.controller.BreadcrumbMapping;
import app.market.web.controller.PageMapping;
import app.market.web.controller.SpringBaseController;
import app.market.web.form.app.AppForm;
import app.market.web.services.app.AppService;
import app.market.web.services.main.MainService;
import app.market.web.services.user.UserService;


/**
 *
 * @author Toyota
 *
 * App Manager
 */
@Controller
@RequestMapping(value = "appDev")
public class AppControllerDev extends SpringBaseController {
	private static Logger logger = LoggerFactory.getLogger( AppControllerDev.class );

    @Autowired
    private AppService appService;

    @Autowired
    private MainService mainService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "")
    public ModelAndView init(@RequestParam(value = "token", required = false) String token, HttpSession session) throws Exception {
        logger.debug( "APPinit--Class: " + this.getClass().getName() + "--method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() );
        LinkedHashMap<String, Object> model = new LinkedHashMap<>();
        // Unauthorized Exception
		JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            model.put( MODEL_ERRORS, jr.getData().toString() );
            return new ModelAndView( PageMapping.M3_LIST.toString(), model );
        }
        // Search User name
        if(!StringUtils.isEmpty(token)){
			User user = userService.selectCurrentUser(session);
        	model.put( "username", user.getUserName() );
        	model.put( "userid",user.getUserId() );
        	model.put( "auid", user.getAuId());
        }
		model.put("menuPathString", mainService.selectMenuResourceByLoginId(session));
        return new ModelAndView( PageMapping.M3_LIST.toString(), model );
    }

    /**
     * App Manager Detail
     * @param appId
     * @param token
     * @param isListDev
     * @param appStatus //button word(install:0,update:2,launch:1)
     * @param session
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "detail")
    @ResponseBody
    public ModelAndView detail(@RequestParam(value = "appId", required = false) String appId
    		, @RequestParam(value = "token", required = false) String token
    		, @RequestParam(value = "isListDev") boolean isListDev
    		, @RequestParam(value = "appStatus") String appStatus
    		, HttpSession session) throws Exception {
        logger.debug( "APP modify--Class: " + this.getClass().getName() + "--method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() );

        Map<String, Object> model = new LinkedHashMap<String, Object>();

        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            model.put( MODEL_ERRORS, jr.getData().toString() );
            return new ModelAndView( PageMapping.M3_DETAIL.toString(), model );
        }
        AppForm apForm = new AppForm();
        try {
        	// Search AppForm By AppId
            apForm = appService.searchFormById(appId, session);
            apForm.getBreadcrumb().addAll( BreadcrumbMapping.getBreadcrumb(BreadcrumbMapping.APP_DETAIL).getBreadcrumb() );
        } catch ( Exception e ) {
            model.put( MODEL_ERRORS, e.getMessage() );
            return new ModelAndView( PageMapping.M3_DETAIL.toString(), model );
        }

        // Get App Option
        appService.getAppTypeOption( model, true, session);
        String date = DateTimeUtils.getDate(DateTimeUtils.DATE_FORMAT_YYYYMMDD, apForm.getCreateDate());
        model.put( "appInfo", apForm );
        model.put( "date", date);
        model.put( Constants.TOKEN_AUTHORIZATION,token);
        model.put( "isListDev", isListDev);
        model.put( "appStatus", appStatus);
        logger.debug( "APP modify" );
        return new ModelAndView( PageMapping.M3_DETAIL.toString(), model );
    }

    /**
	 * App Manager localApp
	 *
	 * @param formString
	 * @return
	 */
	@RequestMapping(value = "localApp")
	public ModelAndView localApp(HttpSession session){
		LinkedHashMap<String, Object> model = new LinkedHashMap<>();
        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            model.put( MODEL_ERRORS, jr.getData().toString() );
            return new ModelAndView( PageMapping.M3_LOCAL_LIST.toString(), model );
        }
		try {
			appService.getAppTypeOption( model, false, session );
			appService.getDeviceTypeOption(model, false, session);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return new ModelAndView( PageMapping.M3_LOCAL_LIST.toString(), model );
	}

    /**
	 * App Manager localApp
	 *
	 * @param formString
	 * @return
	 */
	@RequestMapping(value = "SearchDev")
	public ModelAndView SearchDev(HttpSession session){
		LinkedHashMap<String, Object> model = new LinkedHashMap<>();
        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            model.put( MODEL_ERRORS, jr.getData().toString() );
            return new ModelAndView( PageMapping.M3_SEARCH.toString(), model );
        }
		try {
			appService.getAppTypeOption( model, false, session );
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView( PageMapping.M3_SEARCH.toString(), model );
	}

	/**
     * App Manager Search
     *
     * @param draw
     * @param length
     * @param start
     * @param sort
     * @param appDeveloper
     * @param appTypeId
     * @param appName
     * @param order
     * @return
     * @throws Exception
     */
	@RequestMapping(value = "search")
	@ResponseBody
	public String search(
			@RequestParam(value = "draw") String draw,
			@RequestParam(value = "length") int length,
			@RequestParam(value = "start") int start,
			@RequestParam(value = "orderColumn") String sort,
			@RequestParam(value = "typeId") String appTypeId,
			@RequestParam(value = "orderDir") String order,
			@RequestParam(value = "deviceTypeId") String appDeviceTypeId,
			@RequestParam(value = "token", required = false) String token,
			@RequestParam(value = "keyWord") String keyWord, HttpSession session) throws Exception {
		logger.debug("search list start--Class: " + this.getClass().getName() + "--method: "
				+ Thread.currentThread().getStackTrace()[1].getMethodName() + keyWord);

		// Unauthorized Exception
		JsonResult jr = appService.validateAuthentication1(session);
		if (jr.getStatus() != Constants.STATUS_SUCCESS) {
			return writeDataTableMap(jr, draw);
		}

		// Assign value To AppForm
		AppForm form = new AppForm();
		form.setDraw(draw);
		form.setAppDeviceTypeId(appDeviceTypeId);
		form.setTypeId(appTypeId);
		form.setOffset(start);
		form.setLimit(length);
		form.setSort(ApiParam.API_PARAM_DEFAULT_SORT_NAME);
		form.setOrder(ApiParam.API_PARAM_VALUE_ORDER_DESC);
		form.setIsDel(ApiParam.API_PARAM_DEFAULT_DEL);
		form.setKeyWord(keyWord);
		User user = userService.selectCurrentUser(session);
		if (user == null) {
			form.setAppIsPublic(ApiParam.API_PARAM_DEFAULT_IS_PUBLIC);
		}
		// Page Search
		String ret = appService.selectPaginationData(form, session);

		logger.debug("search list stop");
		return ret;
	}

    @RequestMapping(value = "CheckUpdateInfoDev")
    @ResponseBody
    public String checkUpdateInfo(@RequestBody String localStr, HttpSession session) throws Exception{
    	logger.debug( "CheckDataDev--Class: " + this.getClass().getName() + "--method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() );
        // Unauthorized Exception
        JsonResult jr = appService.validateAuthentication1(session);
        if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
            return writeDataTableMap( jr, localStr );
        }
    	String id = "";
    	String localversion = "";
    	String dbVersion = "";
    	List<AppForm> appList = new ArrayList<>();

    	try {
			List<Object> localAppList = JSONObject.parseArray(localStr);//installed local app list

			for(Object item : localAppList){
					String localid = ((JSONObject) item).getString(Constants.CONFIG_APP_PARAM_ID);
					id =  localid.substring(0,localid.lastIndexOf(Constants.APP_ID_SEPARATOR));
					localversion =  ((JSONObject) item).getString(Constants.CONFIG_APP_PARAM_VERSION);
				    //check  weather the app in locallist is from server or not
					AppForm form = appService.searchFormByCustomId( id, session );
					//set local information for local app
					if(form == null){
						form = new AppForm();
						form.setAppName(((JSONObject) item).getString(Constants.CONFIG_APP_PARAM_NAME));
						//form.setImagePath(((JSONObject) item).getString(Constants.CONFIG_APP_PARAM_ICON));
						form.setImagePath(null);
						form.setAppAbstract(((JSONObject) item).getString(Constants.CONFIG_APP_PARAM_DESCRIPTION));
						form.setVersionName(localversion);
						form.setCreateDate(null);
						form.setAppDeviceTypeName(null);
						form.setAppDeviceTypeId(null);
						form.setUpdateFlag(false);
					}
					//check the systemAPP use localid
					if(localid.equals(Constants.APP_ID_LAUNCHER) || localid.equals(Constants.APP_ID_HOMESCREEN) || localid.equals(Constants.APP_ID_WAREHOUSE)){
						form.setSystemApp(true);
					}else{
						//check app's versionName for update
						dbVersion = form.getVersionName();
						if( !dbVersion.equals(localversion)){
							form.setUpdateFlag(true);
						}
						else{
							form.setUpdateFlag(false);
						}
					}
					appList.add(form);
			}
    	} catch (Exception e) {
			e.printStackTrace();
		}
		return JsonMapperUtils.writeValueAsString( appList );
    }

    @RequestMapping(value = "downloadenter")
	@ResponseBody
	public void downloadtest() {
		logger.debug( "downloadenter done" );
	}
    /**
     * App Manager Download File
     *
     * @param appId
     * @param typeId
     * @param filePath
     * @param response
     * @throws HttpException
     * @throws FileNotFoundException
     */
	@RequestMapping(value = "download")
	@ResponseBody
	public void download(@RequestParam(value = "appId", required = false) String appId,
			@RequestParam(value = "typeId", required = false) String typeId,
			@RequestParam(value = "filePath", required = false) String filePath,
			HttpServletResponse response, HttpSession session)
			throws HttpException, FileNotFoundException {
        logger.debug( "download--Class: " + this.getClass().getName() + "--method: "
                + Thread.currentThread().getStackTrace()[1].getMethodName() + filePath );
		// Assign value To AppForm
		AppForm form = new AppForm();
		form.setAppId(appId);
		form.setTypeId(typeId);
		form.setVerFilePath(filePath);

		try {
			ResponseEntity<byte[]> rEntity = appService.download(form, session);

			response.setContentType(rEntity.getHeaders().getContentType().toString());
			response.setContentLength((int) rEntity.getHeaders().getContentLength());
			String fileInsName = filePath.substring(filePath.lastIndexOf(Constants.PATH_SEPARATOR) + 1);
			response.addHeader(Constants.APP_HEADER_KEY, Constants.APP_HEADER_VALUE+fileInsName);

			OutputStream outStream = response.getOutputStream();
			outStream.write(rEntity.getBody(), 0, rEntity.getBody().length);
			outStream.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		logger.debug( "download done" );
	}
}


