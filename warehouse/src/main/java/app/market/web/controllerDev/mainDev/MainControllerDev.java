/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.web.controllerDev.mainDev;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import app.market.utils.constants.Constants;
import app.market.utils.json.JsonResult;
import app.market.web.controller.PageMapping;
import app.market.web.controller.SpringBaseController;
import app.market.web.services.main.MainService;
import app.market.web.services.user.UserService;

/**
 * login
 *
 * @author Toyota
 * @date 2017/10/10
 */
@Controller
@RequestMapping(value = "mainDev")
public class MainControllerDev extends SpringBaseController {

    @Autowired
    private MainService mainService;

    @Autowired
    private UserService userService;

    /**
     * init
     *
     * @param token
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "")
    public ModelAndView init(@RequestParam(value = "token", required = false) String token, @RequestParam(value = "port") String port, @RequestParam(value = "systemToken") String systemToken, @RequestParam(value = "installPath") String installPath,HttpSession session) throws Exception {
        LinkedHashMap<String, Object> modal = new LinkedHashMap<>();
        Map<String, Object> model = new LinkedHashMap<String, Object>();

        session.setAttribute(Constants.SESSION_PORT, port);
        session.setAttribute(Constants.SESSION_SYSTEM_TOKEN, systemToken);
        session.setAttribute(Constants.SESSION_INSTALL_PATH, installPath);

        // Not authentication
        if(StringUtils.isNotEmpty(token)){
        	JsonResult jr = mainService.validateAuthentication1(session);
        	if ( jr.getStatus() != Constants.STATUS_SUCCESS ) {
                modal.put( MODEL_ERRORS, jr.getData().toString() );
                return new ModelAndView( PageMapping.MAIN.toString(), modal );
            }
			model.put("username", userService.selectCurrentUser(session).getUserName());
			model.put("userid", userService.selectCurrentUser(session).getUserId());
			model.put("auid", userService.selectCurrentUser(session).getAuId());
			model.put("menuPathString", mainService.selectMenuResourceByLoginId(session));
        }

		model.put(Constants.TOKEN_AUTHORIZATION, token);
        return new ModelAndView( PageMapping.MAINDEV.toString(), model );
    }

}
