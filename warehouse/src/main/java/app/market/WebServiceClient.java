/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market;

import app.market.utils.webservice.WebServiceURI;

public class WebServiceClient {

    private static final String REST_SERVICE_URI = PropertyTool.getPropertites( "webservice_base_uri" );

    /** token expire */
    public static final String REST_TOKEN_VALIDATETOKENEXPIRE = REST_SERVICE_URI.concat( WebServiceURI.REST_TOKEN_VALIDATETOKENEXPIRE_LF );

    public static final String REST_TOKEN_VALIDATETOKENAUTHORTICATION = REST_SERVICE_URI.concat( WebServiceURI.REST_TOKEN_VALIDATETOKENAUTHORTICATION_LF );

    public static final String REST_TOKEN = REST_SERVICE_URI.concat( WebServiceURI.REST_TOKEN_LF );


    /** ▼▼▼▼▼▼▼▼▼▼ user ▼▼▼▼▼▼▼▼▼▼ */
    /** login */
    public static final String REST_USER_SELECTLOGINUSER = REST_SERVICE_URI.concat( WebServiceURI.REST_USER_SELECTLOGINUSER_LF );
    /** get user collection */
    public static final String REST_USER_SELECTPAGINATIONDATABYEXAMPLE = REST_SERVICE_URI.concat( WebServiceURI.REST_USER_SELECTPAGINATIONDATABYEXAMPLE_LF );
    /** get user information */
    public static final String REST_USER_BY_USERNAME = REST_SERVICE_URI.concat( WebServiceURI.REST_USER_BY_USERNAME_LF );
    /** get current user */
    public static final String REST_USER_SELECT_CURRENT = REST_SERVICE_URI.concat( WebServiceURI.REST_USER_SELECT_CURRENT_LF );
    /** save user */
    public static final String REST_USER = REST_SERVICE_URI.concat( WebServiceURI.REST_USER_LF );
    /** delete user */
    public static final String REST_USER_BY_USERID = REST_SERVICE_URI.concat( WebServiceURI.REST_USER_BY_USERID_LF );
    /** ▲▲▲▲▲▲▲▲▲▲ user ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ application ▼▼▼▼▼▼▼▼▼▼ */
    /** get application collection */
    public static final String REST_APP_COLLECTION_APP = REST_SERVICE_URI.concat( WebServiceURI.REST_APP_COLLECTION_APP_LF );
    /** get application information */
    public static final String REST_APP_INFO = REST_SERVICE_URI.concat( WebServiceURI.REST_APP_INFO_LF );

    public static final String REST_APP_INFO_PARM_ID = REST_SERVICE_URI.concat( WebServiceURI.REST_APP_INFO_PARM_ID_LF );
    /** get application authority */
    public static final String REST_APP_INFO_PARM_CUSTOMID = REST_SERVICE_URI.concat( WebServiceURI.REST_APP_INFO_PARM_CUSTOMID_LF );

    public static final String REST_APP_VERSION_INFO = REST_SERVICE_URI.concat( WebServiceURI.REST_APP_VERSION_INFO_LF );

    /** upload application */
    public static final String REST_APP_FILE = REST_SERVICE_URI.concat( WebServiceURI.REST_APP_FILE_LF );

    public static final String REST_APP_FILE_PARM_FILENAME_TYPEID_APPID = REST_SERVICE_URI.concat( WebServiceURI.REST_APP_FILE_PARM_FILENAME_TYPEID_APPID_LF );

    public static final String REST_APP_IMAGE = REST_SERVICE_URI.concat( WebServiceURI.REST_APP_IMAGE_LF );
    public static final String REST_APP_FILE_PARM_FILEPATH = REST_SERVICE_URI.concat( WebServiceURI.REST_APP_FILE_PARM_FILEPATH_LF );
    /** ▲▲▲▲▲▲▲▲▲▲ application ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ authority ▼▼▼▼▼▼▼▼▼▼ */
    /** get authority list */
    public static final String REST_AUTHORITY_GET_LIST_OPTION = REST_SERVICE_URI.concat( WebServiceURI.REST_AUTHORITY_GET_LIST_OPTION_LF );
    /** ▲▲▲▲▲▲▲▲▲▲ authority ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ resource ▼▼▼▼▼▼▼▼▼▼ */
    public static final String REST_RESOURCE_GET_MENU_RESOURCES_BY_LOGINID = REST_SERVICE_URI.concat( WebServiceURI.REST_RESOURCE_GET_MENU_RESOURCES_BY_LOGINID_LF );
    /** ▲▲▲▲▲▲▲▲▲▲ resource ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ dictionary ▼▼▼▼▼▼▼▼▼▼ */
    public static final String REST_DICTIONARY_COLLECTION = REST_SERVICE_URI.concat( WebServiceURI.REST_DICTIONARY_COLLECTION_LF );
    public static final String REST_DICTIONARY_INFO = REST_SERVICE_URI.concat( WebServiceURI.REST_DICTIONARY_INFO_LF );
    /** ▲▲▲▲▲▲▲▲▲▲ dictionary ▲▲▲▲▲▲▲▲▲▲ */
}
