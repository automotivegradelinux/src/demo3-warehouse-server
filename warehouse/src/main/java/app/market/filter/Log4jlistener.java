/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.filter;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class Log4jlistener implements ServletContextListener {

    public void contextDestroyed(ServletContextEvent servletcontextevent) {
        // System.getProperties().clear();
    }

    public void contextInitialized(ServletContextEvent servletcontextevent) {
        Properties prop = new Properties();
        try {
            prop.load( new InputStreamReader(
                    Log4jlistener.class.getClassLoader().getResourceAsStream( "config/properties.properties" ) ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
        Enumeration<Object> e = prop.keys();
        while ( e.hasMoreElements() ) {
            String key = e.nextElement().toString();
            String path = prop.getProperty( key );
            System.setProperty( key, path );
            File f = new File( path );
            if ( !f.getParentFile().exists() ) {
                f.getParentFile().mkdirs();
            }
        }
    }

}
