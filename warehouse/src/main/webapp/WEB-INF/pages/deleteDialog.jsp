<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<input type="hidden" id="deleteId" value="0"/>
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Delete</h4>
			</div>
			<div class="modal-body">Are you sure you want to delete the selected?</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" Style="background-Color:gray" data-dismiss="modal">NO&nbsp;</button>
				<button type="button" id="dialog-delete-confirm" class="btn btn-primary">Yes</button>
			</div>
		</div>
	</div>
</div>