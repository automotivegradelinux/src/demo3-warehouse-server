<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
String ippath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
%>

<!DOCTYPE html>
<html lang="en">
<script>
var width = window.screen.width;
</script>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>AGL App Warehouse</title>
	<link href="<%=basePath%>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<%=basePath%>/css/font-awesome.min.css" rel="stylesheet">
	<link href="<%=basePath%>/css/dataTables-1.10.16/dataTables.bootstrap4.css" rel="stylesheet">
	<link href="<%=basePath%>/css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="<%=basePath%>/css/style.css" rel="stylesheet">
</head>
<style type="text/css">
	#mainNav{
		z-index:0;
	}
  	body {
	    background: url('image/transp_bg.png');
	}
	.style {
	    min-height:500px;
	}
	.user-name-label{
		margin-top:auto;
	}
</style>
<div style="border-radius:3px;box-shadow: darkgrey 1px 1px 30px 10px">
<body class="" id="page-top">
	<input type="hidden" id="basePath" value="<%=basePath%>"/>
	<input type="hidden" id="ippath" value="<%=ippath%>"/>
	<input type="hidden" id="userId" value="${userid}"/>
	<input type="hidden" id="auId" value="${auid}"/>
	<input type="hidden" id="tokenAuthorization" value="${authorization_token}"/>
	<input type="hidden" id="refreshtokenAuthorization" value="${authorization_refreshtoken}"/>

<header>
  <!-- ########################### -->
  <div class="top container">
    <div class="row">
      <div class="col-md-12">
      <img class="toplogo" src="image/logo.png"/>
      </div>
    </div>
  </div>
  <!-- delete dialog -->
  <jsp:include page="deleteDialog.jsp" flush="true"></jsp:include>
  <!-- ############################## -->
    <div id="nav-wrapper">
	  	<nav class="navbar navbar-expand-lg navbar-dark container bg-gradient" id="mainNav">
			<c:if test="${fn:contains(menuPathString, 'account/list')}">
				<ul class="main-manager">
					<li class="main-manager-nav"><a id="AppManager" class="navbar-choosen" href="javascript:void(0)" onclick="Commons.showContent('<%=basePath%>/app/')">App</a><div class="main-manager-nav-line"></div></li>
					<li class="main-manager-nav"><a id="UserManager" class="navbar-default"  href="javascript:void(0)" onclick="Commons.showContent('<%=basePath%>/account/')">User</a><div class="main-manager-nav-line"></div></li>
	　　　　　　　　　　　　　　 <li class="main-manager-nav"><a id="TypeManager" class="navbar-default"  href="javascript:void(0)" onclick="Commons.showContent('<%=basePath%>/app/type')">Setting</a></li>
				</ul>
			</c:if>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<c:choose>
					<c:when test="${not empty username}">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<!-- <div class="dropdown show"> -->
									<div class="nav-link" style="color:rgba(255, 255, 255, 0.5);fond-size:20px;" id="dropdownMenuLink">
										<i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;<label class="user-name-label">${username}</label>
										<%-- ${menuPathString} contains--%>
									</div>
								<!-- </div> -->
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="modal" id="logout" href="javascript:void(0)"><i class="fa fa-fw fa-sign-out"></i>Logout</a>
							</li>
						</ul>
					</c:when>
					<c:otherwise>
					    <ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<a class="nav-link" data-toggle="modal" id="loginbut" href="javascript:void(0)"><i class="fa fa-fw fa-sign-in"></i>Login</a>
							</li>
						</ul>
					</c:otherwise>
				</c:choose>
	 		</div>
	 	</nav>
	</div>
</header>
<script src="<%=basePath%>/js/jquery/jquery.min.js"></script>
<script src="<%=basePath%>/js/jquery/easing/jquery.easing.min.js"></script>
<script src="<%=basePath%>/js/bootstrap-4.0.0-beta/popper/popper.min.js"></script>
<script src="<%=basePath%>/js/bootstrap-4.0.0-beta/bootstrap.min.js"></script>
<script src="<%=basePath%>/js/dataTables-1.10.16/jquery.dataTables.min.js"></script>
<script src="<%=basePath%>/js/dataTables-1.10.16/dataTables.bootstrap4.min.js"></script>
<script src="<%=basePath%>/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
<script src="<%=basePath%>/js/bootstrap-datetimepicker/bootstrap-datetimepicker.ja.js"></script>
<script src="<%=basePath%>/js/chart/Chart.bundle.js"></script>
<script src="<%=basePath%>/js/chart/utils.js"></script>
<script src="<%=basePath%>/js/jquery/jqLoading.min.js"></script>
<script src="<%=basePath%>/js/main.js"></script>
<script src="<%=basePath%>/js/common.js"></script>
<div id="page-content" class="index-page container">
	<div class="row">
		<div class="col-md-12 col-md-offset-1">
			<div id="htmlContent" class="style">
				<c:choose>
					<c:when test="${not empty modelErrors}">
						<div class="alert alert-danger" id="modalErrorArea" role="alert">
							&nbsp;${modelErrors}
						</div>
					</c:when>
					<c:otherwise>
					     <jsp:include page="app/list.jsp" flush="true"></jsp:include>
					</c:otherwise>
				</c:choose>
			</div><!-- htmlContent -->
			</div><!-- class="col-md-6 col-md-offset-1 -->
        </div><!-- row -->	
	    <!-- Scroll to Top Button-->
		<a class="scroll-to-top rounded" href="#page-top">
			<i class="fa fa-angle-up"></i>
		</a>
		<!-- Logout Modal-->
		<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
					</div>
					<div class="modal-body">Please make sure。</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<a class="btn btn-primary" href="javascript:void(0)" id="logout">&nbsp;&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;&nbsp;</a>
					</div>
				</div>
			</div>
		</div>
</div>

<!-- /.content-wrapper-->
<footer class="container ">
	<div class="row">
		<div class="col-md-8" style="margin:auto;">
			<p>Copyright (c) 2018-2020 TOYOTA MOTOR CORPORATION -V03.00.00</p>
		</div>
	</div>
</footer>
</body>
</div>
<script>
$(function(){
	$(".style").css({
		minHeight:($(window).height()- 300)
	})

	$('#AppManager').click(function(){
		var $AppManager = $("#AppManager");
		var $UserManager = $("#UserManager");
		var $TypeManager = $("#TypeManager");
		$AppManager.removeClass();
		$UserManager.removeClass();
		$TypeManager.removeClass();
		$AppManager.addClass("navbar-choosen");
		$UserManager.addClass("navbar-default");
		$TypeManager.addClass("navbar-default");
	});
});
(function($) {
	$('#logout').click(function(){
		debugger;
		$.ajax({
		    url: $('#basePath').val()+'/login/logout',
			cache: false,
			type: 'GET',
			success: function (data) {
			    window.location.href="<%=basePath%>";
			}
		});
	});
	
	$('#UserManager').click(function(){
		var $AppManager = $("#AppManager");
		var $UserManager = $("#UserManager");
		var $TypeManager = $("#TypeManager");
		$AppManager.removeClass();
		$UserManager.removeClass();
		$TypeManager.removeClass();
		$AppManager.addClass("navbar-default");
		$UserManager.addClass("navbar-choosen");
		$TypeManager.addClass("navbar-default");
	});
	
	$('#TypeManager').click(function(){
		var $AppManager = $("#AppManager");
		var $UserManager = $("#UserManager");
		var $TypeManager = $("#TypeManager");
		$AppManager.removeClass();
		$UserManager.removeClass();
		$TypeManager.removeClass();
		$AppManager.addClass("navbar-default");
		$UserManager.addClass("navbar-default");
		$TypeManager.addClass("navbar-choosen");
	});
})(jQuery);

$('#loginbut').click(function(){
	Commons.login();
});
</script>
</html>