<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
String header = request.getHeader("referer");
%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AGL</title>
	<link href="<%=basePath%>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<%=basePath%>/css/styles.css" rel="stylesheet">
	<link rel="shortcut icon" href="<%=basePath%>/img/favicon.ico" type="images/x-icon" />
	<link rel="icon" href="<%=basePath%>/img/favicon.ico" type="images/x-icon" />
	
	<style>
	body {
		width: 100%;
		background: #363D4D none repeat scroll 0% 0% !important;
		font-size: 16px;
		font-family: miranafont,"Hiragino Sans GB",STXihei,"Microsoft YaHei",SimSun,sans-serif;
		line-height: 1.82;
	}

	#error404 {
		color: #505C73;
		text-align: center;
		font-size: 22px;
	}
	
	p.errorfour {
		margin: 100px auto 40px;
		width: 504px;
		height: 215px;
		background: transparent url("<%=basePath%>/img/page404.png") no-repeat scroll 0% 0%;
	}
	
	p {
		margin: 20px 0px;
	}
	
	#error404 a {
		display: inline-block;
		margin-top: 80px;
		width: 200px;
		height: 50px;
		border: 1px solid #475266;
		border-radius: 5px;
		color: #505C73;
		text-align: center;
		font-size: 20px;
		line-height: 50px;
		transition: all 0.3s linear 0s;
	}
	
	#error404 a:hover {
		border: 1px solid #737F99;
		color: #737F99;
	}
	</style>
</head>
	<div id="error404">
		<p class="errorfour"></p>
		<p> Page not found 。</p>
		<a href="<%=basePath%>">Jump to the login</a>
	</div>
<body>

</body>
<script src="<%=header%>/js/jquery-1.11.1.min.js"></script>
</html>