<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
String ippath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
String remoteIp = request.getRemoteAddr();
String params = request.getQueryString();
%>

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>AGL App Warehouse</title>
	<link href="<%=basePath%>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<%=basePath%>/css/font-awesome.min.css" rel="stylesheet">
	<link href="<%=basePath%>/css/dataTables-1.10.16/dataTables.bootstrap4.css" rel="stylesheet">
	<link href="<%=basePath%>/css/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css" rel="stylesheet">
	<link href="<%=basePath%>/css/style.css" rel="stylesheet">
</head>
<style type="text/css">
	#mainNav{
		z-index:0;
	}
  	body {
	    background: url('image/transp_bg.png');
	}
	.style {
	    min-height:500px;
	}
	.size {
	    font-size:20px;
	}
</style>
<body class="test1" id="page-top" style="width:100%;">
	<input type="hidden" id="remoteIp" value="<%=remoteIp%>"/>
	<input type="hidden" id="params" value="<%=params%>"/>
	<input type="hidden" id="basePath" value="<%=basePath%>"/>
	<input type="hidden" id="ippath" value="<%=ippath%>"/>
	<input type="hidden" id="userId" value="${userid}"/>
	<input type="hidden" id="auId" value="${auid}"/>
	<input type="hidden" id="tokenAuthorization" value="${authorization_token}"/>
<header>
  <!-- delete dialog -->
  <jsp:include page="deleteDialog.jsp" flush="true"></jsp:include>
  <!-- ############################## -->

    <div id="nav-wrapper">
	  	<nav class="navbar navbar-expand-lg navbar-dark container bg-gradient" style="background-color: #29282E;;" id="mainNav">
			<div style="font-size:30px;color:#EAC100">Warehouse For AGL</div>
			<div id="backButton"></div>
			<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<c:choose>
					<c:when test="${not empty username}">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<div class="dropdown show">
									<div class="nav-link button-size" style="color:rgba(255, 255, 255, 0.5);color:#EAC100" id="dropdownMenuLink">
										<i class="fa fa-user-circle" aria-hidden="true"></i>&nbsp;<label>${username}</label>
										<%-- ${menuPathString} contains--%>
									</div>
								</div>
							</li>
							<li class="nav-item button-size">
								<a class="nav-link" data-toggle="modal" id="logout" href="javascript:void(0)" style="color:#EAC100"><i class="fa fa-fw fa-sign-out"></i>Logout</a>
							</li>
						</ul>
					</c:when>
					<c:otherwise>
					    <ul class="navbar-nav ml-auto button-size">
							<li class="nav-item">
								<a class="nav-link" data-toggle="modal" id="login" href="javascript:void(0)" style="color:#EAC100"><i class="fa fa-fw fa-sign-in"></i>Login</a>
							</li>
						</ul>
					</c:otherwise>
				</c:choose>
	 		</div>
	 	</nav>
	</div>
</header>
	<script src="<%=basePath%>/js/jquery/jquery.min.js"></script>
	<script src="<%=basePath%>/js/jquery/easing/jquery.easing.min.js"></script>
	<script src="<%=basePath%>/js/bootstrap-4.0.0-beta/popper/popper.min.js"></script>
	<script src="<%=basePath%>/js/bootstrap-4.0.0-beta/bootstrap.min.js"></script>
	<script src="<%=basePath%>/js/dataTables-1.10.16/jquery.dataTables.min.js"></script>
	<script src="<%=basePath%>/js/dataTables-1.10.16/dataTables.bootstrap4.min.js"></script>
	<script src="<%=basePath%>/js/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<script src="<%=basePath%>/js/bootstrap-datetimepicker/bootstrap-datetimepicker.ja.js"></script>
	<script src="<%=basePath%>/js/chart/Chart.bundle.js"></script>
	<script src="<%=basePath%>/js/chart/utils.js"></script>
	<script src="<%=basePath%>/js/jquery/jqLoading.min.js"></script>
	<script src="<%=basePath%>/js/main.js"></script>
	<script src="<%=basePath%>/js/common.js"></script>
    <script src="<%=basePath%>/js/AFB-websock.js"></script>
 	<script src="<%=basePath%>/js/AglSocket.js"></script>
 	<script src="<%=basePath%>/js/download.js"></script>
 	<script src="<%=basePath%>/js/deviceList.js"></script>	
<div id="page-content" class="index-page container test1" style="width:100%;height:100%;background-size:cover;">
	<div class="row">
		<div class="col-md-12 col-md-offset-1" style="overflow-x:hidden;">
			<div id="htmlContent" class="style">
				<c:choose>
					<c:when test="${not empty modelErrors}">
						<div class="alert alert-danger" id="modalErrorArea" role="alert">
							&nbsp;${modelErrors}
						</div>
					</c:when>
					<c:otherwise>
					    <jsp:include page="appDev/listDev.jsp" flush="true"></jsp:include>
					</c:otherwise>
				</c:choose>
			</div><!-- htmlContent -->
			</div><!-- class="col-md-6 col-md-offset-1 -->
        </div><!-- row -->	
	    <!-- Scroll to Top Button-->
		<!-- <a class="scroll-to-top" href="#page-top">
			<i class="fa fa-angle-up"></i>
		</a> -->
		<!-- Logout Modal-->
		<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
						</button>
						<h5 class="modal-title" id="exampleModalLabel">Are you sure?</h5>
					</div>
					<div class="modal-body">Please make sure。</div>
					<div class="modal-footer">
						<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
						<a class="btn btn-primary" href="javascript:void(0)" id="logout">&nbsp;&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;&nbsp;</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row" style="text-align:center;line-height:50px;">
			<div id="listBtn" class="size col-md-4" style="border-right:solid 1px white;" onclick="list()">List</div>
			<div id="searchBtn" class="size col-md-4" style="border-right:solid 1px white;" onclick="search()">Search</div>
			<div id="localBtn" class="size col-md-4" onclick="localList()">Local List</div>
		</div>
</div>

<!-- /.content-wrapper-->
<footer class="container" style="background:#272727">
	
	<div class="row">
		<div class="col-md-12" style="text-align:center">
			<p>Copyright (c) 2018-2020 TOYOTA MOTOR CORPORATION -V03.00.00</p>
		</div>
	</div>
</footer>
</body>
<script>
$(function(){
	$('#AppManager').click(function(){
		var $AppManager = $("#AppManager");
		var $UserManager = $("#UserManager");
		$AppManager.removeAttr("style");
		$UserManager.removeAttr("style");
		var data={
				'padding-left':'10px',
				'font-size':'22px',
				'color':'#cee3f6',
				};
		var userdata={
				'padding-left':'30px',
				'font-size':'22px',
				'color':'rgba(255, 255, 255, 1)',
		}
		$AppManager.css(data);
		$UserManager.css(userdata);
	});
	initAglSocket();
	list();
});
function list(){
	debugger;
	Commons.showContent('<%=basePath%>/appDev');
	$("#localBtn").css({"background": "#3C3C3C"});
	$("#localBtn").css({"color": "white"});
	$("#listBtn").css({"background": "white"});
	$("#listBtn").css({"color": "black"});
	$("#searchBtn").css({"background": "#3C3C3C"});
	$("#searchBtn").css({"color": "white"});
	
};
function search(){
	debugger;
	Commons.showContent('<%=basePath%>/appDev/SearchDev');
	$("#localBtn").css({"background": "#3C3C3C"});
	$("#localBtn").css({"color": "white"});
	$("#listBtn").css({"background": "#3C3C3C"});
	$("#listBtn").css({"color": "white"});
	$("#searchBtn").css({"background": "white"});
	$("#searchBtn").css({"color": "black"});
	
};
function localList(){
	debugger;	
	$("#localBtn").css({"background": "white"});
	$("#localBtn").css({"color": "black"});
	$("#listBtn").css({"background": "#3C3C3C"});
	$("#listBtn").css({"color": "white"});
	$("#searchBtn").css({"background": "#3C3C3C"});
	$("#searchBtn").css({"color": "white"});
	Commons.showContent('<%=basePath%>/appDev/localApp');
};
var aglSocket;
function initAglSocket() {
	aglSocket = new AglSocket();
}

(function($) {
	var param = $('#params').val();
	$('#logout').click(function(){
		debugger;
		$.ajax({
		    url: $('#basePath').val()+'/login/logout',
			cache: false,
			type: 'GET',
			success: function (data) {
				window.location.href="<%=basePath%>/mainDev?" + param;
			}
		});
	});
	
	$('#UserManager').click(function(){
		var $AppManager = $("#AppManager");
		var $UserManager = $("#UserManager");
		$AppManager.removeAttr("style");
		$UserManager.removeAttr("style");
		var data={
				'padding-left':'10px',
				'font-size':'22px',
				'color':'rgba(255, 255, 255, 1)',
				};
		var userdata={
				'padding-left':'30px',
				'font-size':'22px',
				'color':'#cee3f6',
		}
		$AppManager.css(data);
		$UserManager.css(userdata);
	});
})(jQuery);

$('#login').click(function(){
	debugger;
	Commons.login();
});
</script>
</html>