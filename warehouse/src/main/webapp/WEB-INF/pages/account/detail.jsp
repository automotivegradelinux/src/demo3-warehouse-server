<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<style type="text/css">
	label{
	margin-top:15px;
	}
	/* .col-sm-4 {
	 margin-top:-16px;
	} */
	#e_au {
	height:auto;
	}
	.div-label{
	margin-top:-10px;
	}
</style>
<!-- navigationDialog dialog -->
<jsp:include page="../navigationDialog.jsp" flush="true"></jsp:include>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
		<div class="card mb-3" id="defaultForm">
			<div class="card-body card-change-password pt0">
		      <!-- <div class="card-header"></div> -->
			<div class="alert alert-danger" id="modalErrorArea" role="alert"<c:choose>
				<c:when test="${not empty modelErrors}">style="display: block"</c:when>
				<c:otherwise>style="display: none"</c:otherwise>
				</c:choose>>
				<c:if test="${not empty modelErrors}">
					&nbsp;${modelErrors}
				</c:if>
			</div>
		      <div class="card-body"  style="min-height:530px;">
		        <form id="registerForm" class="was-validated" action = "accountList">
		        <input type="hidden" id="e_userId" value="${appInfo.userId}">
		        <input type="hidden" id="e_userId" value="${appInfo.userPw}">
		          <div class="form-group row">
		            <div class="col-md-3 div-label"><label for="e_userName" class="notNull">User name </label></div>
		            <div class="col-md-9">
		              <input class="form-control" title="This field is required" placeholder="This field is required" id="e_userName" name="e_userName" type="text" value="${appInfo.userName}"  required disabled>
		            </div>
		          </div>
		          <div class="form-group row">
		            <div class="col-md-3 div-label"><label for="e_mailAddress" class="notNull">Mail address</label></div>
		            <div class="col-md-9">
		              <input class="form-control" title="This field is required" id="e_mailAddress" placeholder="This field is required" name="e_mailAddress" type="email" value="${appInfo.mailAddress}"  required disabled>
		            </div>
		          </div>
		          <div class="form-group row">
                     <div class="col-md-3 div-label"><label for="e_au" class="notNull">Authority</label></div>
                     <div class="col-md-9">
                     <input class="form-control" id="e_au" name="e_au" type="text" value="${appInfo.auName}"  required disabled>
                	</div>
                </div>
                 <div class="form-group row">
                     <div class="col-md-3 div-label"><label for="e_au" class="notNull">Create date</label></div>
                     <div class="col-md-9">
                     <input class="form-control" id="e_createDate" name="e_au" type="text" value="${createDate}"  required disabled>
                	</div>
                </div>
                <div class="form-group row">
                     <div class="col-md-3 div-label"><label for="e_au" class="notNull">Update date</label></div>
                     <div class="col-md-9">
                     <input class="form-control" id="e_updateDate" name="e_au" type="text" value="${updateDate}"  required disabled>
                	</div>
                </div>
		        </form>
		      </div>
		    </div>
		</div>
	</c:otherwise>
</c:choose>