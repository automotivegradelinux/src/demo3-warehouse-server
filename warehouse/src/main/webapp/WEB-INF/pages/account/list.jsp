<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<style type="text/css">
	div.dataTables_wrapper div.dataTables_info {
 	padding-top: 0.3em;
 	white-space: nowrap;
 	margin-left:120px;
}
	a.page-link{
	height:calc(1.8125rem + 4px);
	}
 	.styleTable{
	text-align:center;
	} 
</style>
<div class="alert alert-secondary" role="alert">
	<div style="display:inline-block">User</div>
</div>
<div id="mouse" style="width:200px;border:1px solid #D0D0D0;position:absolute;z-index:999;background:#F0F0F0;margin-left:180px;margin-top:-110px;display:none">
	 <c:forEach var="list" items="${authorityList}">
         <option value="${list.value}" >${list.label}</option>
     </c:forEach>
</div>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
		 <div class="alert alert-danger" id="modalErrorArea" role="alert"<c:choose>
			<c:when test="${not empty modelErrors}">style="display: block"</c:when>
			<c:otherwise>style="display: none"</c:otherwise>
			</c:choose>>
			<c:if test="${not empty modelErrors}">
			&nbsp;${modelErrors}
			</c:if>
		</div> 
		<div class="card mb-3">
			<div class="card-header">
				<div class="">
					<div class="row">
					    <div style="display:none">
							<div class="form-group" >
								<label class="control-label" for="s_createDate">CreateDate</label>
								<div class="input-group date date_yyyymmdd" data-date="" data-date-format="yyyy/mm/dd" data-link-field="s_begin_06020101" data-link-format="yyyy/mm/dd">
									<input class="form-control" size="10" type="text" placeholder="please choose" readonly id="s_createDate" style="width:150px;height:38px">
									<span class="input-group-addon"><span class="glyphicon glyphicon-remove fa fa-window-close"></span></span>
									<span class="input-group-addon"><span class="glyphicon glyphicon-calendar fa fa-calendar"></span></span>
								</div>
								<input type="hidden" id="s_begin_06020101"/>
							</div>
						</div>
						<div class="col">
						    <div class="form-group" >
								<label class="control-label" for="s_authority">Authority</label>
								<select id="s_authority" class="form-control">
                                    <c:forEach var="list" items="${authorityList}">
                                        <option value="${list.value}" >${list.label}</option>
                                    </c:forEach>
                                </select>
							</div>
						</div>
						<div class="col">
							<div style="position:absolute;right:20px;top:40px;cursor:pointer;display:none;" class="input_clear">		
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>	    
						    </div> 
							<label class="control-label" for="t_userName">KeyWord</label>
							<input type="text" class="form-control style-height" id="t_keyWord" name="t_userName" placeholder="please enter">
						</div>
						<div class="col" style="visibility:hidden">
						    <div style="position:absolute;right:20px;top:40px;cursor:pointer;display:none;" class="input_clear">		
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>	    
						    </div>
							<label class="control-label" for="t_mailAddress">MailAddress</label>
							<input type="email" class="form-control style-height" id="t_mailAddress" name="s_mailAddress" placeholder="please enter">
						</div>
						<div class="col" style="visibility:hidden">
							<div class="form-group">
								<label class="control-label" for="t_mailAddress">*******</label>
								<input type="email" class="form-control style-height" id="t_mailAddress" name="s_mailAddress" placeholder="please enter">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col" style="text-align:right">
						<button type="button" class="btn btn-primary pl20 pr20" id="btn-save-edit" onclick="Commons.showContent('<%=basePath%>/account/modify')">Create</button>
						<button type="button" title="step2 development" class="btn btn-success pl20 pr20 ml10" id="btn-save-edit" onclick="$table.DataTable().ajax.reload();">Search</button>
					</div>
				</div>
			</div>
			<div class="card-body">
				<div class="table-responsive" id="div-table-container">
					<table id="example" class="table table-bordered hover table-striped" cellspacing="0" width="100%">
						<thead>
							<tr>
								<th style="text-align:left;">No.</th>
								<th>UserName</th>
								<th>MailAddress</th>
								<th>CreateDate</th>
								<th>Authority</th>
								<th>Operation</th>
							</tr>
						</thead>
					</table>
					<div style="height:20px;"></div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>
<script>
$(function() {
	$("input").focus(function(){      
		$(this).parent().children(".input_clear").show();  
	});  
	$("input").blur(function(){      
		if($(this).val()==''){          
			$(this).parent().children(".input_clear").hide();      
			}  
	});  
	$(".input_clear").click(function(){      
		$(this).parent().find('input').val('');      
		$(this).hide();  
	}); 
	$(".card-body").css({
		minHeight:($(window).height()-500)
	})
	Commons.bindDateControl();
	Commons.appendRedAsterisk();
	initTable();
	bingDateControl();
});

var $table = $('#example');
var $errorArea = $('#modalErrorArea');

var queryParam = function(param) {
	param.orderColumn = param.order[0].column;
	param.orderDir = param.order[0].dir;
	param.keyWord = $('#t_keyWord').val();
	param.mailAddress = $('#t_mailAddress').val();
    param.auId = $('#s_authority').val();
    param.createDate = $('#s_createDate').val(); 
	return param;
}; 

function initTable() {
	$table.DataTable({
		order: [[ 1, 'asc' ]],
		ajax: {
			url: '<%=basePath%>/account/search',
			data: queryParam,
			type: 'GET',
			dataFilter: function(data){
            	$errorArea.empty();
            	$errorArea.css("display", "none");
				var json = $.parseJSON(data);
				for(var i= 0;i<json.data.length;i++){
					json.data[i].createDate = Commons.format(json.data[i].createDate);
				}
				
	            if (json.statusCode && json.statusCode != null) {
	            	$errorArea.empty().append('<div>' + json.statusText + '</div>');
	            	$errorArea.css("display", "block");
	            }
	            var data = JSON.stringify(json) 
	            return data; // return JSON string
	        }
	    },
	    columns: [
			{
				orderable : false,
				/* style : "text-align:center", */
				class : 'styleTable',
			    width : '30px',
			    render : function(data, type, row, meta) {
					return meta.row+1;
				}	
			},
			{
				data : 'userName',
				width : '150px'
			},
			{
				data : 'mailAddress',
				width : '150px'
			},
			{
				data : 'createDate',
				width : '120px'
			},
			{
				data : 'auName',
				orderable : false,
				width : '120px'
			},
			{
				className : 'td-operation',
				data: null,
				defaultContent:'',
				orderable : false,
				width : '80px',
				render: function(data, type, row, meta) {
					var context = '<a href="javascript:void(0)" style="color:rgba(134, 142, 150, 1);" title="REVISE" onclick="Commons.showContent(\'<%=basePath%>/account/modify?isDetail=false&userId='+data.userId+'\')"><i class="fa fa-pencil" aria-hidden="true"></i></a>&nbsp;';
					 if (data.isDel == 0){
						context += '<a href="javascript:void(0)" style="color:rgba(134, 142, 150, 1);padding:15px;" title="DELETE" class="data-delete" data-data-id="'+data.userId+'" style="margin-left:0.5em"><i class="fa fa-trash" aria-hidden="true"></i></a>';
						context += '<a href="javascript:void(0)" style="color:rgba(134, 142, 150, 1);" title="DETAIL" onclick="Commons.showContent(\'<%=basePath%>/account/modify?isDetail=true&userId='+data.userId+'\')"><i class="fa fa-search-plus" aria-hidden="true"></i></a>&nbsp;';
						
					} 
					return context;
				}
			}
		],
		drawCallback: function(settings) {
			TableListDeletePlugin.init('example', '<%=basePath%>/account/search', '<%=basePath%>/account/delete');
		}
	});
}
</script>