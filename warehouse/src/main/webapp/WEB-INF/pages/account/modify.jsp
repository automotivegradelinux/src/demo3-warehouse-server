<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<style type="text/css">
	label{
	margin-top:15px;
	}
	/* .col-sm-4 {
	 margin-top:-16px;
	} */
	#e_au {
	height:auto;
	}
	.div-label{
	margin-top:-10px;
	}
</style>
<!-- navigationDialog dialog -->
<jsp:include page="../navigationDialog.jsp" flush="true"></jsp:include>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
		<div class="card mb-3" id="defaultForm">
			<div class="card-body card-change-password pt0">
		      <!-- <div class="card-header"></div> -->
			<div class="alert alert-danger" id="modalErrorArea" role="alert"<c:choose>
				<c:when test="${not empty modelErrors}">style="display: block"</c:when>
				<c:otherwise>style="display: none"</c:otherwise>
				</c:choose>>
				<c:if test="${not empty modelErrors}">
					&nbsp;${modelErrors}
				</c:if>
			</div>
		      <div class="card-body"  style="min-height:530px;">
		      	<div class="alert alert-danger" style="display:none" id="modalErrorArea" role="alert"></div>
		        <form id="registerForm" class="was-validated" action = "accountList">
		        <input type="hidden" id="e_userId" value="${appInfo.userId}">
		          <div class="form-group row">
		            <div class="col-md-3 div-label"><label for="e_userName" class="notNull">User Name </label></div>
		            <div class="col-md-9">
		              <input class="form-control style-height" maxlength="32" placeholder="Maximum length is 32 characters" id="e_userName" name="e_userName" type="text" value="${appInfo.userName}"  required>
		            </div>
		          </div>
		          <c:if test="${empty appInfo.userId}">
		          <div class="form-group row">
		            <div class="col-md-3 div-label"><label for="e_userPw" class="notNull">Password</label></div>
		            <div class="col-md-9">
		              <input class="form-control input-color style-height" id="e_userPw" name="e_userPw" type="password" maxlength="32"  placeholder="Between 6 and 32 characters" value="${appInfo.userPw}"  required>
		            </div>
		          </div>
		          <div class="form-group row">
		            <div class="col-md-3 div-label"><label for="e_reuserPw" class="notNull">Password Confirmation</label></div>
		            <div class="col-md-9">
		              <input class="form-control input-color style-height"  id="e_reuserPw" name="e_reuserPw" type="password" maxlength="32" placeholder="The same as the password above" value="${appInfo.reuserPw}"  required>
		            </div>
		          </div>
		          </c:if>
		          <div class="form-group row">
		            <div class="col-md-3 div-label"><label for="e_mailAddress" class="notNull">Mail Address</label></div>
		            <div class="col-md-9">
		              <input class="form-control input-color style-height"  id="e_mailAddress" maxlength="32" placeholder="Only letters, numbers, and underlines can be used" name="e_mailAddress" value="${appInfo.mailAddress}"  required>
		            </div>
		          </div>
		          <div class="form-group row">
                     <div class="col-md-3 div-label"><label for="e_au" class="notNull">Authority</label></div>
                     <div class="col-md-9">
                      <select id="e_au" class="form-control" required>
                        <c:forEach var="list" items="${authorityList}">
                           <option value="${list.value}" <c:if test="${list.value eq appInfo.auId}">selected="selected"</c:if>>${list.label}</option>
                        </c:forEach>
                     </select> 
                	</div>
                </div>
		        </form>
		       <div style="float:right" class="col-md-0">
		        <button type="button" class="btn btn-primary form-btn-w-70" id="btn-save-edit" onclick="saveaccountForm()">save</button>
		       <%--  <button type="button" class="btn btn-cancel form-btn-w-70" Style="background-Color:gray" onclick="Commons.showContent('<%=basePath%>/account/')">back</button> --%>
		      </div>
		      </div>
		    </div>
		</div>
	</c:otherwise>
</c:choose>

<script src="<%=basePath%>/js/constant.js"></script>
<script>
$(function(){
	Commons.appendRedAsterisk();
	var e_userId = $('#e_userId').val();
	if(e_userId !== ""){
		$('#e_mailAddress')[0].style.border = "1px solid #28a745";
	}
    $('#e_userPw').bind('input propertychange', function() {
        if($('#e_userPw').val().length > 5 ){
    	    $('#e_userPw')[0].style.border = "1px solid #28a745";
    	}else{
    	    $('#e_userPw')[0].style.border = "1px solid red";
    	}
    });
    $('#e_reuserPw').bind('input propertychange', function() {
    	if($('#e_reuserPw').val().length > 5 ){
    	    $('#e_reuserPw')[0].style.border = "1px solid #28a745";
    	}else{
    	    $('#e_reuserPw')[0].style.border = "1px solid red";
    	}
    });
    $('#e_mailAddress').bind('input propertychange', function() {
    	var szReg=/^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.[a-zA-Z0-9]{2,6}$/;
    	           
    	if(!szReg.test($('#e_mailAddress').val())){
    		$('#e_mailAddress')[0].style.border = "1px solid red";
    	}else{
    		$('#e_mailAddress')[0].style.border = "1px solid #28a745";
    	}
    });
});
var accountForm = function() {
	this.userId = '';
	this.userPw = '';
	this.userName = '';
	this.auId = '';
	this.mailAddress = '';
	this.userPw = '';
	this.reuserPw = '';
};

var $errorArea = $('#modalErrorArea');
var saveaccountForm = function() {
	$("#btn-save-edit").attr('disabled',true);
	$errorArea.empty();
	$errorArea.css("display", "none");
	var tf = new accountForm();
	tf.userId = $('#e_userId').val();
	tf.reuserPw = $('#e_reuserPw').val();
	tf.userPw = $('#e_userPw').val();
	tf.userName = $('#e_userName').val();
	tf.auId = $('#e_au').val();
	tf.mailAddress = $('#e_mailAddress').val();

	$.ajax({
		url: '<%=basePath%>/account/update',
		cache: false,
		type: 'POST',
		data: {form: JSON.stringify(tf)},
		success: function (data) {
			var json = JSON.parse(data);
			if(200 == json.result) {
				Commons.showContent('<%=basePath%>' + json.forward);
			} else if(401 == json.result) {
				var errorContent = '';
				for (var i in json.message) {
					errorContent += '<div>' + json.message[i] + '</div>';
				}
				$errorArea.empty().append(errorContent);
				$errorArea.css("display", "block");
				$("#btn-save-edit").attr('disabled',false);
			} else if(500 == json.result) {
				var errorContent = '';
				for (var i in json.message) {
					errorContent += '<div>' + json.message[i] + '</div>';
				}
				$errorArea.empty().append(errorContent);
				$errorArea.css("display", "block");
				$("#btn-save-edit").attr('disabled',false);
			}
		}
	});
	/* }
	} */
};
</script>