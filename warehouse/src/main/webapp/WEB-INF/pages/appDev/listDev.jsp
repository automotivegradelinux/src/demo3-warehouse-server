<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<style type="text/css">
div.dataTables_wrapper div.dataTables_info {
	padding-top: 0.3em;
	white-space: nowrap;
	margin-left: 120px;
}

a.page-link {
	height: calc(1.8125rem + 4px);
	margin-left: -5px;
}

</style>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
		<div class="card mb-3" style="BACKGROUND-COLOR: transparent;color:white">
		     <div class="container" role="alert" style="display:none">
				<div class="row" >
		            <div class="input-group">		  
						<input type="text" class="form-control size" id="t_keyWord" name="t_keyWord" value="" placeholder="please input keywords" style="height:50px;BACKGROUND-COLOR: transparent;color:white">
			            <button class="input-group-addon fa fa-search" id="btn-save-edit" style="font-size:30px;color:green;height:50px;BACKGROUND-COLOR: transparent;" onclick="$table.DataTable().ajax.reload();"></button>
					</div>
			    </div>
			</div>
			<div class="card-header" style="display:none">
					<input type="text" class="form-control" id="t_filePath" size="16" name="t_filePath" style="display:none;">
					<div style="overflow-x:scroll;white-space:nowrap;height:70px;">
					    <c:forEach var="list" items="${appDeviceTypeList}">
						    <label class="btn btn-skin col-md-4 button-size" style="overflow:hidden;color:white" onclick="deviceTypeButton(${list.value})"><input style="display:none;" type="radio" name="deviceType" >${list.label}</label>
						</c:forEach>
					</div><br>
                    <div id="appType" style="color:gray;overflow-x:scroll;white-space:nowrap;height:70px;display:none;">
					    <c:forEach var="list" items="${appTypeList}">
							<label class="btn btn-skin col-md-4 button-size" style="overflow:hidden;color:white" onclick="TypeButton(${list.value})"><input style="display:none;" type="radio" name="appType" >${list.label}</label>
						</c:forEach>
					</div>
			</div>
			<div class="card-body">
			    <div id="appHeight" style="overflow-x:hidden;" class="test1">
			       <table id="example" cellspacing="0" style="width:100%;">
						<thead style="display:none">
							<tr><hr>
								<th>No.</th>
								<th>App Name</th>
								<th>Developer</th>
								<th>TypeId</th>
								<th id="textarea">App Abstract</th>
								<th>Manipulation</th>
							</tr>			
						</thead>
				    </table>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>
<script src="<%=basePath%>/js/common.js"></script> 
<script>
$(function() {
	$("#appHeight").height($(document).height()-250);
	$("#page-content").css("background-image","url(<%=basePath%>/image/AGL_HMI_Blue_Background_Car-01.png)");
	Commons.bindDateControl();
	Commons.appendRedAsterisk();

	initData(true);
});

var deviceTypeId = "";
var TypeId = "";	
var $table = $('#example');
var $errorArea = $('#modalErrorArea');

</script>