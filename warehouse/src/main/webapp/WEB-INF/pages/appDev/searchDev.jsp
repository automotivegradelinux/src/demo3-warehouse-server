<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<style type="text/css">
	div.dataTables_wrapper div.dataTables_info {padding-top: 0.3em;white-space: nowrap;margin-left:120px;}
	a.page-link{height:calc(1.8125rem + 4px);margin-left:-5px;}
   .div{font-size:18px; color:#000;line-height:24px;white-space:nowrap;text-overflow:ellipsis;word-break:break-all;overflow:hidden;}
		.item,.carousel-caption{
			width: 100%;
			height: 300px;
		}
		.carousel-caption{
			left: 0;
			top: 0;
			padding: 0;
		}
		.mask {       
            position: absolute; top: 0px; filter: alpha(opacity=60); background-color: #777;     
            z-index: 1002; left: 0px;     
            opacity:0.5; -moz-opacity:0.5;     
        } 
        
	</style>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
		<div class="card mb-3" style="BACKGROUND-COLOR: transparent;color:white">
			<div class="alert alert-danger" id="modalErrorArea" role="alert"
				<c:choose>
						<c:when test="${not empty modelErrors}">style="display: block"</c:when>
						<c:otherwise>style="display: none"</c:otherwise>
						</c:choose>>
				<c:if test="${not empty modelErrors}">
							&nbsp;${modelErrors}
						</c:if>
			</div>
			<div class="card-header" style="height:250px;">
			    <div style="color:#00CACA;font-size:30px;">KeyWord</div><br>
			    <div class="row" >
		            <div class="input-group col-md-10">		  
						<input type="text" class="form-control size" id="t_keyWord" name="t_keyWord" value="" placeholder="please input keywords" style="height:50px;BACKGROUND-COLOR: transparent;color:white">
			            <button id="btn-save-edit" style="color:white;height:50px;width:50px;font-size:30px;border-color:white;background:none;" onclick="clean()">X</button>
					</div>
 					<div class="col-md-2">
					    <button class="btn btn-skin button-size" id="btn-search" style="color:white;height:50px;float:right" onclick="searchApp(this);">search</button>				 
					</div>
			    </div><br>
			    <div style="color:#00CACA;font-size:30px;">AppType</div>
					<input type="text" class="form-control" id="t_filePath" size="16" name="t_filePath" style="display:none;">
					<div style="overflow-x:scroll;white-space:nowrap;height:70px;display:none">
					    <c:forEach var="list" items="${appDeviceTypeList}">
						    <label class="btn btn-skin col-md-4 button-size" style="overflow:hidden;color:white" onclick="deviceTypeButton(${list.value})"><input style="display:none;" type="radio" name="deviceType" >${list.label}</label>
						</c:forEach>
					</div><br>
                    <div id="appType" style="color:gray;overflow-x:scroll;white-space:nowrap;height:70px;" class="test1">
                   		<button id="type-btn-all" class="btn btn-skin col-md-4 button-size" style="overflow:hidden;color:white"" onclick="TypeButton(this,'');">Show All Types</button>
					    <c:forEach var="list" items="${appTypeList}">
							<button id="type-btn-signal" class="btn btn-skin col-md-4 button-size" style="overflow:hidden;color:white" onclick="TypeButton(this,${list.value});">${list.label}</button>
						</c:forEach>
					</div>
			</div>
			<div class="card-body">
			    <div id="appHeight" style="overflow-x:hidden;" class="test1">
			       <table id="example" cellspacing="0" style="width:100%;">
						<thead style="display:none">
							<tr><hr>
								<th>No.</th>
								<th>App Name</th>
								<th>Developer</th>
								<th>TypeId</th>
								<th id="textarea">App Abstract</th>
								<th>Manipulation</th>
							</tr>			
						</thead>
				    </table>
				</div>
				<div style="height:20px"></div>
			</div>
		</div>
	</c:otherwise>
</c:choose>
 
<script>
$(function() {
	$("#appHeight").height($(document).height()-520);
	$("#page-content").css("background-image","url(<%=basePath%>/image/AGL_HMI_Blue_Background_Car-01.png)");
	Commons.bindDateControl();
	Commons.appendRedAsterisk();
}); 

var appTypeFlag = false;
function searchApp(obj){
	debugger;
	if(appTypeFlag == false){
		$("#type-btn-all").css("background-color", "#00D096");
	}
	if(isInitTable == false){
		initData(false);
		isInitTable = true;
	}else if(isInitTable == true){
		$table.DataTable().ajax.reload();
	}
}

//appDeviceType
var deviceTypeButton = function(deviceTypeValue){
    $("input[name='deviceType']").parent().css('background-color','');
	$('#appType')[0].style.display="block";
	$table.DataTable().ajax.reload();
	$('input:radio[name="deviceType"]:checked').parent().css('background-color','#00D096');
	deviceTypeId = deviceTypeValue;
};

//appType
var TypeButton = function(obj, typeValue){
	TypeId = typeValue;
	debugger;
	$('button[id^="type-btn-"]').css("background-color", "");
    $(obj).css("background-color", "#00D096");
    appTypeFlag = true;
	searchApp(obj);
};

//Judge whether is the first time searched
var isInitTable = false;
var deviceTypeId = "";
var TypeId = "";
var $table = $('#example');
var $errorArea = $('#modalErrorArea');

 var strSub = function(str, subLen){
	if(str == null){
		return "";
	} 
	if(str.length > subLen){
		str = str.substring(0, subLen);
		appAbstract = str + "...";
		return appAbstract;
	}
	return str;
}; 

function clean(){
	$("#t_keyWord").val("");
}

</script>