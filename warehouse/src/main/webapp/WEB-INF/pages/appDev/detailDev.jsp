<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<!-- navigationDialog dialog -->
<%-- <jsp:include page="../navigationDialog.jsp" flush="true"></jsp:include> --%>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
	<div id="page-content" class="index-page container" style="BACKGROUND-COLOR: transparent;color:white">
	<div class="row" id="appHeight">
		<div class="col-md-12 col-md-offset-12">
			<div id="main-content">
				<article>
					<div class="art-content" style="min-height:480px;height:100%;width:auto;margin-left:45px;margin-right:45px;word-break: break-all; word-wrap:break-word;">
						<input id="t_typeId" type="hidden" value="${appInfo.typeId}">
						<input id="t_appId" type="hidden" value="${appInfo.appId}">
						<input id="t_filePath" type="hidden" value="${appInfo.verFilePath}">
						<input id="iconPath" type="hidden" value="${appInfo.imagePath}">
						<div  style="display:none">
						    <input id="t_developer" type="text" value="${appInfo.developer}">
						</div>
						
						<h2 style="color:white;">${appInfo.appName}</h2>
						<table>
							<tr>
								<td rowspan="3" valign="top" style="width:130px"><img id="imagePath" src="${appInfo.imagePath}" style="width:120px;height:120px"/></td>
								<td>By ${appInfo.developerName}&nbsp;${date}<br>${appInfo.versionName}<br>${appInfo.typeName}<br>${appInfo.appDeviceTypeName}<br></td>
							</tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
						</table>
						<div id="appAbstract" class="col-md-12" style="margin-left:-15px">${appInfo.appAbstract}</div><br>
						<div style="color:#ADADAD; margin: 0px 0px 10px 0px;">MD5:<a>${appInfo.hashcode}</a></div>
						<c:choose>
							<c:when test="${appStatus == 0}">
								<button class="btn btn-skin size" type="button" style="width:190px;padding-left:0px;padding-right:0px;" onclick="doDownload(this, '${appInfo.appId}','${appInfo.typeId}','${appInfo.verFilePath}')">install</button>
							</c:when>
							<c:when test="${appStatus == 1}">
								<button class="btn btn-skin size" type="button" style="width:190px;padding-left:0px;padding-right:0px;" onclick="launchApp('${localApp.id}')">launch</button>
							</c:when>
							<c:when test="${appStatus == 2}">
								<button class="btn btn-skin size" type="button" style="width:190px;padding-left:0px;padding-right:0px;" onclick="onclick=updateApp(this,'${appInfo.appId}','${appInfo.typeId}','${appInfo.verFilePath}','${localApp.id}')">update</button>
							</c:when>
						</c:choose>
						<a href="javascript:void(0);" onclick="backToPrePage('${isListDev}')"><button type="button" class="btn btn-skin size" style="width:190px;">Back</button></a>
					</div>
							
					</div>
				</article>
			</div>
		</div>
		<div style="height:28px"></div>
    </div>
	</c:otherwise>
</c:choose>
<script src="<%=basePath%>/js/constant.js"></script>
<script>
$(function() {
	$("#appHeight").height($(document).height()-250);
	$("#page-content").css("background-image","url(<%=basePath%>/image/AGL_HMI_Blue_Background_Car-01.png)");
	if($('#iconPath').val() == ''){
		$('#imagePath').attr("src",'<%=basePath%>/image/defaultAppIcon.png');
	}
	// TODO DJ 暂时放置，等上传功能好用之后再对应，猜想textarea上传后，数据库会把回车空格之类的给替换掉。
	var abstractSubStr = document.getElementById("appAbstract").innerHTML; 
	var reg=new RegExp("\r\n","g"); 
    var reg1=new RegExp("\n","g"); 
	var abstractSubStr= abstractSubStr.replace(reg,"<br>");
	abstractSubStr= abstractSubStr.replace(reg1,"<br>");
	document.getElementById("appAbstract").innerHTML = abstractSubStr; 
}); 

function backToPrePage(isListDev){
	if("true" == isListDev){
		Commons.showContent('<%=basePath%>/appDev');
	}else if("false" == isListDev){
		Commons.showContent('<%=basePath%>/appDev/SearchDev');
	} 
}
</script>