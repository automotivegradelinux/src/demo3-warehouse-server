<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<style type="text/css">
	a.page-link{height:calc(1.8125rem + 4px);margin-left:-5px;}
   .div{font-size:18px; color:#000;line-height:24px;white-space:nowrap;text-overflow:ellipsis;word-break:break-all;overflow:hidden;}
		.item,.carousel-caption{
			width: 100%;
			height: 300px;
		}
		.carousel-caption{
			left: 0;
			top: 0;
			padding: 0;
		}
</style>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}</div>
	</c:when>
	<c:otherwise>
		<div class="card mb-3"
			style="BACKGROUND-COLOR: transparent; color: white">
			<div class="card-body">
				<div id="appHeight" style="overflow-x: hidden;" class="test1">
					<div>
						<hr>
						<div id="localList" style="padding-left: 15px; padding-right: 15px;"></div>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>
<script>
$(function() {
	$("#appHeight").height($(document).height()-250);
	$("#page-content").css("background-image","url(<%=basePath%>/image/AGL_HMI_Blue_Background_Car-01.png)");
	Commons.bindDateControl();
	Commons.appendRedAsterisk();
	
	initLocalData();
}); 

var $errorArea = $('#modalErrorArea');

function initLocalData(){
	debugger;
	console.log("localapp" + "initLocalData");
	this.listReplyok = this.listReplyok.bind(this);
	aglSocket.listInstalledApps(initLocalDataReplyok, initLocalDatareplyerr);
};

function initLocalDataReplyok(obj){
	debugger;
	console.log("localapp" + "listReplyok\n");
	var appListStr = obj["response"];
	checkLocalUpdate(appListStr);
	console.log(appListStr);
};

function initLocalDatareplyerr(obj){
	debugger;
	alert("get local app failed");
};

function checkLocalUpdate(appListStr){
	debugger;
	$.ajax({
		url:"<%=basePath%>/appDev/CheckUpdateInfoDev",
		type: "POST",
		dataType: "json",
		contentType:"application/json;charset=utf-8",
		data: JSON.stringify(appListStr),
		success:function(data){
			debugger;
			initTable(data,appListStr);
			} 
	})
};

/* 
author:"↵     Loïc Collignon <loic.collignon@iot.bzh>↵     Matt Porter <mporter@konsulko.com>↵   "
author-email:"info@automotivelinux.org"
description:"AGL Default Mixer"
height:""
http-port:1024
icon:"/var/local/lib/afm/applications/mixer/6.90/icon.svg"
id:"mixer@6.90"
name:"mixer"
shortname:""
version:"6.90.0-04990a8"
width:"" 
*/
/***
 * applist: local list
 * appListStr:local app package(afm-util list)
 */
function initTable(applist,appListStr){
	console.log(TAG_LOG + "applist.length" + applist.length);
	for (var i = 0; i < applist.length; i++) {
		var data = applist[i];
		var localdata = appListStr[i];
		var appStatusButton = "";  
		
/* 		if(data.updateFlag){
			var appStatusButton = '<button class="btn btn-skin button-size" type="button" style="margin-right:auto;padding-left:0px;padding-right:0px;width:150px;color:white"' 
                               +'onclick=updateApp(this,"'+data.appId+'","'+data.typeId+'","'+data.verFilePath+'","'+localdata.id+'")>update</button>'
		}
		else */{
			var appStatusButton = '<a href="javascript:void(0);" onclick=launchApp("'+localdata.id+'")><button class="btn btn-skin button-size" type="button" style="margin-right:auto;width:150px;color:white" >launch</button></a>';
		}
		
		if(data.systemApp){
			var uninstall = '<a href="javascript:void(0);"><button class="btn btn-skin button-size" type="button" style="display:none;" >uninstall</button></a>';
		}
		else{
			var uninstall = '<a href="javascript:void(0);" onclick=uninstallApp("'+localdata.id+'")><button class="btn btn-skin button-size" type="button" style="margin-right:auto;width:150px;color:white" >uninstall</button></a>';
		}
		
		var date = null;
		if(data.createDate == null){
			date = "----/--/--";
		}
		else{
			date = new Date(data.createDate).format("yyyy-MM-dd");
		}
		
	    if(data.imagePath == null){
	    	data.imagePath = '<%=basePath%>/image/defaultAppIcon.png';
		}
	    
	    if(data.typeName == null){
	    	data.typeName = "--";
	    }
	    
		var context =
			   '<div class="row">'						    
				  +'<div class="col-md-2" style="text-align:center;">'
				  	+'<img style="height:auto;width:100%;" src = "'+data.imagePath+'">'
				  +'</div>'
				  
				  +'<div class="col-md-7">'
				  	+'<h4 style="color:#00AEAE;">' + localdata.name + '</h3>'
				  	+'<div class="font-size"> <strong>Version</strong>:&nbsp'+ localdata.version + '</div>'
				  	+'<p class="font-size ellipsis" style="color:white;"> <strong>Description</strong>:&nbsp' + data.appAbstract + '</p>'
				  +'</div>'
				  
				  +'<div class="col-md-3">'
				    +'<div style="float:right">'+ appStatusButton + '</div>'
				    +'<div style="float:right;margin-top:10px;">' + uninstall + '</div>'
				  +'</div>'
			 +'</div>'			
			+ '<hr>';
/* 	    var context = '<div style="height:120px width:100%; margin-right:210px;""><div><img style="height:120px;width:120px" src = "'+data.imagePath+'"></div>'
	        +'<div class="margin-left"style="margin-top:-120px;"><div style="color:#00AEAE;font-size:30px;">'+localdata.name+'</div></div><div style="height:20px;"></div>'
	        +'<div class="font-size margin-left">'+ 'Version:&nbsp' + localdata.version +'</div>'
	        +'<div class="font-size margin-left" style="overflow: hidden;white-space: nowrap;text-overflow: ellipsis; max-width:400px; height:50px;">'+ 'Description:&nbsp' + data.appAbstract+'</div></div>'
	        +'<div style="float:right; width:200px;"><div style="margin-top:-105px;">'+appStatusButton+'</div>'
	        +'<div style="padding-top:10px;">'+uninstall+'</div></div><hr>'; */
	    $("#localList").append(context);
	}
}

function uninstallApp(appId) {
	debugger;
	aglSocket.uninstallApp(appId);
	Commons.showContent('<%=basePath%>/appDev/localApp');	
}
</script>