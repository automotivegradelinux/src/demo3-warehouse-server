<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<script src="<%=basePath%>/js/constant.js"></script>
<div>${breadcrumb.name}</div>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
  	<c:forEach var="breadcrumb" items="${appInfo.breadcrumb}">
  		<c:choose>
	  		<c:when test="${not breadcrumb.current}">
				<li class="breadcrumb-item"><a href="javascript:void(0);"onclick="Commons.showContent('<%=basePath%>/${breadcrumb.url}/')">${breadcrumb.name}</a></li>
			</c:when>
			<c:otherwise>
				<li class="breadcrumb-item active" aria-current="page">${breadcrumb.name}</li>
			</c:otherwise>
		</c:choose>
  	</c:forEach>
  </ol>
</nav>
