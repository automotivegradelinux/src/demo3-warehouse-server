<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
String ippath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
%> 
<style type="text/css">
 	#e_appTypeId {height:auto;}
    .box{height:50px;border-style:solid; border-width:1px; border-color:#F0F0F0;text-align:center;line-height:50px;color:white;}
	.file-btn{z-index:0;filter:alpha(opacity=1);opacity:0.8;}
	#e_position{z-index:100;}
	 .create-schedule{border:2px solid #E0E0E0;}
	 .create-scheduling{border:2px solid #29e62d;}
</style>
<!-- navigationDialog dialog -->
<jsp:include page="../navigationDialog.jsp" flush="true"></jsp:include>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
		<div class="card mb-3" style="height:auto;">
		    <!-- <div style="margin-left:10px;width:98%"> -->
		    <div class="col-md-12 box">
		        <div class="form-group row">
		            <img src="image/insertAppDetails.PNG" style="height:100%;width:100%"/>
		        </div>
		        <div class="form-group row" style="margin-top:-15px;">
		            <div class="col-md-4 create-scheduling" ></div> 
		            <div class="col-md-4 create-schedule" ></div> 
		            <div class="col-md-4 create-schedule" ></div>
		        </div>
		    </div>
			<div class="card-body card-change-password pt0">
			<div class="alert alert-danger" id="modalErrorArea" role="alert"<c:choose>
				<c:when test="${not empty modelErrors}">style="display: block"</c:when>
				<c:otherwise>style="display: none"</c:otherwise>
				</c:choose>>
				<c:if test="${not empty modelErrors}">
					&nbsp;${modelErrors}
				</c:if>
			</div>
			<p style="font-size:25px;margin-top:10px;">Fill in information</p>
		      <div class="card-body">
		      	<div class="alert alert-danger;"style="display:none" id="modalErrorArea" role="alert"></div>
		        <form id="registerForm" class="was-validated"> 
		       		<input type="hidden" id="e_deviceTypeId" value="${appInfo.appDeviceTypeId}">
		       		<input type="hidden" id="e_hashcode" value="${appInfo.hashcode}">
		       		<input type="hidden" id="e_verFilePath" value="${appInfo.verFilePath}">
		       		<input type="hidden" id="e_appSize" value="${appInfo.appSize}">
		       		<input type="hidden" id="e_appVersionId" value="${appInfo.appVersionId}">
		       		<input type="hidden" id="e_imagePath" value="${appInfo.imagePath}">
		            <div class="form-group row">
			            <div class="col-md-3"><label for="e_uuId" class="notNull">UUID</label></div>
			            <div class="col-md-9">
			              <input class="form-control style-height" id="e_uuId" name="e_uuId" title="This field is required" maxlength="32" type="text" placeholder="" value="${appInfo.appId}"  required disabled>
			            </div>
		          	</div> 
		          	<c:if test="${empty appInfo.appIdCustom}">
		          	<div class="form-group row">
			            <div class="col-md-3 "><label for="e_position" class="notNull">App File</label></div>
			             <div class="col-md-2 file-box" id="upload" style="display:block">
			                <input type="button" class="btn btn-success" value="UPLOAD"  style="height:50px;width:150px;padding:0px;position:absolute;top:-10px;">
			             	<input title="This field is required" id="path" name="position" type="file"  style="height:50px;width:150px;padding:0px;position:absolute;top:-10px;opacity:0;" class="form-control file-btn" value="${appInfo.verFilePath}" accept=".wgt" onchange="change(event)" required>
			            </div>	
			                 
			            <div class="col-md-2 file-box" id="uploading" style="display:none">
			                <input type="button" class="btn btn-success" value="UPLOADING......"  style="height:50px;width:150px;padding:0px;position:absolute;top:-10px;background:#ADADAD">
			             	<input title="This field is required" id="path" name="position" type="file"  style="height:50px;width:150px;padding:0px;position:absolute;top:-10px;opacity:0;" class="form-control file-btn" value="${appInfo.verFilePath}" accept=".wgt" onchange="change(event)" required>
			            </div>	      
		          	</div>
		          	</c:if>
		          	 <div class="form-group row">
			            <div class="col-md-3"><label for="e_appId" class="notNull">App Id</label></div>
			            <div class="col-md-9">
			              <input class="form-control style-height" id="e_appId" name="e_appId" title="This field is required"  type="text"  value="${appInfo.appIdCustom}"  required disabled>
			            </div>
		          	</div> 
		          	<div class="form-group row">
			            <div class="col-md-3"><label for="e_versionName" class="notNull">Version</label></div>
			            <div class="col-md-9">
			              <input class="form-control style-height" id="e_versionName" title="This field is required" name="e_versionName"  type="text"  value="${appInfo.versionName}"  required disabled>
			            </div>
		          	</div> 
		          	<div class="form-group row">
			            <div class="col-md-3"><label for="e_appName" class="notNull">App Name</label></div>
			            <div class="col-md-9">
			              <input class="form-control style-height" id="e_appName" name="e_appName" title="This field is required" type="text" placeholder="" value="${appInfo.appName}"  required disabled>
			            </div>
		          	</div> 
		          	<div class="form-group row">
			            <div class="col-md-3"><label for="e_isPublicName" class="notNull">App Is Public</label></div>
			            <div class="col-md-9">
			                <select id="e_isPublicName" class="form-control" required>
			                    <c:forEach var="list" items="${appIsPublicList}">
	                                <option value="${list.value}" <c:if test="${list.value eq appInfo.appIsPublic}">selected="selected"</c:if>>${list.label}</option>
	                            </c:forEach>
	                        </select>
			            </div>
		          	</div> 
		           	<div class="form-group row" >
			            <div class="col-md-3 "><label for="e_appTypeId" class="notNull">App Type</label></div>
			            <div class="col-md-9">
			              <select id="e_appTypeId" class="form-control" required>
	                        <c:forEach var="list" items="${appTypeList}">
	                           <option value="${list.value}" <c:if test="${list.value eq appInfo.typeId}">selected="selected"</c:if>>${list.label}</option>
	                        </c:forEach>
	                      </select>
			            </div>
		          	</div>
		          	<div class="form-group row">
	                    <div class="col-md-3" style="margin-top:65px;"><label for="e_appAbstract" class="notNull">App Description</label></div>
		                    <div class="col-md-9">
		                	<textarea class="form-control" title="This field is required" maxlength="800" placeholder="Maximum length is 800 characters." id="e_appAbstract" name="e_appAbstract" style="height:150px;" rows="8" cols="40" required>${appInfo.appAbstract}</textarea>
	                	</div>
                	</div>
                	 <div class="form-group row">
			            <div class="col-md-3"><label style="margin-top:35px;" for="e_thumbnail" class="notNull">App Icon</label></div>
			            <div class="col-md-9">
			                <img id="e_iconPath" src="${appInfo.imagePath}" style="width: 150px; height:150px;border:#ADADAD 1px solid"/><br>
			                <input onchange="changeImg(event,'e_iconPath')"  type="file" style="width:150px;height:150px;margin-top:-150px;position: absolute; opacity:0;">
			            </div>
		          	</div>		          			          			          			          	
		        </form>
		       	<div style="float:right;" class="col-md-0">
			        <button type="button" class="btn btn-primary form-btn-w-70" id="btn-save-edit" onclick="saveAppForm()">Save</button>
		      	</div>
		      </div>
		    </div>
		</div>
	</c:otherwise>
</c:choose>

<script src="<%=basePath%>/js/constant.js"></script>
<script>
$(function(){
	Commons.bindDateControl();
	Commons.appendRedAsterisk();
	blobData=null;
	filename=null;	
	if($('#e_imagePath').val() == ''){
		$('#e_iconPath').attr("src",'<%=basePath%>/image/addPic.png');
	}
});  
var load = function(event){
	var load = document.getElementById("path");
	var index=load.value.lastIndexOf("\_");
	load.value=load.value.substring(index+1,load.value.length);
	return load.value;
}

var blobData, filename;
var change = function (event) {
	file = event.target.files[0];
	var fileReader = new FileReader();
	fileReader.readAsArrayBuffer(file);
	var toBlob = function(a){
        return new Blob([a],{type:file.type})
    }
	
	fileReader.onload = function(){
		var result = this.result;
        var blob = new Blob([result],{type:file.type});
        blobData = blob;
        filename = file.name;
        saveUpload();
    }
}

var $errorArea = $('#modalErrorArea');
var appForm = function() {
	this.developer = '';
	this.typeId = '';
	this.appName = '';
	this.verFilePath = '';
	this.versionName = '';
	this.appAbstract = '';
	this.appId = '';
	this.appVersionId = '';
	this.appIsPublic = '';
};
var saveUpload = function() {
	$("#btn-save-edit").attr('disabled',true);
	$("#upload")[0].style.display = "none";
	$("#uploading")[0].style.display = "block";
	var tf = new appForm();
	tf.appId = $('#e_uuId').val();
	tf.appDeviceTypeId = $('#e_deviceTypeId').val();
	if( filename == null){
		tf.verFilePath = load();
	}else{
		tf.verFilePath = filename;
	}
	var formData = new FormData();
	formData.append("file", blobData);
	formData.append("form", JSON.stringify(tf));

	try {
		$.ajax({
			url: '<%=basePath%>/app/uploadFile',
			cache: false,
			type: 'POST',
			async:true,
            processData:false,
            contentType:false,
			data: formData,
			success: function (data) {
				$("#btn-save-edit").attr('disabled',false);
				var json = JSON.parse(data);
				if(200 == json.result) {
					Commons.showContent('<%=basePath%>' + json.forward);
					$('#e_hashcode').attr("value",json.message.fileHashCode);
					$('#e_appId').attr("value",json.message.configAppId);
					$('#e_versionName').attr("value",json.message.configVersionName);
					$('#e_verFilePath').attr("value",json.message.filePath);
					$('#e_appName').attr("value",json.message.configAppName);
					$('#e_iconPath').attr("src",'<%=ippath%>'+json.message.iconPath);
					$('#e_imagePath').attr("value",json.message.iconPath);
					$('#e_appSize').attr("value",json.message.fileSize);
					if($('#e_imagePath').val() == ''){
						$('#e_iconPath').attr("src",'<%=basePath%>/image/addPic.png');
					}
					blobData="";
					filename="";
					$errorArea.css("display", "none");
				} else if(401 == json.result) {
					var errorContent = '';
					for (var i in json.message) {
						errorContent += '<div>' + json.message[i] + '</div>';
					}
					$errorArea.empty().append(errorContent);
					$errorArea.css("display", "block");
					$("#btn-save-edit").attr('disabled',true);
				} else if(500 == json.result) {
					var errorContent = '';
					for (var i in json.message) {
						errorContent += '<div>' + json.message[i] + '</div>';
					}
					$errorArea.empty().append(errorContent);
					$errorArea.css("display", "block");
					$("#btn-save-edit").attr('disabled',true);
				}
				$("#upload")[0].style.display = "block";
				$("#uploading")[0].style.display = "none";
			},
			 error: function (jqXHR, textStatus, errorThrown) {
		            /*错误信息处理*/
		    }
		});
	} catch(e) {
		console.log(e);
	}
};
//image
function changeImg(e,name) {
	$errorArea.empty();
	$errorArea.css("display", "none");
    for (var i = 0; i < e.target.files.length; i++) {
        var file = e.target.files.item(i);
        if (!(/^image\/.*$/i.test(file.type))) {
        	//isImage
        	$errorArea.empty().append('<div>' + 'You can upload pictures only.' + '</div>');
			$errorArea.css("display", "block");
        } else if(file.size > (500*1024)){
        	//image size
        	$errorArea.empty().append('<div>' + 'The picture sizes should not exceed 500KB.' + '</div>');
			$errorArea.css("display", "block");
        } else{ 
        	//FileReader API
            var freader = new FileReader();
            blobData = file;
            freader.readAsDataURL(file);
            freader.onload = function (e) {
            	var result = this.result;
                var blob = new Blob([result],{type:file.type});
                $("#e_iconPath").attr("src",e.target.result); 
            }         
        }
    }
}
var saveAppForm = function() {
	$("#btn-save-edit").attr('disabled',true);
	var tf = new appForm();
	tf.appId = $('#e_uuId').val();
	tf.appDeviceTypeId = $('#e_deviceTypeId').val();
	tf.versionName = $('#e_versionName').val();
	tf.typeId = $('#e_appTypeId').val();
	tf.appName = $('#e_appName').val();
	tf.appAbstract = $('#e_appAbstract').val();
	tf.appIsPublic = $('#e_isPublicName').val();
	tf.hashcode = $('#e_hashcode').val();
    tf.verFilePath = $('#e_verFilePath').val();
    tf.imagePath = $('#e_imagePath').val();
    tf.appIdCustom = $('#e_appId').val();
    tf.appSize = $('#e_appSize').val();
    tf.appVersionId = $('#e_appVersionId').val();
	var formData = new FormData();
	formData.append("file", blobData);
	formData.append("form", JSON.stringify(tf));

	try {
		$.ajax({
			url: '<%=basePath%>/app/saveInfo',
			cache: false,
			type: 'POST',
			async:true,
            processData:false,
            contentType:false,
			data: formData,
			success: function (data) {
				var json = JSON.parse(data);
				if(200 == json.result) {
					Commons.showContent('<%=basePath%>/' + json.forward+'?appId='+json.message.appId);
					blobData=null;
				} else if(401 == json.result) {
					$("#btn-save-edit").attr('disabled',false);
					var errorContent = '';
					for (var i in json.message) {
						errorContent += '<div>' + json.message[i] + '</div>';
					}
					$errorArea.empty().append(errorContent);
					$errorArea.css("display", "block");
				} else {
					$("#btn-save-edit").attr('disabled',false);
					var errorContent = '';
					for (var i in json.message) {
						errorContent += '<div>' + json.message[i] + '</div>';
					}
					$errorArea.empty().append(errorContent);
					$errorArea.css("display", "block");
				}
			},
			 error: function (jqXHR, textStatus, errorThrown) {
		            /*错误信息处理*/
		    }
		});
	} catch(e) {
		console.log(e);
	}
};
var upload = function() {
	$('#path').click();
};
</script>