<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
String ippath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
%>
<style type="text/css">
.btn-float{
	float:left;
	margin-left:5px;
}
</style>

<!-- navigationDialog dialog -->
<jsp:include page="../navigationDialog.jsp" flush="true"></jsp:include>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
	<div id="page-content" class="index-page container" style="background:#F0F0F0;">
	<div class="row">
		<div class="col-md-12 col-md-offset-12">
			<div id="main-content" style="min-height:520px;height:100%;width:100%;">
				<article>
					<div class="art-content" style="min-height:480px;height:100%;width:auto;margin-left:45px;margin-right:45px;word-break: break-all; word-wrap:break-word;">
						<input id="t_typeId" type="hidden" value="${appInfo.typeId}">
						<input id="t_appId" type="hidden" value="${appInfo.appId}">
						<input id="t_filePath" type="hidden" value="${appInfo.verFilePath}">
						<input id="iconPath" type="hidden" value="${appInfo.imagePath}">
						<div  style="display:none">
						    <input id="t_developer" type="text" value="${appInfo.developer}">
						</div>
						
						<h2>${appInfo.appName}</h2>
						<table>
							<tr>
								<td rowspan="3" valign="top" style="width:130px"><img id="imagePath" src="${appInfo.imagePath}" style="width:120px;height:120px"/></td>
								<td>By ${appInfo.developerName}&nbsp;${date}<br>${appInfo.versionName}<br>${appInfo.typeName}<br>${appInfo.appDeviceTypeName}<br></td>
							</tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
						</table><input id="iconPath" type="hidden" value="${appInfo.imagePath}">
						<div id="appAbstract" class="col-md-12" style="margin-left:-15px">${appInfo.appAbstract}</div><br>
						<div style="color:#ADADAD; margin: 0px 0px 10px 0px;">MD5:<a>${appInfo.hashcode}</a></div>
						<div class="btn-float">
							<a href="<%=basePath%>/app/download?appId=${appInfo.appId}&typeId=${appInfo.typeId}&filePath=${appInfo.verFilePath}&token=${token}">
							<button type="button" class="btn btn-skin"> DownLoad</button></a>
						</div>
						<div id="modifyBtn" style="display:none;" class="btn-float">
							<a onclick="Commons.showContent('<%=basePath%>/app/initInfo?appId=${appInfo.appId}&modifyFlag=2')" >
							<button type="button" id="modify" class="btn btn-skin"> &nbsp;&nbsp;&nbsp; Modify &nbsp;&nbsp;&nbsp;</button></a>
						</div>
                        </div>
					</div>
				</article>
			</div>
		</div>
    </div>
	</div>
	</c:otherwise>
</c:choose>
<script src="<%=basePath%>/js/constant.js"></script>
<script>
$(function() {
	var developer = $('#t_developer').val();
	if($('#auId').val() == 1 || developer == $('#userId').val()){
		$("#modifyBtn")[0].style.display='block';
    } 
	if($('#iconPath').val() == ''){
		$('#imagePath').attr("src",'<%=basePath%>/image/defaultAppIcon.png');
	}
	// TODO DJ 暂时放置，等上传功能好用之后再对应，猜想textarea上传后，数据库会把回车空格之类的给替换掉。
	var abstractSubStr = document.getElementById("appAbstract").innerHTML; 
	var reg=new RegExp("\r\n","g"); 
    var reg1=new RegExp("\n","g"); 
	var abstractSubStr= abstractSubStr.replace(reg,"<br>");
	abstractSubStr= abstractSubStr.replace(reg1,"<br>");
	document.getElementById("appAbstract").innerHTML = abstractSubStr; 
}); 

</script>