<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path;
	String ippath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
%>
<style type="text/css">
.vBtn {
	font-size: 30px;
	margin-top: -40px;
	float: right;
	text-align: center;
	background: none;
	border: none;
	color: gray;
}

.div3D {
	box-shadow: 0px 0px 4px #000;
	background: #FFFFFF;
	border-radius: 5px;
}

.hr {
	height: 1px;
	border: none;
	border-top: 1px solid #E0E0E0;
}

.cr-link-row {
	cr-link-row-icon-width: 40px;
	border-top: var(- -settings-separator-line);
}
</style>

<div class="alert alert-secondary container" role="alert">
	<div style="display: inline-block">Setting</div>
</div>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">${modelErrors}</div>
	</c:when>
	<c:otherwise>
		<input type="hidden" id="appTypeList" value="${appTypeList}">
		<div id="page-content" class="index-page container">
			<div style="margin-left: 100px;">
				<div class="col-md-4" style="margin-left: -50px; font-size: 25px;">
					<p>Application Type</p>
					<button id="downShwoBtn"  class="vBtn fa fa-chevron-circle-up" type="button" onclick="typeBtn()"></button>
					<button id="downCloseBtn"  style="display: none" class="vBtn fa fa-chevron-circle-down" type="button" onclick="typeBtn()"></button>
				</div>
				<br>
				<ul>
					<div id="type" class="col-md-9 div3D">
						<hr style="height: 1px; border: none; border-top: 1px solid #FFFFFF;" />
						<c:forEach var="list" items="${appTypeList}">
							<option value="${list.value}">${list.label}</option>
							<div style="float: right; margin-top: -27px;">
								<button type="button" style="color: #1a73e8; background: #FFFFFF; border: solid 1px #D0D0D0; border-radius: 5px;"
									data-toggle="modal" data-target="#popTypeModal" onclick="setValues('0100', '${list.value}', '${list.label}')">Modify</button>
							</div>
							<hr class="hr" />
						</c:forEach>
						<button type="button" class="btn btn-info" data-toggle="modal"
							data-target="#popTypeModal" onclick="setValues('0100', '', '')">Add Type</button>
						<br>
						<br>
					</div>
				</ul>
			</div>
			<br>
			<br>
			<div style="margin-left: 100px;">
				<div class="col-md-5" style="margin-left: -50px; font-size: 25px;">
					<p>Application Device Type</p>
					<button id="downDeviceCloseBtn" style="display: none" class="vBtn fa fa-chevron-circle-down" type="button" onclick="deviceTypeBtn()"></button>
					<button id="downDeviceShwoBtn" class="vBtn fa fa-chevron-circle-up" type="button" onclick="deviceTypeBtn()"></button>
				</div>
				<br>
				<ul>
					<div id="deviceType" class="col-md-9 div3D">
						<hr style="height: 1px; border: none; border-top: 1px solid #FFFFFF;" />
						<c:forEach var="list" items="${appDeviceTypeList}">
							<option value="${list.value}">${list.label}</option>
							<div style="float: right; margin-top: -27px;">
								<button type="button"
									style="color: #1a73e8; background: #FFFFFF; border: solid 1px #D0D0D0; border-radius: 5px;"
									data-toggle="modal" data-target="#popTypeModal"
									onclick="setValues('0101', '${list.value}', '${list.label}')">Modify</button>
							</div>
						<hr class="hr">
						</c:forEach>
						<button type="button" class="btn btn-info" data-toggle="modal" data-target="#popTypeModal" onclick="setValues('0101', '', '')">
						Add Device Type</button>
						<br>
						<br>
					</div>
				</ul>
			</div>
			<%-- <br><a href="" onclick="Commons.showContent('<%=basePath%>/app/')" style="margin-left: 780px;">
				<button type="button" class="btn btn-primary">Back</button>
			</a><br> --%>
			<br>
		</div>
		<!-- show window -->
		<!-- modal -->
		<div class="modal fade" id="popTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="typeModalTitle">AddType</h4>
					</div>
					<div class="modal-body">
						<div class="alert alert-danger" style="display:none" id="typeModalErrorArea" role="alert"></div>
						<label id="typeModalDescribe">Please input the type name you want to add.</label> 
						<input type="hidden" id="m_type" name="m_type" value=""></input>
					    <input type="hidden" id="m_value" name="m_value" value=""></input> 
					    <input type="hidden" id="m_label" name="m_label" value=""></input>
						<div class="form-group fond-size">
							<!-- <label id="typeLabel">Type</label> -->
							<input class="form-control login-input style-input" id="typeName" name="s_typeLabel" type="text">
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" style="width: 100px;" data-dismiss="modal">Close</button>
						<button type="button" id="submitBtn" class="btn btn-primary" style="width: 100px;" onclick="submitType()">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>

<script src="<%=basePath%>/js/constant.js"></script>
<script>
$(function() {
	$("#submitBtn").attr('disabled',true);
	$("#popTypeModal").on("hide.bs.modal", function() {
	    $(this).removeData("bs.modal");
	    var div = document.getElementById("typeModalErrorArea");
	    div.innerHTML = "";
        div.style.display="none";
	});
	/* $("#type").parent().parent().find("ul").toggle();
	$("#deviceType").parent().parent().find("ul").toggle(); */
	
	$('#typeName').bind('input propertychange', function() {
		$("#submitBtn").attr('disabled',false);
    });
	$('#typeName').bind('input propertychange', function() {
		$("#submitBtn").attr('disabled',false);
    });
}); 

/* A function to show a modal window and set the detail style of this modal window. 
   type:0100(type),0101(deviceType)
   typeValue: label name, null(add), value exists(modify) 
   typeLabel: label name */
function setValues(type, typeValue, typeLabel){
	document.getElementById("m_type").value = type;
	document.getElementById("m_value").value = typeValue;
	document.getElementById("m_label").value = typeLabel;
	
	var m_type = document.getElementById("m_type").value;
	var m_typeValue = document.getElementById("m_value").value;
	var m_typeLabel = document.getElementById("m_label").value;
	
	var typeModalTitle = document.getElementById("typeModalTitle");
	var typeModalDescribe = document.getElementById("typeModalDescribe");
	var typeLabel = document.getElementById("typeLabel");
	var typeName = document.getElementById("typeName");
	
	//type
	if('0100' == m_type){
		//type-modify
		//typeLabel.innerHTML = "Type";
		if("" != m_typeValue){
			typeModalTitle.innerHTML = "Modify Type";
			typeModalDescribe.innerHTML = "Please input the type you want to modify.";
			typeName.value = m_typeLabel;
		}
		//type-add
		else if("" == m_typeValue) { 
			typeModalTitle.innerHTML = "Add Type";
			typeModalDescribe.innerHTML = "Please input the type you want to add.";
			typeName.value = "";
		}
	}
	//device type
	else if('0101' == m_type){ 
		//device type-modify
		//typeLabel.innerHTML = "DeviceType";
		if("" != m_typeValue){ 
			typeModalTitle.innerHTML = "Modify Device Type";
			typeModalDescribe.innerHTML = "Please input the device type you want to modify.";
			typeName.value = m_typeLabel;
		}
		//device type-add
		else if("" == m_typeValue) { 
			typeModalTitle.innerHTML = "Add Device Type";
			typeModalDescribe.innerHTML = "Please input the device type you want to add.";
			typeName.value = "";
		}
	}
}

function submitType(){
	var type = document.getElementById("m_type").value;
	var typeValue = document.getElementById("m_value").value;
	var typeLabel = document.getElementById("typeName").value;
	var div = document.getElementById("typeModalErrorArea");
	
	if("" == typeLabel || null == typeLabel)
	{
		var errorContent = '';
		errorContent = "Type name is required.";
		div.innerHTML = errorContent;
        div.style.display="block";
        return;
	}
	try {
		$.ajax({
			url: '<%=basePath%>/app/saveType',
			type: 'POST',
			data: { "type":type, "typeValue":typeValue, "typeLabel":typeLabel},
			success: function (data) {
				var json = JSON.parse(data);
				if(200 == json.result) {
 					$('#popTypeModal').modal('hide');
					$('.modal-backdrop').remove();
					$('body').removeClass('modal-open'); 
					Commons.showContent('<%=basePath%>/app/type');
				} else if(401 == json.result) {
					$("#submitBtn").attr('disabled',true);	
					var errorContent = '';
					for (var i in json.message) {
						errorContent += json.message[i];
					}
					div.innerHTML = errorContent;
			        div.style.display="block";
				} else {
					$("#submitBtn").attr('disabled',true);	
					var errorContent = '';
					for (var i in json.message) {
						errorContent += '<div>' + json.message[i] + '</div>';
					}
					div.innerHTML = errorContent;
			        div.style.display="block";
				}
			},
			 error: function (jqXHR, textStatus, errorThrown) {
		            /*错误信息处理*/
		    }
		});
	} catch(e) {
		console.log(e);
	}
}

var typeNum = 0;
function typeBtn(){
	$("#type").parent().parent().find("ul").slideToggle();
	typeNum++;
	if(typeNum%2 == 1){
		$("#downShwoBtn").css("display","none");
		$("#downCloseBtn").css("display","block");
	}else{
		$("#downShwoBtn").css("display","block");
		$("#downCloseBtn").css("display","none");
	}
}

var deviceTypeNum = 0;
function deviceTypeBtn(){
	$("#deviceType").parent().parent().find("ul").slideToggle();
	deviceTypeNum++;
	if(deviceTypeNum%2 == 1){
		$("#downDeviceShwoBtn").css("display","none");
		$("#downDeviceCloseBtn").css("display","block");
	}else{
		$("#downDeviceShwoBtn").css("display","block");
		$("#downDeviceCloseBtn").css("display","none");
	}
}
</script>
