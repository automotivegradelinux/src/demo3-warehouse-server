<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
String ippath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
%>
<style type="text/css">
	.triangle{width:0;height:0;border-right:25px solid transparent;border-left:25px solid #0080FF;border-bottom:25px solid transparent;border-top:25px solid transparent;}
    .rectangle{height:50px;font-size:20px;background:#0080FF;color:white}
    .box{height:50px;border-style:solid; border-width:1px; border-color:#F0F0F0;text-align:center;line-height:50px;color:white;}
    .center_div{border-style:solid; border-width:1px; border-color:white;border-top:none;border-bottom:none;}
    .space{background:#FFFFFF;width:5px;position:relative;margin-left:-5px;}
    .create-schedule{border:2px solid #E0E0E0;}
    .create-scheduling{border:2px solid #29e62d;}
</style>
<!-- navigationDialog dialog -->
<jsp:include page="../navigationDialog.jsp" flush="true"></jsp:include>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
	<div id="page-content" style="height:auto;">
	<div class="col-md-12 box">
		<div class="form-group row">
			 <img src="image/insertAppSubmit.PNG" style="height:100%;width:100%"/>
		</div>
		<div class="form-group row" style="margin-top:-15px">
		     <div class="col-md-4 create-schedule"></div> 
		     <div class="col-md-4 create-schedule"></div> 
		     <div class="col-md-4 create-scheduling"></div>
		</div>
    </div>
	<div class="row">
		<div class="col-md-12 col-md-offset-12">
			<div id="main-content" style="min-height:520px;height:100%;width:100%;">
				<article>
					<div class="art-content" style="min-height:480px;height:100%;width:auto;margin-left:45px;margin-right:45px;word-break: break-all; word-wrap:break-word;">
						<input id="t_typeId" type="hidden" value="${appInfo.typeId}">
						<input id="t_appId" type="hidden" value="${appInfo.appId}">
						<input id="t_filePath" type="hidden" value="${appInfo.verFilePath}">
						<input id="t_developer" type="hidden" value="${appInfo.developer}">
						<input id="iconPath" type="hidden" value="${appInfo.imagePath}">
                        <br>
						<p style="font-size:25px;margin-left:-20px;margin-top:10px;">Submission of audit</p>
						<h4>${appInfo.appName}</h4>
						<table>
							<tr>
								<td rowspan="3" valign="top" style="width:130px"><img id="imagePath" src="${appInfo.imagePath}" style="width:120px;height:120px"/></td>
								<td>By <a style="color:#00D096" href="#">${appInfo.developerName}</a>&nbsp;${date}<br>${appInfo.versionName}<br>${appInfo.typeName}<br>${appInfo.appDeviceTypeName}<br>${appInfo.appIdCustom}</td>
							</tr>
							<tr><td></td></tr>
							<tr><td></td></tr>
						</table>
						<div id="appAbstract" class="col-md-12" style="margin-left:-15px">${appInfo.appAbstract}</div>
						<div style="float:right;margin-top:30px;" class="col-md-0">
			                <button type="button" class="btn btn-primary" id="btn-save-edit" onclick="saveAppForm()">SUBMIT</button>
		      	        </div>
					</div>
				</article>
			</div>
		</div>
    </div>
	</div>
	</c:otherwise>
</c:choose>
<script src="<%=basePath%>/js/constant.js"></script>
<script>
$(function() {
	<%-- if($('#iconPath').val() == ''){
		$('#imagePath').attr("src",'<%=basePath%>/image/defaultAppIcon.png');
	} --%>
	// TODO DJ 暂时放置，等上传功能好用之后再对应，猜想textarea上传后，数据库会把回车空格之类的给替换掉。
	var abstractSubStr = document.getElementById("appAbstract").innerHTML; 
	var reg=new RegExp("\r\n","g"); 
    var reg1=new RegExp("\n","g"); 
	var abstractSubStr= abstractSubStr.replace(reg,"<br>");
	abstractSubStr= abstractSubStr.replace(reg1,"<br>");
	document.getElementById("appAbstract").innerHTML = abstractSubStr; 	
}); 
var saveAppForm = function(){
    var appId = $('#t_appId').val();
	$.ajax({
		url: '<%=basePath%>/app/check?appId='+appId,
		cache: false,
		type: 'GET',
		async:true,
        processData:false,
        contentType:false,
		data: '',
		success: function (data) {
			var json = JSON.parse(data);
			if(200 == json.result) {
				Commons.showContent('<%=basePath%>/' + json.forward);
			} else if(401 == json.result) {
				$errorArea.empty().append('<div>' + json.message[i] + '</div>');
				$errorArea.css("display", "block");
			} else {
				var errorContent = '';
				for (var i in json.message) {
					errorContent += '<div>' + json.message[i] + '</div>';
				}
				$errorArea.empty().append(errorContent);
				$errorArea.css("display", "block");
			}
		},
		 error: function (jqXHR, textStatus, errorThrown) {
	            /*错误信息处理*/
		}
	});
};
</script>