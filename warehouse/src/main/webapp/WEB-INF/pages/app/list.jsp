<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%

String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
String ippath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
%>
<style type="text/css">
	div.dataTables_wrapper div.dataTables_info {
 	padding-top: 0.3em;
  	white-space: nowrap;
  	margin-left:120px;
}
	a.page-link{
	height:calc(1.8125rem + 4px);
	margin-left:-5px;
	}

</style>
<div class="alert alert-secondary container" role="alert">
	<div style="display:inline-block">App</div>
</div>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
		<div class="card mb-3">
			<div class="card-header">
				<div class="">
					<div class="alert alert-danger" id="modalErrorArea" role="alert"<c:choose>
						<c:when test="${not empty modelErrors}">style="display: block"</c:when>
						<c:otherwise>style="display: none"</c:otherwise>
						</c:choose>>
						<c:if test="${not empty modelErrors}">
							&nbsp;${modelErrors}
						</c:if>
					</div>
					<input type="text" class="form-control" id="t_filePath" size="16" name="t_filePath" style="display:none;">
					<div class="row" >
						<div class="col">
							<div class="form-group">
								<label class="control-label" for="t_appTypeId">AppType</label>
						        <select id="t_appTypeId" class="form-control">
	                                <c:forEach var="list" items="${appTypeList}">
	                                     <option value="${list.value}">${list.label}</option>
	                                </c:forEach>
	                            </select>
							</div>
						</div>
						<div class="col" >
							<div class="form-group">
								<label class="control-label" for="t_appDeveloper">DeviceType</label>
								   <select id="e_deviceTypeId" class="form-control">
	                                   <c:forEach var="list" items="${appDeviceTypeList}">
	                                       <option value="${list.value}">${list.label}</option>
	                                   </c:forEach>
	                            </select>
							</div>
						</div>	
						<div class="col">             
						    <div style="position:absolute;right:20px;top:40px;cursor:pointer;display:none;" class="input_clear">		
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>	    
						    </div>    
						    <label class="control-label" for="t_keyWord">KeyWord</label>         
						    <input type="text" class="form-control style-height" id="t_keyWord"  size="16" name="t_keyWord" placeholder="please enter"> 
						</div>
						<div class="col" style="visibility:hidden">             
						    <div style="position:absolute;right:20px;top:40px;cursor:pointer;display:none;" class="input_clear">		
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>	    
						    </div>    
						    <label class="control-label" for="t_appName">Developer</label>         
						    <input type="text" class="form-control style-height" id="t_appDeveloper"  name="t_appDeveloper" size="16" placeholder="please enter"> 
						</div>				
					</div>
				</div>
				<div class="row">
					<div  class="col" style="text-align:right;">
					<c:if test="${fn:contains(menuPathString, 'app/insert')}">
						<button type="button" class="btn btn-primary pl20 pr20" id="btn-save-edit" onclick="Commons.showContent('<%=basePath%>/app/initCreate')">Create</button>
					</c:if>
						<button type="button" class="btn btn-success pl20 pr20 ml10" id="listSearch" onclick="search()">Search</button>&nbsp;
					    <button id="gridView" type="button" class="btn btn-info pl20 pr20 ml10"  onclick="chageView(1)" style="float:right">gridView</button>
					    <button id="listView" type="button" class="btn btn-info pl20 pr20 ml10"  onclick="chageView(0)" style="float:right">listView</button>
					</div>
				</div>
			</div>
			<div class="card-body" style="margin-left:12px;margin-right:12px;">
				<div class="table-responsive" id="div-table-container">
					<table id="example" cellspacing="0" style="width:100%;">
						<thead>
						</thead>
					</table>
					<div style="height:20px;"></div>
				</div>
				<div id="grid">
				<div id="chunk" class="row"></div>
				</div>
				<div style="display: none;" id="page">
					<div class="row">
						<div class="col-md-6" style="text-align:right">
							<button id="last" type="button" class="btn btn-primary"
								onclick="chunk(0)"><h4 style="color:white"><</h4></button>
						</div>
						<div class="col-md-6">
							<button id="next" type="button" class="btn btn-primary"
								onclick="chunk(1)"><h4 style="color:white">></h4></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</c:otherwise>
</c:choose>
<script src="<%=basePath%>/js/main.js"></script>
<script src="<%=basePath%>/js/jquery.cookie.js"></script>
<script>

$(function() {
	$("#listView").css('display','none');
	$("input").focus(function(){      
		$(this).parent().children(".input_clear").show();  
	});  
	$("input").blur(function(){      
		if($(this).val()==''){          
			$(this).parent().children(".input_clear").hide();      
			}  
	});  
	$(".input_clear").click(function(){      
		$(this).parent().find('input').val('');      
		$(this).hide();  
	}); 
	
	$(".card-body").css({
		minHeight:($(window).height()-500)
	})
	Commons.bindDateControl();
	Commons.appendRedAsterisk();
	init();
});

var $table = $('#example');
var $errorArea = $('#modalErrorArea');
var queryParam = function(param) {
	param.orderColumn = param.order[0].column;
	param.orderDir = param.order[0].dir;
	param.keyWord = $('#t_keyWord').val();
	param.typeId = $('#t_appTypeId').val();
	param.deviceTypeId = $('#e_deviceTypeId').val();
	param.filepath = $('#t_filePath').val();
	return param;
};

 var strSub = function(str, subLen){
	if(str == null){
		return "";
	} 
	var subStr = str;
	var len = 0;  
	for (var i=0; i<str.length; i++) {  
	    if (str.charCodeAt(i)>127 || str.charCodeAt(i)==94) {  
	       len += 2;  
	    } else {  
	       len ++;  
	    }
	    if(len >= subLen){
	    	subStr = str.substring(0, i);
	    	break;
	    }
	}
	return subStr;
}; 

var strName = function(str, subLen){
	if (str == null){
		return "";
	}  
	if (str.length > subLen) {  
		str=str.substring(0,subLen-3);
        var appName = str+"...";
	}else{
		var appName = str;
	}
	return appName;
};
function init(){
    var flag = $.cookie('flag');
	if(flag == 1){
		chunk();
	}else{
		initTable();
	}
}

function search(){
    var flag = getCookie("flag");
	if(flag == 1){
		chunk();
	}else{
		$table.DataTable().ajax.reload();
	}
}

function setCookie(key,value){
	$.cookie(key, value, { expires: 7 ,path:'/'});
}

function getCookie(key){
	return $.cookie(key);
}

function lastPageButton(start){
	if(start == 0){
		$("#last").attr('disabled',true);
	}else{
		$("#last").attr('disabled',false);
	}
}

function nextPageButton(i,length){
	if(i < length){
		$("#next").attr('disabled',true);
	}else{
		$("#next").attr('disabled',false);
	}
}
var check = 0;
function chageView(value){
	if(value == 0){
	    if(check == 1){
	        $table.DataTable().ajax.reload();
	    }else{
	        initTable();
	    }
	}else{
		chunk();
	}
}

//chunkList
var nowStart = 0;
function chunk(page){
	var start = 0;
	var length = 48;
	// last page
	if(page == 0){
		start = nowStart - length;
	}
	// next page
	if(page == 1){
		start = nowStart + length;
	}
	nowStart = start;

	$("#div-table-container").css('display','none');
	$("#gridView").css('display','none');
	$("#grid").css('display','block');
	$("#page").css('display','block');
	$("#listView").css('display','block');
	setCookie("flag",1);
	var keyWord =  $('#t_keyWord').val();
	var appTypeId =  $('#t_appTypeId').val();
	var deviceTypeId =  $('#e_deviceTypeId').val();

	$.ajax({
		url: '<%=basePath%>/app/search',
		data: { "draw":"1","length":length,"start":nowStart,"orderColumn":"1","orderDir":"asc","typeId":appTypeId,"deviceTypeId":deviceTypeId,"keyWord":keyWord},
		type: 'GET',
		dataFilter: function(data){
			$errorArea.empty();
        	$errorArea.css("display", "none");
        	$("#chunk").empty();
			var json = $.parseJSON(data);
			debugger;
            if (json.statusCode && json.statusCode != null) {
            	$errorArea.empty().append('<div>' + json.statusText + '</div>');	            	
            	$errorArea.css("display", "block");
            }
            var i = 0;
			$.each(json.data, function(index, item){
				i++;
				var imagePath = '<%=ippath%>'+item.imagePath;
				if(item.imagePath == ''){
				    imagePath = '<%=basePath%>/image/defaultAppIcon.png';
				}
				var appName = strName(item.appName, 16);
				var a = '<div class="col-md-3" style="height:340px;padding: 12px;">'
				           +'<div onclick="Commons.showContent(\'<%=basePath%>/app/more?appId='+ item.appId+ '\')" style="border-radius:3px;padding:18px; border:1px solid #BEBEBE;background:#F0F0F0; cursor:pointer;box-shadow: darkgrey 1px 1px 30px 0px">'
						       + '<div style="height:190px;width:100%;border:1px solid #BEBEBE">'
						           + '<img style="height:100%; width:100%;border-radius:3px;box-shadow:0px 0px 10px 0px darkgrey inset" src='+imagePath+'>'
						       +'</div>'
						       + '<h6 style="margin-bottom:5px; margin-top:10px">'+ appName + '</h5>'
						       + '<p style="margin-bottom:2px; color:#111">'+ item.appDeviceTypeName + '</p>'
						       + '<p style="margin-bottom:0px; color:#111">'+ item.typeName+ '</p>'
						   +'</div>'
						+'</div>';
				$("#chunk").append(a);
		    });
			lastPageButton(start);
			nextPageButton(i, length)
		}
	})
}
function initTable() {
	check = 1;
	$table.DataTable({
		order: [[ 1 , 'asc' ]],
		displayLength:50,
		ajax: {
			url: '<%=basePath%>/app/search',
			data: queryParam,
			type: 'GET',
			dataFilter: function(data){
				$("#div-table-container").css('display','block');
			    $("#gridView").css('display','block');
			    $("#grid").css('display','none');
			    $("#page").css('display','none');
			    $("#listView").css('display','none');
			    setCookie("flag",0);
			    
            	$errorArea.empty();
            	$errorArea.css("display", "none");
				var json = $.parseJSON(data);
				debugger;
	            if (json.statusCode && json.statusCode != null) {	            	
	            	$errorArea.empty().append('<div>' + json.statusText + '</div>');	            	
	            	$errorArea.css("display", "block");
	            }
	            return data; // return JSON string
	        }
	    },
	    columns: [
			{
				className : 'td-operation',
				data: null,
				defaultContent:'',
				orderable : false,
				width : '100%',
				render: function(data, type, row, meta) 
				{
					var deleteBtn = '';
					var modifyBtn = '';
					var imagePath = '<%=ippath%>' + data.imagePath;
					if($('#auId').val() == 1 || ($('#auId').val() == 2 && data.developer == $('#userId').val())){
						 deleteBtn = '<a href="javascript:void(0)" style="margin-left:0.5em" class="data-delete" data-data-id="'+data.appId+'">'
						              +'<button type="button" style="margin-right:auto;" class="btn btn-skin">&nbsp&nbsp&nbsp&nbspDelete&nbsp&nbsp&nbsp&nbsp</button></a>';
						 modifyBtn = '<a style="margin-left:0.5em" onclick="Commons.showContent(\'<%=basePath%>/app/initInfo?appId='+data.appId+'&modifyFlag='+1+'\')" >'
						              +'<button type="button" class="btn btn-skin"> &nbsp;&nbsp;&nbsp;&nbsp;Modify&nbsp;&nbsp;&nbsp;&nbsp;</button></a>';
					}
					//default image
					if(data.imagePath == ''){
						imagePath = '<%=basePath%>/image/defaultAppIcon.png';
					}
						
					var downloadBtn = '<a href="<%=basePath%>/app/download?appId=' + data.appId + '&typeId=' + data.typeId + '&filePath='+ data.verFilePath + '&token=' + $("#tokenAuthorization").val() 
	                  + '"onclick="">'+'<button type="button" style="margin-right:auto;" class="btn btn-skin">DownLoad</button></a>';
					var abstractSubStr = strSub(data.appAbstract, 300);
		            var date = new Date(data.createDate).format("yyyy-MM-dd");
		            
				    var context = '<article><div style="width:100%; height:100%;border-bottom:1px solid black; word-break: break-all; word-wrap:break-word; padding:20px">'
						+'<div class="art-header"><h2 style="margin-top:10px;">'+data.appName+'</h2></div>'
						+'<div class="info" >By <a>'+data.developerName+'</a> '+ date +'</div>'
						+'<div class="art-content">'
						  +'<img style="height:120px;width:120px" src=' + imagePath + '>'
						  +'<div class="ellipsis4" style="color:#272727">'+data.appAbstract+'</div>'
						  +'<a href="javascript:void(0)" style="color:#1ECD97; padding:8px" onclick="Commons.showContent(\'<%=basePath%>/app/more?appId='+data.appId+'\')">MORE</a>'+'</div>'
						+'<div class="art-header" style="color:#ADADAD;"> MD5:<a>'+data.hashcode+'</a></div>'
						+'<div class="art-footer">' + downloadBtn + deleteBtn + modifyBtn + ' </div>'
						+'</div></article>';
					return context;
				}
			},
			{
				data: null,
				defaultContent:'',
				orderable : false,
				width : '1px',
				 render: function(data, type, row, meta) {
					return null;
				}
			}
		],
		drawCallback: function(settings) {
			TableListDeletePlugin.init('example', '<%=basePath%>/app/search', '<%=basePath%>/app/delete');
            $(window).scrollTop(0);
		}
	});
}
</script>