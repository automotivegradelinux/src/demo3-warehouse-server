<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<style type="text/css">
 	#e_appTypeId {height:auto;}
    .box{height:50px;border-style:solid; border-width:1px; border-color:#F0F0F0;text-align:center;line-height:50px;color:white;}
    .device_type{position: relative;left:7%;width:950px;}
    .style_button{width:300px;height:100px;margin-top:10px;margin-left:10px;white-space: normal;font-size:20px;}
	 .file-btn{z-index:0;filter:alpha(opacity=1);opacity:0.8;}
	 #e_position{z-index:100;}
	 .create-schedule{border:2px solid #E0E0E0;}
	 .create-scheduling{border:2px solid #29e62d;}
</style>
<!-- navigationDialog dialog -->
<jsp:include page="../navigationDialog.jsp" flush="true"></jsp:include>
<c:choose>
	<c:when test="${not empty modelErrors}">
		<div class="alert alert-danger" id="modalErrorArea" role="alert">
			&nbsp;${modelErrors}
		</div>
	</c:when>
	<c:otherwise>
		<div class="card mb-3" style="height:auto;">
		    <div class="col-md-12 box">
		        <div class="form-group row" >
                    <img src="image/insertAppCreate.PNG" style="height:100%;width:100%"/>
		        </div>  
		        <div class="form-group row" style="margin-top:-15px">
		            <div class="col-md-4 create-scheduling"></div> 
		            <div class="col-md-4 create-schedule"></div> 
		            <div class="col-md-4 create-schedule"></div>
		        </div>
		    </div>
			<div class="card-body card-change-password pt0">
			<div class="alert alert-danger" id="modalErrorArea" role="alert"<c:choose>
				<c:when test="${not empty modelErrors}">style="display: block"</c:when>
				<c:otherwise>style="display: none"</c:otherwise>
				</c:choose>>
				<c:if test="${not empty modelErrors}">
					&nbsp;${modelErrors}
				</c:if>
			</div>
			    <p style="font-size:25px;margin-top:10px;">Choose device type</p>
                <div class="form-group row device_type">
                    <c:forEach var="list" items="${appDeviceTypeList}">
	                   <div id="e_deviceTypeId" class="btn btn-info style_button" style="line-height:80px;" onclick="saveAppForm(${list.value})">${list.label}</div> 
	                </c:forEach>
	            </div>
		    </div>
		</div>
	</c:otherwise>
</c:choose>

<script src="<%=basePath%>/js/constant.js"></script>
<script>
var $errorArea = $('#modalErrorArea');
var appForm = function() {
	this.appDeviceTypeId = '';
};

var saveAppForm = function(value) {
	$("#e_deviceTypeId").attr('disabled',true);
	var tf = new appForm();
	tf.appDeviceTypeId = value;
	var formData = new FormData();
	formData.append("form", JSON.stringify(tf));

	try {
		$.ajax({
			url: '<%=basePath%>/app/createApp',
			cache: false,
			type: 'POST',
			async:true,
            processData:false,
            contentType:false,
			data: formData,
			success: function (data) {
				var json = JSON.parse(data);
				if(200 == json.result) {
					Commons.showContent('<%=basePath%>' + json.forward+'?appId='+json.message.appId+'&modifyFlag='+0);
				} else if(401 == json.result) {
					$("#btn-save-edit").attr('disabled',false);
					var errorContent = '';
					for (var i in json.message) {
						errorContent += '<div>' + json.message[i] + '</div>';
					}
					$errorArea.empty().append(errorContent);
					$errorArea.css("display", "block");
				} else{
					var errorContent = '';
					for (var i in json.message) {
						errorContent += '<div>' + json.message[i] + '</div>';
					}
					$errorArea.empty().append(errorContent);
					$errorArea.css("display", "block");
					$("#btn-save-edit").attr('disabled',false);
				}
				
			},
			 error: function (jqXHR, textStatus, errorThrown) {
		            /*错误信息处理*/
			}
		});
	} catch(e) {
		console.log(e);
	}
};

</script>