<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Agl app warehouse</title>
	<link href="<%=basePath%>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<%=basePath%>/css/font-awesome.min.css" rel="stylesheet">
	<link href="<%=basePath%>/css/style.css" rel="stylesheet">
</head>
<body class="bg-dark">
  <input type="hidden" id="basePath" value="<%=basePath%>"/>
  <div class="container">
  	<div class="alert alert-danger" id="loginModalErrorArea" role="alert"<c:choose>
		<c:when test="${not empty modelErrors}">style="display: block"</c:when>
		<c:otherwise>style="display: none"</c:otherwise>
		</c:choose>>
		<c:if test="${not empty modelErrors}">
			<span class="glyphicon glyphicon-exclamation-sign" style="top:2px !important" aria-hidden="true"></span>&nbsp;${modelErrors}
		</c:if>
	</div>
    <div id="loginContainer" class="card card-login mx-auto mt-5 col-5 px-0">
      <label class="card-header style-login" >Agl app warehouse<a href="javascript:history.back(-1)" style="float:right;color:#C0C0C0"><i class="fa fa-close"></i></a></label>
      <div  style="border:solid 20px #FFF">
      	 <div  class="alert alert-danger" style="display:none" id="loginErrorArea" role="alert"></div>
        <form id="loginForm">
          <div class="form-group  fond-size">
            <label for="exampleInputEmail1">Username</label>
            <input class="form-control login-input style-input" id="s_loginId" name="s_username" type="text" placeholder="Username">
          </div>
          <div class="form-group  fond-size">
            <label for="exampleInputPassword1">Password</label>
            <input class="form-control login-input style-input" id="s_userPw" name="s_userPw" type="password" placeholder="Password">
          </div>
          <div><input id="remember" type="checkbox" style="zoom:200%"></div>&nbsp;&nbsp;<div class="style-login1"><label>Remember me</label></div>
          <a class="btn btn-primary btn-block fond-size" href="javascript:void(0)" id="login">LOGIN</a>
        </form>
      </div>
    </div>
  </div>
</body>
<script src="<%=basePath%>/js/jquery/jquery.min.js"></script>
<script src="<%=basePath%>/js/bootstrap-4.0.0-beta/popper/popper.min.js"></script>
<script src="<%=basePath%>/js/bootstrap-4.0.0-beta/bootstrap.min.js"></script>
<script src="<%=basePath%>/js/jquery/jqLoading.min.js"></script>
<script src="<%=basePath%>/js/common.js"></script>
<script type="text/javascript">
var oUser = document.getElementById('s_loginId');
var oPswd = document.getElementById('s_userPw');
var oRemember = document.getElementById('remember'); 
$(function(){
	Commons.appendRedAsterisk();
	$('#login').click(function(){
		if($("input[type='checkbox']").is(':checked')){ 
			//Account cipher is valid for 7 days.
	        setCookie('s_username',oUser.value,7); 
	        setCookie('s_userPw',oPswd.value,7);        
	    } 
		loginAsync();
	});
	
	$('.login-input').keypress(function(e) {
		var e = event ? event : window.event;
		if (e.keyCode === 13) {
			loginAsync();
		}
	});
});

var loginAsync = function() {
	var formData = new FormData();
	formData.append("loginId", $('#s_loginId').val());
	formData.append("userPw",$('#s_userPw').val());
	$.ajax({
		url: '<%=basePath%>/login/signin',
		cache: false,
		type: 'POST',
		async:true,
        processData:false,
        contentType:false,
		data: formData,
		success: function (data, status, xhr) {
			$errorArea = $('#loginErrorArea');
			var json = JSON.parse(data);
			if (json.result == 200) {
				Commons.redirect2($("#basePath").val() + json.forward);
			}else {
				var errorContent = '';
				for (var i in json.message) {
					errorContent += '<div>' + json.message[i] + '</div>';
				}
				$errorArea.empty().append(errorContent);
				var div = document.getElementById("loginErrorArea");
		        div.style.display="block";

			}
		}
	});
}

 window.onload = function(){
	    //If the account password cookie exists, the fill is filled.
	    if(getCookie('s_username') && getCookie('s_userPw')){
	      oUser.value = getCookie('s_username');
	      oPswd.value = getCookie('s_userPw');
	      oRemember.checked = true;
	    }
	    //Remove cookie if unchecked.
	    oRemember.onchange = function(){
	      if(!this.checked){
	        delCookie('s_username');
	        delCookie('s_userPw');
	      }
	    };
	  };
	  
	  //Set cookie.
	  function setCookie(name,value,day){
	    var date = new Date();
	    date.setDate(date.getDate() + day);
	    document.cookie = name + '=' + value + ';expires='+ date;
	  };
	  //Get cookie.
	  function getCookie(name){
	    var reg = RegExp(name+'=([^;]+)');
	    var arr = document.cookie.match(reg);
	    if(arr){
	      return arr[1];
	    }else{
	      return '';
	    }
	  };
	  //delete cookie
	  function delCookie(name){
	    setCookie(name,null,-1);
	  }; 
</script>
</html>