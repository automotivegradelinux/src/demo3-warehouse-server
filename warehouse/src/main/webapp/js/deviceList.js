/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var queryParam = function(param) {
	//debugger;
	param.orderColumn = param.order[0].column;
	param.orderDir = param.order[0].dir;
	param.keyWord = $('#t_keyWord').val();
	param.typeId = TypeId;
	param.deviceTypeId = deviceTypeId;
	param.filepath = $('#t_filePath').val();
	return param;
};

function initAppList(localStr, isListDev) {
	debugger;
	localAppList = eval(localStr);
	$table.DataTable({
		order : [ [ 1, 'asc' ] ],
		lengthChange : false,
		displayLength : 50,
		retrieve: true,
		ajax : {
			url : $("#basePath").val() + '/appDev/search',
			data : queryParam,
			type : 'GET',
			dataFilter : function(data) {
				$errorArea.empty();
				$errorArea.css("display", "none");
				var json = $.parseJSON(data);
				if (json.statusCode && json.statusCode != null) {
					$errorArea.empty().append(
							'<div>' + json.statusText + '</div>');
					$errorArea.css("display", "block");
				}
				return data; // return JSON string
			}
		},
		columns : [
				{
					className : 'td-operation',
					data : null,
					orderable : false,
					defaultContent : '',
					width : '80px',
					render : function(data, type, row, meta) {

						//button show (install update launch) S
						var appStatus = 0;  //button word(install:0,update:2,launch:1)
						var localApp = "";
						if (typeof(localAppList) != "undefined"){
							for (var i = 0; i < localAppList.length; i++) {
								var appInfo = localAppList[i];
								if (appInfo.id.indexOf(data.appIdCustom) == 0){
									if(appInfo.version.indexOf(data.versionName) == 0){
										appStatus = 1;	//launch
									}else{
										appStatus = 2;  //update
									}
									localApp = appInfo;
									break;
								}
							}
						}
						if(appStatus == 2){
							downloadBtnOnclick = 'onclick=updateApp(this,"'+data.appId+'","'+data.typeId+'","'+data.verFilePath+'","'+localApp.id+'")>update</button>';
						}else if(appStatus == 1){
							downloadBtnOnclick = 'onclick=launchApp("'+localApp.id+'")>launch</button>';
						}else{
							downloadBtnOnclick = 'onclick=doDownload(this,"'+data.appId+'","'+data.typeId+'","'+data.verFilePath+'")>install</button>';
						}

						var downloadBtn = '<button class="btn btn-skin button-size" type="button" style="margin-right:auto;padding-left:0px;padding-right:0px;width:150px;color:white"'
								          + downloadBtnOnclick;
					    //button show (install update launch) E

						var imagePath = data.imagePath;
						if (data.imagePath == '') {
							imagePath = $("#basePath").val()
									+ '/image/defaultAppIcon.png';
						}

						var listItemOnclick = 'onclick="Commons.showContent(\''+ $("#basePath").val() + '/appDev/detail?appId='+ data.appId + '&isListDev='+ isListDev + '&appStatus='+ appStatus + '\')"';

						var context =
						   '<div class="row">'
							  +'<div class="col-md-2" style="text-align:center;">'
							  	+'<img style="height:auto;width:100%;" src = "'+imagePath+'">'
							  +'</div>'

							  +'<div class="col-md-7"'+listItemOnclick+'>'
							  	+'<h4 style="color:#00AEAE;">' + data.appName + '</h3>'
							  	+'<div class="font-size"> <strong>Version</strong>:&nbsp'+ data.versionName + '</div>'
							  	+'<p class="font-size ellipsis" style="color:white;"> <strong>Description</strong>:&nbsp' + data.appAbstract + '</p>'
							  +'</div>'

							  +'<div class="col-md-3">'
							  	+'<div style="float:right">'+ downloadBtn + '</div>'
							  +'</div>'
						 +'</div>'
						+ '<hr>';
						return context;
					}
				},
				{
					className : 'td-operation',
					data : null,
					orderable : false,
					defaultContent : '',
					width : '80px',
					render : function(data, type, row, meta) {
						var context = '<a href="javascript:void(0)" style="display:none;" title="detail" onclick="Commons.showContent(\''
								+ $("#basePath").val()
								+ '/app/modify?appId='
								+ data.appId
								+ '\')"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a>';
						if (data.isDel == 0) {
							context += '<a href="javascript:void(0)" style="display:none; title="delete" class="data-delete" data-data-id="'
									+ data.appId
									+ '" style="margin-left:0.5em"><i class="fa fa-trash" aria-hidden="true"></i></a>';
						}
						return context;
					}
				} ],
		drawCallback : function(settings) {
			TableListDeletePlugin.init('example', $("#basePath").val()
					+ '/app/search', $("#basePath").val()
					+ '/app/delete');
			$(window).scrollTop(0);
		}
	});
}

var aglSocket;
function initAglSocket() {
	aglSocket = new AglSocket();
}

var mIslistDev;
function initData(isListDev){
	mIslistDev=isListDev;
	debugger;
	//initAppList("localStr");
	if (typeof(aglSocket) == "undefined"){
		initAppList(undefined, mIslistDev);
	}else{
		this.listReplyok = this.listReplyok.bind(this);
		this.listreplyerr = this.listreplyerr.bind(this);
		aglSocket.listInstalledApps(listReplyok, listreplyerr);
	}
};

var deviceTypeId = "";
var TypeId = "";
var $table = $('#example');
var $errorArea = $('#modalErrorArea');

function listReplyok(obj){
	debugger;
	var localStr = obj["response"];
	initAppList(localStr, mIslistDev);
};

function listreplyerr(obj){
	debugger;
};

function launchApp(LocalAppId) {
	appId = LocalAppId.substring(0,LocalAppId.indexOf("@"));
	var request = new launchRequest(appId, "normal.full");
	aglSocket.launchApp(request);
}

function launchRequest(appid, area){
    this.application_id=appid;
    this.parameter= new requestParameter(area);
}

function requestParameter(area){
	this.area = area;
}

function updateApp(thisObj,appId, typeId, verFilePath,localId){
	debugger;
	aglSocket.uninstallApp(localId);
	doDownload(thisObj,appId, typeId, verFilePath);
}