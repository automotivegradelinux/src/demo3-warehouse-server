/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var TAG_LOG = "AglSocket log -- ";

class AglSocket {
	constructor() {
		this.onopen = this.onopen.bind(this);
		this.onabort = this.onabort.bind(this);
		this.replyerr = this.replyerr.bind(this);
		this.replyok = this.replyok.bind(this);
		this.ws = new afb.ws(this.onopen, this.onabort);
		this.appList = null;
	}

	onopen() {
		this.ws.onevent("*",this.gotevent);
	}

	onabort() {
	}

	imgNotFound() {
		var img = event.srcElement;
		img.src = "images/icon_Default.svg";
		img.onerror = null;
	}

	replyerr(obj) {
		console.log(TAG_LOG + "replyerr:\n" + JSON.stringify(obj));
	}

	replyok(obj) {
		console.log(TAG_LOG + "replyok:\n" + JSON.stringify(obj));
	}

	gotevent(obj) {
		console.log(TAG_LOG + "gotevent:\n" + JSON.stringify(obj));
	}

	launchApp(request) {
		//console.log(TAG_LOG + ", app id is " + appId);
		//this.ws.call("afm-main/start", appId);
		this.ws.call("homescreen/showWindow", request);

	}

	listInstalledApps(listReplyok, replyerr) {
		console.log(TAG_LOG + "list installed apps");
		this.ws.call("afm-main/runnables", "").then(listReplyok, replyerr);
	}

	installApp(path, replyok, replyerr, param) {
		console.log(TAG_LOG + "installApp");
		this.ws.call("afm-main/install", path, param).then(replyok, replyerr);
	}

	uninstallApp(appId) {
		console.log(TAG_LOG + "uninstallApp");
		this.ws.call("afm-main/uninstall", appId).then(this.replyok, this.replyerr);
	}
}

var remoteIp = $("#remoteIp").val();
var param = $("#params").val();
var port = param.split("&")[0].split("=")[1];
var token = param.split("&")[1].split("=")[1];
var afb = new AFB(remoteIp, port, "api", token);
