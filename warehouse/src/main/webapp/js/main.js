/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function($) {
	"use strict"; // Start of use strict
	// Configure tooltips for collapsed side navigation
	$('.navbar-sidenav [data-toggle="tooltip"]').tooltip({
		template: '<div class="tooltip navbar-sidenav-tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
	})
	// Toggle the side navigation
	$("#sidenavToggler").click(function(e) {
		e.preventDefault();
		$("body").toggleClass("sidenav-toggled");
		$(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
		$(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
		if ($(this).find('i').hasClass('float-right')) {
			$(this).find('i').removeClass('float-right');
			$(this).addClass('text-center');
		} else {
			$(this).find('i').addClass('float-right').removeClass('text-center');
			$(this).removeClass('text-center');
		}
	});
	// Force the toggled class to be removed when a collapsible nav link is clicked
	$(".navbar-sidenav .nav-link-collapse").click(function(e) {
		e.preventDefault();
		$("body").removeClass("sidenav-toggled");
	});
	// Prevent the content wrapper from scrolling when the fixed side navigation hovered over
	$('body.fixed-nav .navbar-sidenav, body.fixed-nav .sidenav-toggler, body.fixed-nav .navbar-collapse').on('mousewheel DOMMouseScroll', function(e) {
		var e0 = e.originalEvent, delta = e0.wheelDelta || -e0.detail;
		this.scrollTop += (delta < 0 ? 1 : -1) * 30;
		e.preventDefault();
	});
	// Scroll to top button appear
	$(document).scroll(function() {
		var scrollDistance = $(this).scrollTop();
		if (scrollDistance > 100) {
			$('.scroll-to-top').fadeIn();
		} else {
			$('.scroll-to-top').fadeOut();
		}
	});
	// Configure tooltips globally
	$('[data-toggle="tooltip"]').tooltip()
	// Smooth scrolling using jQuery easing
	$(document).on('click', 'a.scroll-to-top', function(event) {
		var $anchor = $(this);
		$('html, body').stop().animate({
			scrollTop: ($($anchor.attr('href')).offset().top)
		}, 1000, 'easeInOutExpo');
		event.preventDefault();
	});

	$.extend( true, $.fn.dataTable.defaults, {
    	processing: true,
    	searching: false,
    	serverSide: true,
		language: {url: $('#basePath').val()+'/js/dataTables-1.10.16/i18n/datatables-ja-JP.json'},
		dom: "t<'row'<'col'l><'col'i><'col'p>>"
	});
})(jQuery); // End of use strict
