/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
$(function() {
	$.ajaxSetup({
	    beforeSend: function(e, xhr, o){
	    	$(this).jqLoading();
	    	var url = xhr.url;
	    	if(url.indexOf("/js/") == -1) {
    			xhr.url = url;
	    	}
		},
		success:function(data){
			debugger;
		},
	    complete: function(xhr,status) {
	    	$(this).jqLoading("destroy");
	    }
	});
});
/**
 * bind date control
 */
function bingDateControl() {
	$('.date_yyyymmdd').datetimepicker({
		language:  'ja',
		weekStart: 0,
		todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0,
/*		startDate:$('#startDate').val(),
		endDate:$('#endDate').val(),*/
	});
}
var Utils = {
		IsEmpty : function(str) {
			if (str == undefined || str == null || (str+"").trim().length == 0) {
				return true;
			}
			return false;
		},
		isNotEmpty : function(str) {
			return !Utils.IsEmpty(str);
		}
};
function add0(m){
	return m<10?'0'+m:m;
};
var Commons = {
		format : function(timeStamp){
		    var time = new Date(timeStamp);
		    var y = time.getFullYear();
		    var m = time.getMonth()+1;
		    var d = time.getDate();
		    return y+'-'+add0(m)+'-'+add0(d);
		},

		showContent : function(url) {
			$.ajax({
				url: url,
				type: 'GET',
				cache: false,
				success: function (data) {
					debugger;
					$('#htmlContent').empty().append(data);
				}
			});
		},
		appendRedAsterisk: function(position) {
			if (position && position == '1') {
				$('.notNull').each(function(){
					$(this).before("&nbsp;<label style='font-weight:700;color:red'>＊</label>");
				});
			} else {
				$('.notNull').each(function(){
					$(this).after("&nbsp;<label style='font-weight:700;color:red'>＊</label>");
				});
			}
		},
		bindDateControl: function() {
			$('.date_yyyymmdd').datetimepicker({
				language:  'ja',
				weekStart: 0,
				todayBtn:  1,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				minView: 2,
				forceParse: 0
			});
		},
		/**
		 * @param url
		 *            RequestMapping
		 * @param params
		 *            array [index0： name, index1： value]
		 */
		redirect2: function(url, params) {
			var form = $('<form></form>');
			form.attr('action', url);
			form.attr('method', 'post');
			form.css('display', 'none');
			if(typeof(params) != 'undefined' && params != null) {
				for ( var i = 0; i < params.length; i++) {
					var input = $("<input type='hidden' name='" + params[i][0] + "' value='" + params[i][1] + "'/>");
					form.append(input);
				}
			}
			if (url.indexOf("token=") == -1)
				form.append($("<input type='hidden' name='token' value='" + $("#tokenAuthorization").val() + "'/>"));
			form.appendTo('body');
			form.submit();
		},
		login: function() {
			debugger;
			$.ajax({
				url: $('#basePath').val()+'/login/init',
				cache: false,
				type: 'GET',
				success: function (data) {
					var json = JSON.parse(data);
					Commons.redirect2($('#basePath').val() + json.forward);
				}
			});
		}
};
var Validator = {
		/* Alphabeta,Numberic */
		alphaDigit: function(event){
			var e = event ? event : window.event;
			var keychar = String.fromCharCode(e.keyCode);
			/* ←、delete、End、Home、Left、Right、. */
			if (e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 37 || e.keyCode == 39 || (e.keyCode == 110 || e.keyCode == 190)) {
				return true;
			}
			return /\w/.test(keychar);
		},
		/* Numberic */
		bindIsNumberic: function() {
			$('.numberic').each(function(){
				$(this).keydown(function(event){
					var e = event ? event : window.event;
					/* ←、delete、End、Home、Left、Right、. */
					if (e.keyCode == 8 || e.keyCode == 46 || e.keyCode == 35 || e.keyCode == 36 || e.keyCode == 37 || e.keyCode == 39 || (e.keyCode == 110 || e.keyCode == 190)) {
						return;
					} else if (!(e.keyCode >= 48 && e.keyCode <= 57) && !(e.keyCode >= 96 && e.keyCode <= 105)) {
						return false;
					}
				});
			});
		}
	};

var TableListDeletePlugin = {
		dUrl : '',
		sUrl : '',
		id : '',
		table : '',
		init: function(table, searchUrl, deleteUrl) {
			TableListDeletePlugin.sUrl = searchUrl;
			TableListDeletePlugin.dUrl = deleteUrl;
			TableListDeletePlugin.table = table;
			$('.data-delete').each(function() {
				$(this).click(function(e) {
					TableListDeletePlugin.id = $(this).attr('data-data-id');
					var dialogDeleteConfirm = $('#dialog-delete-confirm');
					dialogDeleteConfirm.click(TableListDeletePlugin.func);
					$('#deleteModal').modal('show');
				});
			});
		},
		func : function(e) {
			if (Utils.IsEmpty(TableListDeletePlugin.sUrl) || Utils.IsEmpty(TableListDeletePlugin.dUrl) || Utils.IsEmpty(TableListDeletePlugin.id)) {
				alert('delete param is error');
				return;
			}
			$.ajax({
				url: TableListDeletePlugin.dUrl,
				cache: false,
				type: 'GET',
				data: {id: TableListDeletePlugin.id},
				success: function (data) {
					var json = JSON.parse(data);
					if(200 == json.result){
						$('#dialog-delete-confirm').unbind();
						$('#'+TableListDeletePlugin.table).DataTable().ajax.reload();
					} else if(500 == json.result) {
		    			alert(json.data);
					} else{
		                if (json.message && json.message != null) {
		                	$("#modalErrorArea").empty().append('<div>' + json.message + '</div>');
		                	$("#modalErrorArea").css("display", "block");
		                }
					}
					$('#deleteModal').modal('hide');
				}
			});
		}
};

Date.prototype.format = function(fmt) {
    var o = {
        "M+" : this.getMonth() + 1,
        "d+" : this.getDate(),
        "h+" : this.getHours(),
        "m+" : this.getMinutes(),
        "s+" : this.getSeconds(),
        "q+" : Math.floor((this.getMonth() + 3) / 3),
        "S" : this.getMilliseconds()
    };
    if (/(y+)/.test(fmt))
        fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "")
                .substr(4 - RegExp.$1.length));
    for ( var k in o)
        if (new RegExp("(" + k + ")").test(fmt))
            fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k])
                    : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}