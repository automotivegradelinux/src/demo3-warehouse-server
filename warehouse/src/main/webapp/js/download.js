/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
function doDownload(obj, appId,typeId,verFilePath) {
	debugger;
	 var thisObj=$(obj);
	 var files = [];
	 var deviceAccessParam = $('#params').val();
	 var homePath = deviceAccessParam.split("&")[2].split("=")[1];
	 files.push({thisObj:thisObj, appId:appId, typeId:typeId,filePath:verFilePath});
	 files.forEach(function(item){
		debugger;
		DownloadFile(item.thisObj, item.appId,item.typeId,item.filePath).then(
		function (result)
		{
			var filename = verFilePath.substr(verFilePath.lastIndexOf('/') + 1);
			debugger;
			path = homePath + "/Downloads/" + filename;
			thisObj.html("installing");
			aglSocket.installApp(path, installReplyok, installReplyerr, obj);

			if(result.error){
			}
		}, function(err) {
			console.log("DownloadFile err");
			//alert("DownloadFile err");
			debugger;
		}, function(evt){
			console.log("DownloadFile evt");
			//alert("DownloadFile evt");
			debugger;
		});
	})
}

function installReplyok(params){
	debugger;
	console.log("installApp" + "installReplyok\n");
	if(params.length > 1){
		buttonObj = params[1];
		var thisObj=$(buttonObj);
		thisObj.html("installed");
	}
};

function installReplyerr(params){
	debugger;
	console.log("installApp" + "installReplyerr\n");
	alert("install error");
	if(params.length > 1){
		buttonObj = params[1];
		var thisObj=$(buttonObj);
		thisObj.html("install");
	}
	/* var str="";
	var obj = params[0];
	for (var item in obj){
	    str +=item+":"+obj[item]+"\n";
	}
	alert("str==:\n"+str); */
};

function DownloadFile(thisObj,appId,typeId,verFilePath) {
	debugger;
	var filename = verFilePath.substr(verFilePath.lastIndexOf('/') + 1);
	var dfd = new $.Deferred();
	var promise = $.ajax({
		url: $("#basePath").val() + '/appDev/downloadenter',
		type: "POST",

		xhrFileds:{
			responseType: 'arraybuffer',
			onprogress: dfd.notify
		}
	});
	promise.then(function (data,status,xhr){
		if(xhr.statusText == "success" || xhr.statusText == "ok"
			||xhr.statusText == "SUCCESS" || xhr.statusText == "OK"){
			debugger;
			thisObj.html("downloading");
			var type = xhr.getResponseHeader('Content-Type');
			var blob = new Blob([data],{type:type});
			if(typeof window.navigator.msSaveBlob != 'undefined' ){
				window.navigator.msSaveBlob(blob, filename);
			}else{
				var URL = window.URL || window.webkitURL;
				var downloadUrl = URL.createObjectURL(blob);
				if(filename){
					var a = document.createElement("a");
					if(typeof a.download == 'undefined'){
						window.location = downloadUrl;
					}else{
							a.href = $("#basePath").val() + '/appDev/download?appId='+ appId + '&typeId='+ typeId +'&filePath='+verFilePath;
							a.download = filename;
							document.body.appendChild(a);
							a.click();
					}
				}else{
					window.location = downloadUrl;
				}
				setTimeout(function () {
                    URL.revokeObjectURL(downloadUrl);
                    dfd.resolve({});
                }, 10000);
			}
		}else{
			dfd.resolve({ error: { message: xhr.statusText } });
		}
	});
	return dfd;
};