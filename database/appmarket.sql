CREATE DATABASE  IF NOT EXISTS `appmarket` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `appmarket`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 192.168.20.161    Database: appmarket
-- ------------------------------------------------------
-- Server version	5.7.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_app`
--

DROP TABLE IF EXISTS `t_app`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_app` (
  `APP_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `APP_NAME` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `APP_ABSTRACT` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `APP_TYPE_ID` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `APP_DEVICE_TYPE_ID` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DEVELOPER` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `APP_CREATE_DATE` datetime DEFAULT NULL,
  `APP_UPDATE_DATE` datetime DEFAULT NULL,
  `APP_IS_DEL` char(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `APP_VERSION_NAME` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `APP_IS_PUBLIC` char(1) COLLATE utf8_unicode_ci DEFAULT '0',
  `APP_VERSION_ID` char(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `APP_ID_CUSTOM` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`APP_ID`),
  KEY `develop_fk_idx` (`DEVELOPER`),
  KEY `type_fk_idx` (`APP_TYPE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;



--
-- Table structure for table `t_app_version`
--

DROP TABLE IF EXISTS `t_app_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_app_version` (
  `VERSION_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `V_APP_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `VERSION_NAME` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `SIZE` int(11) DEFAULT NULL,
  `COMMENT` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FILE_PATH` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `MD5` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VERSION_CREATE_DATE` datetime DEFAULT NULL,
  `VERSION_IS_DEL` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `IMAGE_PATH` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`VERSION_ID`),
  KEY `app_fk_idx` (`V_APP_ID`),
  CONSTRAINT `app_fk` FOREIGN KEY (`V_APP_ID`) REFERENCES `t_app` (`APP_ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_app_version`
--


--
-- Table structure for table `t_authority`
--

DROP TABLE IF EXISTS `t_authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_authority` (
  `AU_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `AU_NAME` varchar(50) CHARACTER SET utf8 NOT NULL,
  `AU_IS_DEL` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`AU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_authority`
--

LOCK TABLES `t_authority` WRITE;
/*!40000 ALTER TABLE `t_authority` DISABLE KEYS */;
INSERT INTO `t_authority` VALUES ('1','admin','0'),('2','developer','0'),('3','nomal','0'),('4','guest','1');
/*!40000 ALTER TABLE `t_authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_authority_resource_link`
--

DROP TABLE IF EXISTS `t_authority_resource_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_authority_resource_link` (
  `R_RES_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `R_AU_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`R_RES_ID`,`R_AU_ID`),
  KEY `au_fk_idx` (`R_AU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_authority_resource_link`
--

LOCK TABLES `t_authority_resource_link` WRITE;
/*!40000 ALTER TABLE `t_authority_resource_link` DISABLE KEYS */;
INSERT INTO `t_authority_resource_link` VALUES ('00001','1'),('01001','1'),('01002','1'),('01003','1'),('01004','1'),('01005','1'),('01006','1'),('01007','1'),('02001','1'),('02002','1'),('02003','1'),('02004','1'),('02005','1'),('02006','1'),('02007','1'),('02008','1'),('10000','1'),('11001','1'),('11002','1'),('11003','1'),('11004','1'),('11005','1'),('12001','1'),('12002','1'),('12003','1'),('12004','1'),('12005','1'),('12006','1'),('12007','1'),('12008','1'),('12009','1'),('13001','1'),('14001','1'),('14002','1'),('14003','1'),('15001','1'),('16001','1'),('00001','2'),('02001','2'),('02002','2'),('02003','2'),('02004','2'),('02005','2'),('02006','2'),('02007','2'),('02008','2'),('10000','2'),('11005','2'),('12001','2'),('12002','2'),('12003','2'),('12004','2'),('12005','2'),('12006','2'),('12007','2'),('12008','2'),('12009','2'),('13001','2'),('14001','2'),('14002','2'),('14003','2'),('15001','2'),('16001','2'),('00001','3'),('02001','3'),('02004','3'),('02007','3'),('02008','3'),('10000','3'),('11005','3'),('12001','3'),('12002','3'),('12006','3'),('12008','3'),('12009','3'),('13001','3'),('14002','3'),('14003','3'),('15001','3'),('16001','3'),('00001','4'),('02001','4'),('02004','4'),('02007','4'),('02008','4'),('10000','4'),('11005','4'),('12001','4'),('12002','4'),('12006','4'),('12008','4'),('12009','4'),('13001','4'),('14002','4'),('14003','4'),('15001','4'),('16001','4');
/*!40000 ALTER TABLE `t_authority_resource_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_dictionary`
--

DROP TABLE IF EXISTS `t_dictionary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_dictionary` (
  `DIC_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DIC_TYPE` varchar(4) NOT NULL,
  `DIC_VALUE` varchar(4) NOT NULL,
  `DIC_LABEL` varchar(45) NOT NULL,
  PRIMARY KEY (`DIC_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_dictionary`
--

LOCK TABLES `t_dictionary` WRITE;
/*!40000 ALTER TABLE `t_dictionary` DISABLE KEYS */;
INSERT INTO `t_dictionary` VALUES (1,'0100','0','Automotive'),(2,'0100','1','Operation System'),(3,'0100','2','Connectivity'),(4,'0100','3','Graphics'),(5,'0100','4','Navigation'),(6,'0100','5','Multimedia'),(7,'0100','6','Nativate APP'),(8,'0100','7','AGL APP'),(9,'0100','8','Web APP'),(10,'0100','9','Other'),(11,'0101','0','Renesas M3 Dev Board'),(12,'0101','1','QEMU'),(13,'0101','2','Intel Minnowboard Max'),(14,'0101','3','TI Vayu'),(15,'0101','4','Raspberry Pi 3'),(16,'0101','5','Intel Cyclone V'),(17,'0102','0','Non-public'),(18,'0102','1','Public');
/*!40000 ALTER TABLE `t_dictionary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_resource`
--

DROP TABLE IF EXISTS `t_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_resource` (
  `RES_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `RES_NAME` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `RES_TYPE` varchar(50) CHARACTER SET utf8 NOT NULL,
  `ACCESS_PATH` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `HTTP_METHOD` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_resource`
--

LOCK TABLES `t_resource` WRITE;
/*!40000 ALTER TABLE `t_resource` DISABLE KEYS */;
INSERT INTO `t_resource` VALUES ('00001','メイン画面','resource','/main',NULL),('01001','アカウントリスト','resource','/account/list',NULL),('01002','アカウント編集','resource','/account/modify',NULL),('01003','アカウント検索','resource','/account/search',NULL),('01004','アカウント更新','resource','/account/update',NULL),('01005','アカウント新規を保存','resource','/account/save',NULL),('01006','アカウントを削除','resource','/account/delete',NULL),('01007','アカウント詳細','resource','/account/detail',NULL),('02001','アプリリスト','resource','/app/list',NULL),('02002','アプリ編集','resource','/app/modify',NULL),('02003','アプリ新規','resource','/app/save',NULL),('02004','アプリ検索','resource','/app/search',NULL),('02005','アプリ新規','resource','/app/insert',NULL),('02006','アプリ削除','resource','/app/delete',NULL),('02007','アプリダウンロード ','resource','/app/download',NULL),('02008','アプリ情報','resource','/app/detail',NULL),('10000','login','api','/login','POST'),('11001','getUserList','api','/user/collection','GET'),('11002','getUserById','api','/user/{userId}','GET'),('11003','updateUser','api','/user','POST'),('11004','deleteUser','api','/user/{userId}','DELETE'),('11005','getCurrentUser','api','/user/currentUser','GET'),('12001','getAppList','api','/app/collection','GET'),('12002','getApp','api','/app/info/{appId}','GET'),('12003','updateApp','api','/app/info','POST'),('12004','updateAppVersion','api','/app/info/version','POST'),('12005','deleteAppAllInfo','api','/app/info/{appId}','DELETE'),('12006','getDictionaryCollection','api','/dictionary/collection/{DictionaryTypeId}','GET'),('12007','updateImageFile','api','/app/image','POST'),('12008','getAppInfoByCustomId','api','/app/info/dev/{appIdCustom}','GET'),('12009','saveDictionary','api','/dictionary/info','POST'),('13001','getAuthorityCollection','api','/authority/collection','GET'),('14001','updateAppFile','api','/app/file/{fileName}/{appTypeId}/{appId}','POST'),('14002','downloadAppFile','api','/app/file/{fileName}/{appTypeId}/{appId}','GET'),('14003','downloadAppFile','api','/app/file','GET'),('15001','getMenuResource','api','/resource/menuResource/collection','GET'),('16001','refreshToken','api','/token/{refreshToken}','GET');
/*!40000 ALTER TABLE `t_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user`
--

DROP TABLE IF EXISTS `t_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user` (
  `USER_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `USER_PW` varchar(50) CHARACTER SET utf8 NOT NULL,
  `USER_NAME` varchar(50) CHARACTER SET utf8 NOT NULL,
  `REAL_NAME` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MAIL_ADDRESS` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `IS_DEL` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user`
--

LOCK TABLES `t_user` WRITE;
/*!40000 ALTER TABLE `t_user` DISABLE KEYS */;
INSERT INTO `t_user` VALUES ('4c442aaf-5f61-4673-b45f-acec2fb8eb08','c7f2618467e07b91cb5f542cb4b5c853','super_user',NULL,'admin@tted.com',NULL,NULL,'0'),('817acafc-cff8-4b05-ab04-a1bf4035119f','e10adc3949ba59abbe56e057f20f883e','appMarketGuestAuthorizationUser',NULL,'appMarketGuestAuthorizationUser@126.com',NULL,NULL,'0');
/*!40000 ALTER TABLE `t_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_user_authority_link`
--

DROP TABLE IF EXISTS `t_user_authority_link`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `t_user_authority_link` (
  `A_USER_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  `A_AU_ID` char(36) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`A_USER_ID`,`A_AU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_user_authority_link`
--

LOCK TABLES `t_user_authority_link` WRITE;
/*!40000 ALTER TABLE `t_user_authority_link` DISABLE KEYS */;
INSERT INTO `t_user_authority_link` VALUES ('4c442aaf-5f61-4673-b45f-acec2fb8eb08','1'),('817acafc-cff8-4b05-ab04-a1bf4035119f','3');
/*!40000 ALTER TABLE `t_user_authority_link` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'appmarket'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-19 17:34:04
