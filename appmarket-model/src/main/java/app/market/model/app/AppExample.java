/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.model.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import app.market.model.TokenModel;

public class AppExample extends TokenModel{

    /**
	 *
	 */
	private static final long serialVersionUID = 7069779194904235176L;

	protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AppExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAppIdIsNull() {
            addCriterion("APP_ID is null");
            return (Criteria) this;
        }

        public Criteria andAppIdIsNotNull() {
            addCriterion("APP_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAppIdEqualTo(String value) {
            addCriterion("APP_ID =", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotEqualTo(String value) {
            addCriterion("APP_ID <>", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThan(String value) {
            addCriterion("APP_ID >", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdGreaterThanOrEqualTo(String value) {
            addCriterion("APP_ID >=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThan(String value) {
            addCriterion("APP_ID <", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLessThanOrEqualTo(String value) {
            addCriterion("APP_ID <=", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdLike(String value) {
            addCriterion("APP_ID like", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotLike(String value) {
            addCriterion("APP_ID not like", value, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdIn(List<String> values) {
            addCriterion("APP_ID in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotIn(List<String> values) {
            addCriterion("APP_ID not in", values, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdBetween(String value1, String value2) {
            addCriterion("APP_ID between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andAppIdNotBetween(String value1, String value2) {
            addCriterion("APP_ID not between", value1, value2, "appId");
            return (Criteria) this;
        }

        public Criteria andAppNameIsNull() {
            addCriterion("APP_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAppNameIsNotNull() {
            addCriterion("APP_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAppNameEqualTo(String value) {
            addCriterion("APP_NAME =", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotEqualTo(String value) {
            addCriterion("APP_NAME <>", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameGreaterThan(String value) {
            addCriterion("APP_NAME >", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameGreaterThanOrEqualTo(String value) {
            addCriterion("APP_NAME >=", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLessThan(String value) {
            addCriterion("APP_NAME <", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLessThanOrEqualTo(String value) {
            addCriterion("APP_NAME <=", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameLike(String value) {
            addCriterion("APP_NAME like", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotLike(String value) {
            addCriterion("APP_NAME not like", value, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameIn(List<String> values) {
            addCriterion("APP_NAME in", values, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotIn(List<String> values) {
            addCriterion("APP_NAME not in", values, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameBetween(String value1, String value2) {
            addCriterion("APP_NAME between", value1, value2, "appName");
            return (Criteria) this;
        }

        public Criteria andAppNameNotBetween(String value1, String value2) {
            addCriterion("APP_NAME not between", value1, value2, "appName");
            return (Criteria) this;
        }

        public Criteria andAbstractIsNull() {
            addCriterion("APP_ABSTRACT is null");
            return (Criteria) this;
        }

        public Criteria andAbstractIsNotNull() {
            addCriterion("APP_ABSTRACT is not null");
            return (Criteria) this;
        }

        public Criteria andAbstractEqualTo(String value) {
            addCriterion("APP_ABSTRACT =", value, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractNotEqualTo(String value) {
            addCriterion("APP_ABSTRACT <>", value, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractGreaterThan(String value) {
            addCriterion("APP_ABSTRACT >", value, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractGreaterThanOrEqualTo(String value) {
            addCriterion("APP_ABSTRACT >=", value, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractLessThan(String value) {
            addCriterion("APP_ABSTRACT <", value, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractLessThanOrEqualTo(String value) {
            addCriterion("APP_ABSTRACT <=", value, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractLike(String value) {
            addCriterion("APP_ABSTRACT like", value, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractNotLike(String value) {
            addCriterion("APP_ABSTRACT not like", value, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractIn(List<String> values) {
            addCriterion("APP_ABSTRACT in", values, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractNotIn(List<String> values) {
            addCriterion("APP_ABSTRACT not in", values, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractBetween(String value1, String value2) {
            addCriterion("APP_ABSTRACT between", value1, value2, "abstract");
            return (Criteria) this;
        }

        public Criteria andAbstractNotBetween(String value1, String value2) {
            addCriterion("APP_ABSTRACT not between", value1, value2, "abstract");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNull() {
            addCriterion("APP_TYPE_ID is null");
            return (Criteria) this;
        }

        public Criteria andTypeIdIsNotNull() {
            addCriterion("APP_TYPE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andTypeIdEqualTo(String value) {
            addCriterion("APP_TYPE_ID =", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotEqualTo(String value) {
            addCriterion("APP_TYPE_ID <>", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThan(String value) {
            addCriterion("APP_TYPE_ID >", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdGreaterThanOrEqualTo(String value) {
            addCriterion("APP_TYPE_ID >=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThan(String value) {
            addCriterion("APP_TYPE_ID <", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLessThanOrEqualTo(String value) {
            addCriterion("APP_TYPE_ID <=", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdLike(String value) {
            addCriterion("APP_TYPE_ID like", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotLike(String value) {
            addCriterion("APP_TYPE_ID not like", value, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdIn(List<String> values) {
            addCriterion("APP_TYPE_ID in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotIn(List<String> values) {
            addCriterion("APP_TYPE_ID not in", values, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdBetween(String value1, String value2) {
            addCriterion("APP_TYPE_ID between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andTypeIdNotBetween(String value1, String value2) {
            addCriterion("APP_TYPE_ID not between", value1, value2, "typeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdIsNull() {
            addCriterion("APP_DEVICE_TYPE_ID is null");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdIsNotNull() {
            addCriterion("APP_DEVICE_TYPE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdEqualTo(String value) {
            addCriterion("APP_DEVICE_TYPE_ID =", value, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdNotEqualTo(String value) {
            addCriterion("APP_DEVICE_TYPE_ID <>", value, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdGreaterThan(String value) {
            addCriterion("APP_DEVICE_TYPE_ID >", value, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdGreaterThanOrEqualTo(String value) {
            addCriterion("APP_DEVICE_TYPE_ID >=", value, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdLessThan(String value) {
            addCriterion("APP_DEVICE_TYPE_ID <", value, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdLessThanOrEqualTo(String value) {
            addCriterion("APP_DEVICE_TYPE_ID <=", value, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdLike(String value) {
            addCriterion("APP_DEVICE_TYPE_ID like", value, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdNotLike(String value) {
            addCriterion("APP_DEVICE_TYPE_ID not like", value, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdIn(List<String> values) {
            addCriterion("APP_DEVICE_TYPE_ID in", values, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdNotIn(List<String> values) {
            addCriterion("APP_DEVICE_TYPE_ID not in", values, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdBetween(String value1, String value2) {
            addCriterion("APP_DEVICE_TYPE_ID between", value1, value2, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andAppDeviceTypeIdNotBetween(String value1, String value2) {
            addCriterion("APP_DEVICE_TYPE_ID not between", value1, value2, "appDeviceTypeId");
            return (Criteria) this;
        }

        public Criteria andDeveloperIsNull() {
            addCriterion("DEVELOPER is null");
            return (Criteria) this;
        }

        public Criteria andDeveloperIsNotNull() {
            addCriterion("DEVELOPER is not null");
            return (Criteria) this;
        }

        public Criteria andDeveloperEqualTo(String value) {
            addCriterion("DEVELOPER =", value, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperNotEqualTo(String value) {
            addCriterion("DEVELOPER <>", value, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperGreaterThan(String value) {
            addCriterion("DEVELOPER >", value, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperGreaterThanOrEqualTo(String value) {
            addCriterion("DEVELOPER >=", value, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperLessThan(String value) {
            addCriterion("DEVELOPER <", value, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperLessThanOrEqualTo(String value) {
            addCriterion("DEVELOPER <=", value, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperLike(String value) {
            addCriterion("DEVELOPER like", value, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperNameLike(String value) {
            addCriterion("USER_NAME like", value, "developerName");
            return (Criteria) this;
        }

        public Criteria andDeveloperNotLike(String value) {
            addCriterion("DEVELOPER not like", value, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperIn(List<String> values) {
            addCriterion("DEVELOPER in", values, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperNotIn(List<String> values) {
            addCriterion("DEVELOPER not in", values, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperBetween(String value1, String value2) {
            addCriterion("DEVELOPER between", value1, value2, "developer");
            return (Criteria) this;
        }

        public Criteria andDeveloperNotBetween(String value1, String value2) {
            addCriterion("DEVELOPER not between", value1, value2, "developer");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNull() {
            addCriterion("APP_CREATE_DATE is null");
            return (Criteria) this;
        }

        public Criteria andCreateDateIsNotNull() {
            addCriterion("APP_CREATE_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andCreateDateEqualTo(Date value) {
            addCriterion("APP_CREATE_DATE =", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotEqualTo(Date value) {
            addCriterion("APP_CREATE_DATE <>", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThan(Date value) {
            addCriterion("APP_CREATE_DATE >", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("APP_CREATE_DATE >=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThan(Date value) {
            addCriterion("APP_CREATE_DATE <", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("APP_CREATE_DATE <=", value, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateIn(List<Date> values) {
            addCriterion("APP_CREATE_DATE in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotIn(List<Date> values) {
            addCriterion("APP_CREATE_DATE not in", values, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateBetween(Date value1, Date value2) {
            addCriterion("APP_CREATE_DATE between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("APP_CREATE_DATE not between", value1, value2, "createDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNull() {
            addCriterion("APP_UPDATE_DATE is null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIsNotNull() {
            addCriterion("APP_UPDATE_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateDateEqualTo(Date value) {
            addCriterion("APP_UPDATE_DATE =", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotEqualTo(Date value) {
            addCriterion("APP_UPDATE_DATE <>", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThan(Date value) {
            addCriterion("APP_UPDATE_DATE >", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("APP_UPDATE_DATE >=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThan(Date value) {
            addCriterion("APP_UPDATE_DATE <", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateLessThanOrEqualTo(Date value) {
            addCriterion("APP_UPDATE_DATE <=", value, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateIn(List<Date> values) {
            addCriterion("APP_UPDATE_DATE in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotIn(List<Date> values) {
            addCriterion("APP_UPDATE_DATE not in", values, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateBetween(Date value1, Date value2) {
            addCriterion("APP_UPDATE_DATE between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andUpdateDateNotBetween(Date value1, Date value2) {
            addCriterion("APP_UPDATE_DATE not between", value1, value2, "updateDate");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("APP_IS_DEL is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("APP_IS_DEL is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(String value) {
            addCriterion("APP_IS_DEL =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(String value) {
            addCriterion("APP_IS_DEL <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(String value) {
            addCriterion("APP_IS_DEL >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(String value) {
            addCriterion("APP_IS_DEL >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(String value) {
            addCriterion("APP_IS_DEL <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(String value) {
            addCriterion("APP_IS_DEL <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLike(String value) {
            addCriterion("APP_IS_DEL like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotLike(String value) {
            addCriterion("APP_IS_DEL not like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<String> values) {
            addCriterion("APP_IS_DEL in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<String> values) {
            addCriterion("APP_IS_DEL not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(String value1, String value2) {
            addCriterion("APP_IS_DEL between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(String value1, String value2) {
            addCriterion("APP_IS_DEL not between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicIsNull() {
            addCriterion("APP_IS_PUBLIC is null");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicIsNotNull() {
            addCriterion("APP_IS_PUBLIC is not null");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicEqualTo(String value) {
            addCriterion("APP_IS_PUBLIC =", value, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicNotEqualTo(String value) {
            addCriterion("APP_IS_PUBLIC <>", value, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicGreaterThan(String value) {
            addCriterion("APP_IS_PUBLIC >", value, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicGreaterThanOrEqualTo(String value) {
            addCriterion("APP_IS_PUBLIC >=", value, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicLessThan(String value) {
            addCriterion("APP_IS_PUBLIC <", value, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicLessThanOrEqualTo(String value) {
            addCriterion("APP_IS_PUBLIC <=", value, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicLike(String value) {
            addCriterion("APP_IS_PUBLIC like", value, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicNotLike(String value) {
            addCriterion("APP_IS_PUBLIC not like", value, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicIn(List<String> values) {
            addCriterion("APP_IS_PUBLIC in", values, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicNotIn(List<String> values) {
            addCriterion("APP_IS_PUBLIC not in", values, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicBetween(String value1, String value2) {
            addCriterion("APP_IS_PUBLIC between", value1, value2, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppIsPublicNotBetween(String value1, String value2) {
            addCriterion("APP_IS_PUBLIC not between", value1, value2, "appIsPublic");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdIsNull() {
            addCriterion("APP_VERSION_ID is null");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdIsNotNull() {
            addCriterion("APP_VERSION_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdEqualTo(String value) {
            addCriterion("APP_VERSION_ID =", value, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdNotEqualTo(String value) {
            addCriterion("APP_VERSION_ID <>", value, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdGreaterThan(String value) {
            addCriterion("APP_VERSION_ID >", value, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdGreaterThanOrEqualTo(String value) {
            addCriterion("APP_VERSION_ID >=", value, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdLessThan(String value) {
            addCriterion("APP_VERSION_ID <", value, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdLessThanOrEqualTo(String value) {
            addCriterion("APP_VERSION_ID <=", value, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdLike(String value) {
            addCriterion("APP_VERSION_ID like", value, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdNotLike(String value) {
            addCriterion("APP_VERSION_ID not like", value, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdIn(List<String> values) {
            addCriterion("APP_VERSION_ID in", values, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdNotIn(List<String> values) {
            addCriterion("APP_VERSION_ID not in", values, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdBetween(String value1, String value2) {
            addCriterion("APP_VERSION_ID between", value1, value2, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppVersionIdNotBetween(String value1, String value2) {
            addCriterion("APP_VERSION_ID not between", value1, value2, "appVersionId");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomIsNull() {
            addCriterion("APP_ID_CUSTOM is null");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomIsNotNull() {
            addCriterion("APP_ID_CUSTOM is not null");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomEqualTo(String value) {
            addCriterion("APP_ID_CUSTOM =", value, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomNotEqualTo(String value) {
            addCriterion("APP_ID_CUSTOM <>", value, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomGreaterThan(String value) {
            addCriterion("APP_ID_CUSTOM >", value, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomGreaterThanOrEqualTo(String value) {
            addCriterion("APP_ID_CUSTOM >=", value, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomLessThan(String value) {
            addCriterion("APP_ID_CUSTOM <", value, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomLessThanOrEqualTo(String value) {
            addCriterion("APP_ID_CUSTOM <=", value, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomLike(String value) {
            addCriterion("APP_ID_CUSTOM like", value, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomNotLike(String value) {
            addCriterion("APP_ID_CUSTOM not like", value, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomIn(List<String> values) {
            addCriterion("APP_ID_CUSTOM in", values, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomNotIn(List<String> values) {
            addCriterion("APP_ID_CUSTOM not in", values, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomBetween(String value1, String value2) {
            addCriterion("APP_ID_CUSTOM between", value1, value2, "appIdCustom");
            return (Criteria) this;
        }

        public Criteria andAppIdCustomNotBetween(String value1, String value2) {
            addCriterion("APP_ID_CUSTOM not between", value1, value2, "appIdCustom");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}