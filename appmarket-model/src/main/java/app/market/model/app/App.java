/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.model.app;

import java.util.Date;

import app.market.model.TokenModel;

public class App extends TokenModel{
    /**
	 *
	 */
	private static final long serialVersionUID = -8859096814674858740L;

	private String appId;

    private String appName;

    private String appAbstract;

    private String typeId;

    private String typeName;

    private String appDeviceTypeId;

    private String appDeviceTypeName;

    private String developer;

    private String developerName;

    private String versionName;

	private String appVersionId;

    private String verFilePath;

    private int verFileSize;

    private Date verCreateDate;

    private Date createDate;

    private Date updateDate;

    private String isDel;

    private String hashcode;

	private String appIsPublic;

	private String appIsPublicLable;

    private String appIdCustom;

    private String imagePath;

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId == null ? null : appId.trim();
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName == null ? null : appName.trim();
    }

    public String getAppAbstract() {
        return appAbstract;
    }

    public void setAppAbstract(String appAbstract) {
        this.appAbstract = appAbstract == null ? null : appAbstract.trim();
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId == null ? null : typeId.trim();
    }

    public String getAppDeviceTypeId() {
        return appDeviceTypeId;
    }

    public void setAppDeviceTypeId(String appDeviceTypeId) {
        this.appDeviceTypeId = appDeviceTypeId == null ? null : appDeviceTypeId.trim();
    }

    public String getDeveloper() {
        return developer;
    }

    public void setDeveloper(String developer) {
        this.developer = developer == null ? null : developer.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel == null ? null : isDel.trim();
    }

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	public String getDeveloperName() {
		return developerName;
	}

	public void setDeveloperName(String developerName) {
		this.developerName = developerName;
	}

	public String getVerFilePath() {
		return verFilePath;
	}

	public void setVerFilePath(String verFilePath) {
		this.verFilePath = verFilePath;
	}

	public int getVerFileSize() {
		return verFileSize;
	}

	public void setVerFileSize(int verFileSize) {
		this.verFileSize = verFileSize;
	}

	public Date getVerCreateDate() {
		return verCreateDate;
	}

	public void setVerCreateDate(Date verCreateDate) {
		this.verCreateDate = verCreateDate;
	}

	public String getHashcode() {
		return hashcode;
	}

	public void setHashcode(String hashcode) {
		this.hashcode = hashcode;
	}

    public String getAppIsPublic() {
        return appIsPublic;
    }

    public void setAppIsPublic(String appIsPublic) {
        this.appIsPublic = appIsPublic == null ? null : appIsPublic.trim();
    }

	public String getAppDeviceTypeName() {
		return appDeviceTypeName;
	}

	public void setAppDeviceTypeName(String appDeviceTypeName) {
		this.appDeviceTypeName = appDeviceTypeName;
	}

	public String getAppIsPublicLable() {
		return appIsPublicLable;
	}

	public void setAppIsPublicLable(String appIsPublicLable) {
		this.appIsPublicLable = appIsPublicLable;
	}

    public String getVersionName() {
		return versionName;
	}

	public String getAppVersionId() {
		return appVersionId;
	}

	public void setAppVersionId(String appVersionId) {
		this.appVersionId = appVersionId;
	}

    public String getAppIdCustom() {
        return appIdCustom;
    }

    public void setAppIdCustom(String appIdCustom) {
        this.appIdCustom = appIdCustom == null ? null : appIdCustom.trim();
    }
	public void setVersionName(String versionName) {
		this.versionName = versionName;
	}

}