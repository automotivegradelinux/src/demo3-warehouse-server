/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.model.app;

import java.util.Date;

public class AppVersion {
    private String versionId;

    private String vAppId;

    private String versionName;

    private Integer size;

    private String comment;

    private String filePath;

    private String md5;

    private Date versionCreateDate;

    private String versionIsDel;
    private String imagePath;

    public String getVersionId() {
        return versionId;
    }

    public void setVersionId(String versionId) {
        this.versionId = versionId == null ? null : versionId.trim();
    }

    public String getvAppId() {
        return vAppId;
    }

    public void setvAppId(String vAppId) {
        this.vAppId = vAppId == null ? null : vAppId.trim();
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName == null ? null : versionName.trim();
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath == null ? null : filePath.trim();
    }

    public String getMd5() {
        return md5;
    }

    public void setMd5(String md5) {
        this.md5 = md5 == null ? null : md5.trim();
    }

    public Date getVersionCreateDate() {
        return versionCreateDate;
    }

    public void setVersionCreateDate(Date versionCreateDate) {
        this.versionCreateDate = versionCreateDate;
    }

    public String getVersionIsDel() {
        return versionIsDel;
    }

    public void setVersionIsDel(String versionIsDel) {
        this.versionIsDel = versionIsDel == null ? null : versionIsDel.trim();
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath == null ? null : imagePath.trim();
    }
}