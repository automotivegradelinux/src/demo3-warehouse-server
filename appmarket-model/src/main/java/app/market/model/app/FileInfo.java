/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.model.app;

import app.market.model.TokenModel;

public class FileInfo extends TokenModel {

	private static final long serialVersionUID = -6324279033213618383L;

	private String fileName= "";

	private long fileSize = 0;

	private String filePath= "";

	private String fileHashCode = "";

	/**
	 * [app id] value in config.xml
	 */
	private String configAppId= "";

	/**
	 * [version] value in config.xml
	 */
	private String configVersionName= "";

	/**
	 * [name] value in config.xml
	 */
	private String configAppName= "";

	/**
	 * [icon] value in config.xml
	 */
	private String iconPath = "";

	public String getFileHashCode() {
		return fileHashCode;
	}
	public void setFileHashCode(String fileHashCode) {
		this.fileHashCode = fileHashCode;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getConfigVersionName() {
		return configVersionName;
	}
	public void setConfigVersionName(String configVersionName) {
		this.configVersionName = configVersionName;
	}
	public String getConfigAppName() {
		return configAppName;
	}
	public void setConfigAppName(String configAppName) {
		this.configAppName = configAppName;
	}
	public String getIconPath() {
		return iconPath;
	}
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	public String getConfigAppId() {
		return configAppId;
	}
	public void setConfigAppId(String configAppId) {
		this.configAppId = configAppId;
	}
	}