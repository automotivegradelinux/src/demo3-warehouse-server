/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.model.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import app.market.model.TokenModel;

public class AppVersionExample extends TokenModel {
    /**
	 *
	 */
	private static final long serialVersionUID = 5985453790095559260L;

	protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AppVersionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andVersionIdIsNull() {
            addCriterion("VERSION_ID is null");
            return (Criteria) this;
        }

        public Criteria andVersionIdIsNotNull() {
            addCriterion("VERSION_ID is not null");
            return (Criteria) this;
        }

        public Criteria andVersionIdEqualTo(String value) {
            addCriterion("VERSION_ID =", value, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdNotEqualTo(String value) {
            addCriterion("VERSION_ID <>", value, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdGreaterThan(String value) {
            addCriterion("VERSION_ID >", value, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdGreaterThanOrEqualTo(String value) {
            addCriterion("VERSION_ID >=", value, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdLessThan(String value) {
            addCriterion("VERSION_ID <", value, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdLessThanOrEqualTo(String value) {
            addCriterion("VERSION_ID <=", value, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdLike(String value) {
            addCriterion("VERSION_ID like", value, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdNotLike(String value) {
            addCriterion("VERSION_ID not like", value, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdIn(List<String> values) {
            addCriterion("VERSION_ID in", values, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdNotIn(List<String> values) {
            addCriterion("VERSION_ID not in", values, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdBetween(String value1, String value2) {
            addCriterion("VERSION_ID between", value1, value2, "versionId");
            return (Criteria) this;
        }

        public Criteria andVersionIdNotBetween(String value1, String value2) {
            addCriterion("VERSION_ID not between", value1, value2, "versionId");
            return (Criteria) this;
        }

        public Criteria andVAppIdIsNull() {
            addCriterion("V_APP_ID is null");
            return (Criteria) this;
        }

        public Criteria andVAppIdIsNotNull() {
            addCriterion("V_APP_ID is not null");
            return (Criteria) this;
        }

        public Criteria andVAppIdEqualTo(String value) {
            addCriterion("V_APP_ID =", value, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdNotEqualTo(String value) {
            addCriterion("V_APP_ID <>", value, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdGreaterThan(String value) {
            addCriterion("V_APP_ID >", value, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdGreaterThanOrEqualTo(String value) {
            addCriterion("V_APP_ID >=", value, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdLessThan(String value) {
            addCriterion("V_APP_ID <", value, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdLessThanOrEqualTo(String value) {
            addCriterion("V_APP_ID <=", value, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdLike(String value) {
            addCriterion("V_APP_ID like", value, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdNotLike(String value) {
            addCriterion("V_APP_ID not like", value, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdIn(List<String> values) {
            addCriterion("V_APP_ID in", values, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdNotIn(List<String> values) {
            addCriterion("V_APP_ID not in", values, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdBetween(String value1, String value2) {
            addCriterion("V_APP_ID between", value1, value2, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVAppIdNotBetween(String value1, String value2) {
            addCriterion("V_APP_ID not between", value1, value2, "vAppId");
            return (Criteria) this;
        }

        public Criteria andVersionNameIsNull() {
            addCriterion("VERSION_NAME is null");
            return (Criteria) this;
        }

        public Criteria andVersionNameIsNotNull() {
            addCriterion("VERSION_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andVersionNameEqualTo(String value) {
            addCriterion("VERSION_NAME =", value, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameNotEqualTo(String value) {
            addCriterion("VERSION_NAME <>", value, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameGreaterThan(String value) {
            addCriterion("VERSION_NAME >", value, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameGreaterThanOrEqualTo(String value) {
            addCriterion("VERSION_NAME >=", value, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameLessThan(String value) {
            addCriterion("VERSION_NAME <", value, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameLessThanOrEqualTo(String value) {
            addCriterion("VERSION_NAME <=", value, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameLike(String value) {
            addCriterion("VERSION_NAME like", value, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameNotLike(String value) {
            addCriterion("VERSION_NAME not like", value, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameIn(List<String> values) {
            addCriterion("VERSION_NAME in", values, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameNotIn(List<String> values) {
            addCriterion("VERSION_NAME not in", values, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameBetween(String value1, String value2) {
            addCriterion("VERSION_NAME between", value1, value2, "versionName");
            return (Criteria) this;
        }

        public Criteria andVersionNameNotBetween(String value1, String value2) {
            addCriterion("VERSION_NAME not between", value1, value2, "versionName");
            return (Criteria) this;
        }

        public Criteria andSizeIsNull() {
            addCriterion("SIZE is null");
            return (Criteria) this;
        }

        public Criteria andSizeIsNotNull() {
            addCriterion("SIZE is not null");
            return (Criteria) this;
        }

        public Criteria andSizeEqualTo(Integer value) {
            addCriterion("SIZE =", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotEqualTo(Integer value) {
            addCriterion("SIZE <>", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThan(Integer value) {
            addCriterion("SIZE >", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeGreaterThanOrEqualTo(Integer value) {
            addCriterion("SIZE >=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThan(Integer value) {
            addCriterion("SIZE <", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeLessThanOrEqualTo(Integer value) {
            addCriterion("SIZE <=", value, "size");
            return (Criteria) this;
        }

        public Criteria andSizeIn(List<Integer> values) {
            addCriterion("SIZE in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotIn(List<Integer> values) {
            addCriterion("SIZE not in", values, "size");
            return (Criteria) this;
        }

        public Criteria andSizeBetween(Integer value1, Integer value2) {
            addCriterion("SIZE between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andSizeNotBetween(Integer value1, Integer value2) {
            addCriterion("SIZE not between", value1, value2, "size");
            return (Criteria) this;
        }

        public Criteria andCommentIsNull() {
            addCriterion("COMMENT is null");
            return (Criteria) this;
        }

        public Criteria andCommentIsNotNull() {
            addCriterion("COMMENT is not null");
            return (Criteria) this;
        }

        public Criteria andCommentEqualTo(String value) {
            addCriterion("COMMENT =", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotEqualTo(String value) {
            addCriterion("COMMENT <>", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentGreaterThan(String value) {
            addCriterion("COMMENT >", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentGreaterThanOrEqualTo(String value) {
            addCriterion("COMMENT >=", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentLessThan(String value) {
            addCriterion("COMMENT <", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentLessThanOrEqualTo(String value) {
            addCriterion("COMMENT <=", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentLike(String value) {
            addCriterion("COMMENT like", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotLike(String value) {
            addCriterion("COMMENT not like", value, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentIn(List<String> values) {
            addCriterion("COMMENT in", values, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotIn(List<String> values) {
            addCriterion("COMMENT not in", values, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentBetween(String value1, String value2) {
            addCriterion("COMMENT between", value1, value2, "comment");
            return (Criteria) this;
        }

        public Criteria andCommentNotBetween(String value1, String value2) {
            addCriterion("COMMENT not between", value1, value2, "comment");
            return (Criteria) this;
        }

        public Criteria andFilePathIsNull() {
            addCriterion("FILE_PATH is null");
            return (Criteria) this;
        }

        public Criteria andFilePathIsNotNull() {
            addCriterion("FILE_PATH is not null");
            return (Criteria) this;
        }

        public Criteria andFilePathEqualTo(String value) {
            addCriterion("FILE_PATH =", value, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathNotEqualTo(String value) {
            addCriterion("FILE_PATH <>", value, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathGreaterThan(String value) {
            addCriterion("FILE_PATH >", value, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathGreaterThanOrEqualTo(String value) {
            addCriterion("FILE_PATH >=", value, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathLessThan(String value) {
            addCriterion("FILE_PATH <", value, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathLessThanOrEqualTo(String value) {
            addCriterion("FILE_PATH <=", value, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathLike(String value) {
            addCriterion("FILE_PATH like", value, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathNotLike(String value) {
            addCriterion("FILE_PATH not like", value, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathIn(List<String> values) {
            addCriterion("FILE_PATH in", values, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathNotIn(List<String> values) {
            addCriterion("FILE_PATH not in", values, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathBetween(String value1, String value2) {
            addCriterion("FILE_PATH between", value1, value2, "filePath");
            return (Criteria) this;
        }

        public Criteria andFilePathNotBetween(String value1, String value2) {
            addCriterion("FILE_PATH not between", value1, value2, "filePath");
            return (Criteria) this;
        }

        public Criteria andMd5IsNull() {
            addCriterion("MD5 is null");
            return (Criteria) this;
        }

        public Criteria andMd5IsNotNull() {
            addCriterion("MD5 is not null");
            return (Criteria) this;
        }

        public Criteria andMd5EqualTo(String value) {
            addCriterion("MD5 =", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5NotEqualTo(String value) {
            addCriterion("MD5 <>", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5GreaterThan(String value) {
            addCriterion("MD5 >", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5GreaterThanOrEqualTo(String value) {
            addCriterion("MD5 >=", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5LessThan(String value) {
            addCriterion("MD5 <", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5LessThanOrEqualTo(String value) {
            addCriterion("MD5 <=", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5Like(String value) {
            addCriterion("MD5 like", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5NotLike(String value) {
            addCriterion("MD5 not like", value, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5In(List<String> values) {
            addCriterion("MD5 in", values, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5NotIn(List<String> values) {
            addCriterion("MD5 not in", values, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5Between(String value1, String value2) {
            addCriterion("MD5 between", value1, value2, "md5");
            return (Criteria) this;
        }

        public Criteria andMd5NotBetween(String value1, String value2) {
            addCriterion("MD5 not between", value1, value2, "md5");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateIsNull() {
            addCriterion("VERSION_CREATE_DATE is null");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateIsNotNull() {
            addCriterion("VERSION_CREATE_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateEqualTo(Date value) {
            addCriterion("VERSION_CREATE_DATE =", value, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateNotEqualTo(Date value) {
            addCriterion("VERSION_CREATE_DATE <>", value, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateGreaterThan(Date value) {
            addCriterion("VERSION_CREATE_DATE >", value, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateGreaterThanOrEqualTo(Date value) {
            addCriterion("VERSION_CREATE_DATE >=", value, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateLessThan(Date value) {
            addCriterion("VERSION_CREATE_DATE <", value, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateLessThanOrEqualTo(Date value) {
            addCriterion("VERSION_CREATE_DATE <=", value, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateIn(List<Date> values) {
            addCriterion("VERSION_CREATE_DATE in", values, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateNotIn(List<Date> values) {
            addCriterion("VERSION_CREATE_DATE not in", values, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateBetween(Date value1, Date value2) {
            addCriterion("VERSION_CREATE_DATE between", value1, value2, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionCreateDateNotBetween(Date value1, Date value2) {
            addCriterion("VERSION_CREATE_DATE not between", value1, value2, "versionCreateDate");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelIsNull() {
            addCriterion("VERSION_IS_DEL is null");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelIsNotNull() {
            addCriterion("VERSION_IS_DEL is not null");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelEqualTo(String value) {
            addCriterion("VERSION_IS_DEL =", value, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelNotEqualTo(String value) {
            addCriterion("VERSION_IS_DEL <>", value, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelGreaterThan(String value) {
            addCriterion("VERSION_IS_DEL >", value, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelGreaterThanOrEqualTo(String value) {
            addCriterion("VERSION_IS_DEL >=", value, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelLessThan(String value) {
            addCriterion("VERSION_IS_DEL <", value, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelLessThanOrEqualTo(String value) {
            addCriterion("VERSION_IS_DEL <=", value, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelLike(String value) {
            addCriterion("VERSION_IS_DEL like", value, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelNotLike(String value) {
            addCriterion("VERSION_IS_DEL not like", value, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelIn(List<String> values) {
            addCriterion("VERSION_IS_DEL in", values, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelNotIn(List<String> values) {
            addCriterion("VERSION_IS_DEL not in", values, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelBetween(String value1, String value2) {
            addCriterion("VERSION_IS_DEL between", value1, value2, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andVersionIsDelNotBetween(String value1, String value2) {
            addCriterion("VERSION_IS_DEL not between", value1, value2, "versionIsDel");
            return (Criteria) this;
        }

        public Criteria andImagePathIsNull() {
            addCriterion("IMAGE_PATH is null");
            return (Criteria) this;
        }

        public Criteria andImagePathIsNotNull() {
            addCriterion("IMAGE_PATH is not null");
            return (Criteria) this;
        }

        public Criteria andImagePathEqualTo(String value) {
            addCriterion("IMAGE_PATH =", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathNotEqualTo(String value) {
            addCriterion("IMAGE_PATH <>", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathGreaterThan(String value) {
            addCriterion("IMAGE_PATH >", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathGreaterThanOrEqualTo(String value) {
            addCriterion("IMAGE_PATH >=", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathLessThan(String value) {
            addCriterion("IMAGE_PATH <", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathLessThanOrEqualTo(String value) {
            addCriterion("IMAGE_PATH <=", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathLike(String value) {
            addCriterion("IMAGE_PATH like", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathNotLike(String value) {
            addCriterion("IMAGE_PATH not like", value, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathIn(List<String> values) {
            addCriterion("IMAGE_PATH in", values, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathNotIn(List<String> values) {
            addCriterion("IMAGE_PATH not in", values, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathBetween(String value1, String value2) {
            addCriterion("IMAGE_PATH between", value1, value2, "imagePath");
            return (Criteria) this;
        }

        public Criteria andImagePathNotBetween(String value1, String value2) {
            addCriterion("IMAGE_PATH not between", value1, value2, "imagePath");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}