/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.model.authority;

import java.util.ArrayList;
import java.util.List;

public class AuthorityExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AuthorityExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAuIdIsNull() {
            addCriterion("AU_ID is null");
            return (Criteria) this;
        }

        public Criteria andAuIdIsNotNull() {
            addCriterion("AU_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAuIdEqualTo(String value) {
            addCriterion("AU_ID =", value, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdNotEqualTo(String value) {
            addCriterion("AU_ID <>", value, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdGreaterThan(String value) {
            addCriterion("AU_ID >", value, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdGreaterThanOrEqualTo(String value) {
            addCriterion("AU_ID >=", value, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdLessThan(String value) {
            addCriterion("AU_ID <", value, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdLessThanOrEqualTo(String value) {
            addCriterion("AU_ID <=", value, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdLike(String value) {
            addCriterion("AU_ID like", value, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdNotLike(String value) {
            addCriterion("AU_ID not like", value, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdIn(List<String> values) {
            addCriterion("AU_ID in", values, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdNotIn(List<String> values) {
            addCriterion("AU_ID not in", values, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdBetween(String value1, String value2) {
            addCriterion("AU_ID between", value1, value2, "auId");
            return (Criteria) this;
        }

        public Criteria andAuIdNotBetween(String value1, String value2) {
            addCriterion("AU_ID not between", value1, value2, "auId");
            return (Criteria) this;
        }

        public Criteria andAuNameIsNull() {
            addCriterion("AU_NAME is null");
            return (Criteria) this;
        }

        public Criteria andAuNameIsNotNull() {
            addCriterion("AU_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andAuNameEqualTo(String value) {
            addCriterion("AU_NAME =", value, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameNotEqualTo(String value) {
            addCriterion("AU_NAME <>", value, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameGreaterThan(String value) {
            addCriterion("AU_NAME >", value, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameGreaterThanOrEqualTo(String value) {
            addCriterion("AU_NAME >=", value, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameLessThan(String value) {
            addCriterion("AU_NAME <", value, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameLessThanOrEqualTo(String value) {
            addCriterion("AU_NAME <=", value, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameLike(String value) {
            addCriterion("AU_NAME like", value, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameNotLike(String value) {
            addCriterion("AU_NAME not like", value, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameIn(List<String> values) {
            addCriterion("AU_NAME in", values, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameNotIn(List<String> values) {
            addCriterion("AU_NAME not in", values, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameBetween(String value1, String value2) {
            addCriterion("AU_NAME between", value1, value2, "auName");
            return (Criteria) this;
        }

        public Criteria andAuNameNotBetween(String value1, String value2) {
            addCriterion("AU_NAME not between", value1, value2, "auName");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNull() {
            addCriterion("AU_IS_DEL is null");
            return (Criteria) this;
        }

        public Criteria andIsDelIsNotNull() {
            addCriterion("AU_IS_DEL is not null");
            return (Criteria) this;
        }

        public Criteria andIsDelEqualTo(String value) {
            addCriterion("AU_IS_DEL =", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotEqualTo(String value) {
            addCriterion("AU_IS_DEL <>", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThan(String value) {
            addCriterion("AU_IS_DEL >", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelGreaterThanOrEqualTo(String value) {
            addCriterion("AU_IS_DEL >=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThan(String value) {
            addCriterion("AU_IS_DEL <", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLessThanOrEqualTo(String value) {
            addCriterion("AU_IS_DEL <=", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelLike(String value) {
            addCriterion("AU_IS_DEL like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotLike(String value) {
            addCriterion("AU_IS_DEL not like", value, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelIn(List<String> values) {
            addCriterion("AU_IS_DEL in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotIn(List<String> values) {
            addCriterion("AU_IS_DEL not in", values, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelBetween(String value1, String value2) {
            addCriterion("AU_IS_DEL between", value1, value2, "isDel");
            return (Criteria) this;
        }

        public Criteria andIsDelNotBetween(String value1, String value2) {
            addCriterion("AU_IS_DEL not between", value1, value2, "isDel");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}