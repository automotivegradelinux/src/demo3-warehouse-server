/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.model.errors;

import java.io.Serializable;

/**
 * ResponseErrors
 */
public class ResponseErrors implements Serializable {

	/**
	 * Default ID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 *  error code
	 */
	private String error_code;

	/**
	 *  error message
	 */
	private String message;

	/**
	 *  other information
	 */
	private Object ext;

	public String getcode() {
		return error_code;
	}

	public void setcode(String error_code) {
		this.error_code = error_code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getExt() {
		return ext;
	}

	public void setExt(Object ext) {
		this.ext = ext;
	}
}