/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.persistence.mapper.app;

import java.util.List;
import app.market.model.app.App;
import app.market.model.app.AppExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

@Repository(value = "AppMapper")
public interface AppMapper {
    int countByExample(AppExample example);

    int deleteByExample(AppExample example);

    int deleteByPrimaryKey(String appId);

    int insert(App record);

    int insertSelective(App record);

    List<App> selectByExample(AppExample example);

    List<App> selectByExample(RowBounds rowBounds, AppExample example);

    App selectByPrimaryKey(String appId);

    App selectByCustomId(String appIdCustom);

    int updateByExampleSelective(@Param("record") App record, @Param("example") AppExample example);

    int updateByExample(@Param("record") App record, @Param("example") AppExample example);

    int updateByPrimaryKeySelective(App record);

    int updateByPrimaryKey(App record);

    int deleteByAppId(String appId);

    int SelectByCount(@Param("keyWord") String keyWord, @Param("appTypeId") String appTypeId,
    		@Param("deviceTypeId") String deviceTypeId, @Param("isPublic") String isPublic);

    List<App> SelectByList(@Param("offset") int offset, @Param("limit") int limit, @Param("keyWord") String keyWord,
    		@Param("order") String order, @Param("sort") String sort,@Param("appTypeId") String appTypeId,
    		@Param("deviceTypeId") String deviceTypeId, @Param("isPublic") String isPublic);
}