/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.persistence.mapper.user;

import java.util.List;
import app.market.model.user.User;
import app.market.model.user.UserExample;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;
import org.springframework.stereotype.Repository;

@Repository(value = "UserMapper")
public interface UserMapper {
	int countByExample(UserExample example);

	int deleteByExample(UserExample example);

	int deleteByPrimaryKey(String userId);

	int insert(User record);

	int insertSelective(User record);

	List<User> selectByExample(UserExample example);

	User selectByPrimaryKey(String userId);

    List<User> selectOptionByExample(UserExample example);

    User selectOneByExample(UserExample example);

    User selectByLoginId(String loginId);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

	int updateByExample(@Param("record") User record, @Param("example") UserExample example);

	int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int updatePassword(User record);

    int updateByPrimaryKeyWithBLOBs(User record);

    int insertOrUpdate(User record);

    List<User> selectByExample(RowBounds rowBounds, UserExample example);

	int deleteByUserName(String userName);

	int countByUserName(String userName);

	int countByUserId(String userId);

	User selectByUserName(String userName);
}