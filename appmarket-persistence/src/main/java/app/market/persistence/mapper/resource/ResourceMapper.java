/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.persistence.mapper.resource;

import java.util.List;
import app.market.model.resource.Resource;
import app.market.model.resource.ResourceExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository(value = "ResourceMapper")
public interface ResourceMapper {
	int countByExample(ResourceExample example);

	int deleteByExample(ResourceExample example);

	int deleteByPrimaryKey(String resId);

	int insert(Resource record);

	int insertSelective(Resource record);

	List<Resource> selectByExample(ResourceExample example);

	Resource selectByPrimaryKey(String resId);

	int updateByExampleSelective(@Param("record") Resource record, @Param("example") ResourceExample example);

	int updateByExample(@Param("record") Resource record, @Param("example") ResourceExample example);

	int updateByPrimaryKeySelective(Resource record);

	int updateByPrimaryKey(Resource record);

    List<Resource> selectResourcesByUserId(String userName);
}