/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.persistence.mapper.authority;

import java.util.List;
import app.market.model.authority.Authority;
import app.market.model.authority.AuthorityExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository(value = "AuthorityMapper")
public interface AuthorityMapper {
	int countByExample(AuthorityExample example);

	int deleteByExample(AuthorityExample example);

	int deleteByPrimaryKey(String auId);

	int insert(Authority record);

	int insertSelective(Authority record);

	List<Authority> selectByExample(AuthorityExample example);

	Authority selectByPrimaryKey(String auId);

	int updateByExampleSelective(@Param("record") Authority record, @Param("example") AuthorityExample example);

	int updateByExample(@Param("record") Authority record, @Param("example") AuthorityExample example);

	int updateByPrimaryKeySelective(Authority record);

	int updateByPrimaryKey(Authority record);

    List<Authority> selectOption();
}