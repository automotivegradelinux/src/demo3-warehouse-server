/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.persistence.mapper.user;

import java.util.List;
import app.market.model.user.UserAuthorityLinkExample;
import app.market.model.user.UserAuthorityLinkKey;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository(value = "UserAuthorityLinkMapper")
public interface UserAuthorityLinkMapper {
	int countByExample(UserAuthorityLinkExample example);

	int deleteByExample(UserAuthorityLinkExample example);

	int deleteByPrimaryKey(UserAuthorityLinkKey key);

	int insert(UserAuthorityLinkKey record);

	int insertSelective(UserAuthorityLinkKey record);

	List<UserAuthorityLinkKey> selectByExample(UserAuthorityLinkExample example);

	int updateByExampleSelective(@Param("record") UserAuthorityLinkKey record,
			@Param("example") UserAuthorityLinkExample example);

	int updateByExample(@Param("record") UserAuthorityLinkKey record,
			@Param("example") UserAuthorityLinkExample example);
}