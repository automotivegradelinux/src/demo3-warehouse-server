/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.persistence.mapper.authority;

import java.util.List;
import app.market.model.authority.AuthorityResourceLinkExample;
import app.market.model.authority.AuthorityResourceLinkKey;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Repository(value = "AuthorityResourceLinkMapper")
public interface AuthorityResourceLinkMapper {
	int countByExample(AuthorityResourceLinkExample example);

	int deleteByExample(AuthorityResourceLinkExample example);

	int deleteByPrimaryKey(AuthorityResourceLinkKey key);

	int insert(AuthorityResourceLinkKey record);

	int insertSelective(AuthorityResourceLinkKey record);

	List<AuthorityResourceLinkKey> selectByExample(AuthorityResourceLinkExample example);

	int updateByExampleSelective(@Param("record") AuthorityResourceLinkKey record,
			@Param("example") AuthorityResourceLinkExample example);

	int updateByExample(@Param("record") AuthorityResourceLinkKey record,
			@Param("example") AuthorityResourceLinkExample example);
}