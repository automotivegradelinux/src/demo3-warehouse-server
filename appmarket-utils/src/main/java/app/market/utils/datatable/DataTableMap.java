/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.datatable;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * datatable data struction
 *
 * @author Toyota
 */
public class DataTableMap implements Serializable {

    private static final long serialVersionUID = 4334941724837603916L;

    private LinkedHashMap<String, Object> map;

    public DataTableMap() {
        map = new LinkedHashMap<String, Object>();
    }

    /**
     * Init data table
     *
     * @param draw
     * @param list
     */
    public DataTableMap(String draw, List<?> list) {
        map = new LinkedHashMap<String, Object>();
        map.put( "draw", draw );
        map.put( "recordsTotal", 0 );
        map.put( "recordsFiltered", 0 );
        map.put( "data", list );
    }

    /**
     * Set page count
     *
     * @param total
     */
    public void setTotal(int total) {
        map.put( "recordsTotal", total );
        map.put( "recordsFiltered", total );
    }

    /**
     * Add data
     *
     * @param list
     */
    @SuppressWarnings("unchecked")
    public void addData(List<?> list) {
        ( (List<Object>) map.get( "data" ) ).addAll( list );
    }

    /**
     * Reset data
     *
     * @param list
     */
    public void resetData(List<?> list) {
        map.put( "data", list );
    }

    /**
     * Set error message
     *
     * @param errorCode
     * @param errorText
     */
    public void setErrorMsg(int errorCode, String errorText) {
        map.put( "statusCode", errorCode );
        map.put( "statusText", errorText );
    }

    /**
     * get data map
     *
     * @return
     */
    public LinkedHashMap<String, Object> getMapData() {
        return this.map;
    }
}
