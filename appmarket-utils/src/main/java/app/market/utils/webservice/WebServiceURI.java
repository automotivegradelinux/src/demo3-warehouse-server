/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.webservice;

public class WebServiceURI {

    /** token expire */
    public static final String REST_TOKEN_VALIDATETOKENEXPIRE_LF = "/api/v1/validateTokenRest/validateTokenExpire";

    public static final String REST_TOKEN_VALIDATETOKENAUTHORTICATION_LF = "/api/v1/validateTokenRest/validateTokenAuthortication";

    public static final String REST_TOKEN_LF = "/api/v1/token/{refreshToken}";

    /** ▼▼▼▼▼▼▼▼▼▼ user ▼▼▼▼▼▼▼▼▼▼ */
    /** login */
    public static final String REST_USER_SELECTLOGINUSER_LF = "/api/v1/login";
    /** user collection */
    public static final String REST_USER_SELECTPAGINATIONDATABYEXAMPLE_LF = "/api/v1/user/collection";
    /** user search */
    public static final String REST_USER_BY_USERNAME_LF = "/api/v1/user/{userName}";
    /** get current user */
    public static final String REST_USER_SELECT_CURRENT_LF = "/api/v1/user/currentUser";
    /** save user */
    public static final String REST_USER_LF = "/api/v1/user";
    /** delete user */
    public static final String REST_USER_BY_USERID_LF = "/api/v1/user/{userId}";
    /** ▲▲▲▲▲▲▲▲▲▲ user ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ application ▼▼▼▼▼▼▼▼▼▼ */
    /** get application collection */
    public static final String REST_APP_COLLECTION_APP_LF = "/api/v1/app/collection";
    /** get application information */
    public static final String REST_APP_INFO_LF = "/api/v1/app/info";

    public static final String REST_APP_INFO_PARM_ID_LF = "/api/v1/app/info/{appId}";
    /** get application authority */
    public static final String REST_APP_INFO_PARM_CUSTOMID_LF = "/api/v1/app/info/dev/{appIdCustom}";

    public static final String REST_APP_VERSION_INFO_LF = "/api/v1/app/info/version";

    /** application upload */
    public static final String REST_APP_FILE_LF = "/api/v1/app/file";

    public static final String REST_APP_FILE_PARM_FILENAME_TYPEID_APPID_LF = "/api/v1/app/file/{fileName}/{appDeviceTypeId}/{appId}";

    public static final String REST_APP_FILE_PARM_FILEPATH_LF = "/api/v1/app/file";

    public static final String REST_APP_IMAGE_LF = "/api/v1/app/image";
    /** ▲▲▲▲▲▲▲▲▲▲ application ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ authority ▼▼▼▼▼▼▼▼▼▼ */
    /** get authority list */
    public static final String REST_AUTHORITY_GET_LIST_OPTION_LF = "/api/v1/authority/collection";
    /** ▲▲▲▲▲▲▲▲▲▲ authority ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ resource ▼▼▼▼▼▼▼▼▼▼ */
    public static final String REST_RESOURCE_GET_MENU_RESOURCES_BY_LOGINID_LF = "/api/v1/resource/menuResource/collection";
    /** ▲▲▲▲▲▲▲▲▲▲ resource ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ dictionary ▼▼▼▼▼▼▼▼▼▼ */
    public static final String REST_DICTIONARY_COLLECTION_LF = "/api/v1/dictionary/collection/{dictionaryTypeId}";
    public static final String REST_DICTIONARY_INFO_LF = "/api/v1/dictionary/info";
    /** ▲▲▲▲▲▲▲▲▲▲ dictionary ▲▲▲▲▲▲▲▲▲▲ */
}
