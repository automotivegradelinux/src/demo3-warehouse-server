/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.webservice;

import app.market.utils.property.MessageUtil;

public class ErrorCode {

	private ErrorCodeEnum codeStr=null;
	private String message = "";

    /**
     *
     * @param codeStr
     * @param messageKey
     */
	public ErrorCode(ErrorCodeEnum codeStr, String messageKey) {
		super();
		this.codeStr = codeStr;

		String message = MessageUtil.getPropertites( messageKey );
		this.message = message;
	}

	public ErrorCodeEnum getCodeStr() {
		return codeStr;
	}
	public void setCodeStr(ErrorCodeEnum codeStr) {
		this.codeStr = codeStr;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
}
