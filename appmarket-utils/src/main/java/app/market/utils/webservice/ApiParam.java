/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.webservice;

import java.util.HashMap;

public class ApiParam {
	public static final String API_PARAM_SORT = "sort";
	public static final String API_PARAM_ORDER = "order";
	public static final String API_PARAM_OFFSET = "offset";
	public static final String API_PARAM_LIMIT = "limit";

	/**
	 *
	 */
	public static final String API_PARAM_VALUE_ORDER_ASC = "ASC";
	public static final String API_PARAM_VALUE_ORDER_DESC = "DESC";
	public static final String API_PARAM_VALUE_DICTIONARY_CATEGORY = "0100";
	public static final String API_PARAM_VALUE_DICTIONARY_DEVICETYPE = "0101";
	public static final String API_PARAM_VALUE_DICTIONARY_ISPUBLIC = "0102";

	/**
	 * パラメータの
	 */
	public static final String API_PARAM_DEFAULT_OFFSET = "0";
	public static final String API_PARAM_DEFAULT_LIMIT = "10";
	public static final int API_PARAM_CRITICAL_OFFSET = 0;
	public static final int API_PARAM_CRITICAL_LIMIT = 0;

	public static final String API_PARAM_DEFAULT_IS_PUBLIC = "1";
	public static final String API_PARAM_DEFAULT_NOT_PUBLIC = "0";

	public static final String API_PARAM_DEFAULT_SORT_NAME = "createDate";
	public static final String API_PARAM_DEFAULT_DEL = "0";

	/**
	 *
	 */
	public static final String API_PARAM_CRITICAL_LIMIT_MIN = "1";
	public static final String API_PARAM_CRITICAL_TYPE_MIN = "0";
	public static final String API_PARAM_CRITICAL_TYPE_MAX = "9";
	public static final String API_PARAM_CRITICAL_DEVICETYPE_MIN = "0";
	public static final String API_PARAM_CRITICAL_DEVICETYPE_MAX = "5";

	/**
	 *
	 */
	public static final int API_PARAM_APPNAME_LENGTH = 32;
	public static final int API_PARAM_USERNAME_LENGTH = 32;
	public static final int API_PARAM_VERSIONNAME_LENGTH = 32;
	public static final int API_PARAM_VERFILEPATH_LENGTH = 200;
	public static final int API_PARAM_APPABSTRACT_LENGTH = 800;
	public static final int API_PARAM_USERPASSMIN_LENGTH = 6;
	public static final int API_PARAM_USERPASSMAX_LENGTH = 32;
	public static final int API_PARAM_MAILADDRESS_LENGTH = 32;

	/**
	 *
	 */
	public static final String API_APP_PARAM_LOGINID = "loginId";
	public static final String API_APP_PARAM_REFRESHTOKEN = "refreshToken";
	public static final String API_APP_PARAM_PASSWORD = "password";
	public static final String API_APP_PARAM_MULTIPARTFILE = "multipartFile";
	public static final String API_APP_PARAM_FILE_HASH = "hashcode";
	public static final String API_APP_PARAM_APPTYPEID = "appTypeId";
	public static final String API_APP_PARAM_APPID = "appId";
	public static final String API_APP_PARAM_FILE_NAME = "fileName";
	public static final String API_APP_PARAM_FILE_PATH = "filePath";
	public static final String API_APP_PARAM_APPNAME = "appName";
	public static final String API_APP_PARAM_DEVELOPER = "appDeveloper";
	public static final String API_APP_PARAM_APPDEVICETYPEID = "appDeviceTypeId";
	public static final String API_APP_PARAM_APPDEVICETYPE = "appDeviceType";
	public static final String API_APP_PARAM_APPISPUBLIC = "appIsPublic";
	public static final String API_APP_PARAM_DICTIONARY_TYPEID = "dictionaryTypeId";
	public static final String API_APP_PARAM_DICTIONARY_VALUE = "dictionaryValue";
	public static final String API_APP_PARAM_DICTIONARY_LABEL = "dictionaryLabel";
	public static final String API_APP_PARAM_KEYWORD = "keyWord";
	public static final String API_APP_PARAM_APPID_CUSTOM = "appIdCustom";

	/**
	 *
	 */
	public static final String API_USER_PARAM_USERNAME = "userName";
	public static final String API_USER_PARAM_MAIL = "mail";
	public static final String API_USER_PARAM_CREATEDATE = "createDate";
	public static final String API_USER_PARAM_AUID = "auId";

	/**
	 *
	 */
	public static final String API_RESPONSE_TOKEN = "token";
	public static final String API_RESPONSE_REFRESHTOKEN = "refreshToken";
	public static final String API_RESPONSE_APPID = "appId";
	public static final String API_RESPONSE_APPVERSIONID = "appVersionId";
	public static final String API_RESPONSE_USERID = "userId";

	public static final  HashMap<String, String> AppQueryParam=new HashMap<String, String>();
	static
	{
		AppQueryParam.put("appId"             , "APP_ID");
	    AppQueryParam.put("appName"           , "APP_NAME");
	    AppQueryParam.put("appAbstract"       , "APP_ABSTRACT");
	    AppQueryParam.put("typeId"            , "APP_TYPE_ID");
	    AppQueryParam.put("typeName"          , "TYPE_NAME");
	    AppQueryParam.put("appDeviceTypeId"   , "APP_DEVICE_TYPE_ID");
	    AppQueryParam.put("appDeviceTypeName" , "APP_DEVICE_TYPE_NAME");
	    AppQueryParam.put("developer"         , "DEVELOPER");
	    AppQueryParam.put("developerName"     , "USER_NAME");
	    AppQueryParam.put("versionName"       , "VERSION_NAME");
	    AppQueryParam.put("verFileSize"       , "SIZE");
	    AppQueryParam.put("verCreateDate"     , "VERSION_CREATE_DATE");
	    AppQueryParam.put("createDate"        , "APP_CREATE_DATE");
	    AppQueryParam.put("updateDate"        , "APP_UPDATE_DATE");
	    AppQueryParam.put("hashcode"          , "MD5");

	}

	public static final  HashMap<String, String> UserQueryParam=new HashMap<String, String>();
	static
	{
		UserQueryParam.put("userId" , "USER_ID");
		UserQueryParam.put("userName" , "USER_NAME");
		UserQueryParam.put("mailAddress" , "MAIL_ADDRESS");
		UserQueryParam.put("createDate" , "CREATE_DATE");
		UserQueryParam.put("updateDate" , "UPDATE_DATE");
		UserQueryParam.put("auId" , "A_AU_ID");
		UserQueryParam.put("auName" , "AU_NAME");
	}

	public enum UserQueryParamWeb {
		USERNAME("userName", 1)
		, USERMAILADDRESS("mailAddress", 2)
		, USERCREATEDATE("createDate", 3);

		private String fieldName;
		private int paramId;

		private UserQueryParamWeb(String fieldName, int paramId) {
			this.fieldName = fieldName;
			this.paramId = paramId;
		}

		public static String getFieldNameById(int id) {
			for (UserQueryParamWeb c : UserQueryParamWeb.values()) {
				if (c.getId() == id) {
					return c.fieldName;
				}
			}
			return null;
		}

		public int getId() {
			return paramId;
		}
	}

	public enum UserAuId {
		ADIMIN("ADIMIN", 1)
		, DEVELOPER("DEVELOPER", 2)
		, NORMAL("NORMAL", 3);

		private String fieldName;
		private int paramId;

		private UserAuId(String fieldName, int paramId) {
			this.fieldName = fieldName;
			this.paramId = paramId;
		}

		public static String getFieldNameById(int id) {
			for (UserAuId c : UserAuId.values()) {
				if (c.getId() == id) {
					return c.fieldName;
				}
			}
			return null;
		}

		public int getId() {
			return paramId;
		}
	}
}
