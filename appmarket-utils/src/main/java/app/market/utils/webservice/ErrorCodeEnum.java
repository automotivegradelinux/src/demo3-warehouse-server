/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.webservice;

public enum ErrorCodeEnum{

	//●400リ request error
	INVALID_BODY,INVALID_QUERYPARAM,MISSING_NECESSARY_QUERYPARAM,MISSING_RESOURCE,BAD_FAILED,ALREADY_UPDATED,

	//●401 authorized error
	UNAUTHORIZED_API,EXPIRED_TOKEN,EXPIRED_REFRESH_TOKEN,LOGIN_FAILD,

	//●402 MD5 error
	MD5_FAILED,

	//●403 forbidden
	FORBIDDEN_RESOURCE,

	//●409 already exist
	RESOURCE_ALREADY_EXISTS,CORRELATION_RESOURCE_ALREADY_EXISTS,TYPENAME_ALREADY_EXISTS,

	//●415 unsupport file
	UNSUPPORT_FILE,

	//●503 system maintenance
	UNDER_MAINTENANCE,

}
