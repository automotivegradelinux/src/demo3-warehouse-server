/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.datetime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * date
 *
 * @author Toyota
 * @date 2017/10/10
 */
public class DateTimeUtils {

    /** yyyy */
    public static final String DATE_FORMAT_YYYY = "yyyy";
    /** yyyy/MM/dd */
    public static final String DATE_FORMAT_YYYYMMDD = "yyyy/MM/dd";
    /** yyyy/MM/dd hh:mm:ss */
    public static final String DATE_FORMAT_YYYYMMDDHHMMSS_SLASH = "yyyy/MM/dd hh:mm:ss";
    /** yyyy-MM-dd hh:mm:ss */
    public static final String DATE_FORMAT_YYYYMMDDHHMMSS_DASH = "yyyy-MM-dd hh:mm:ss";
    /** yyyyMMddhhmmss */
    public static final String DATE_FORMAT_YYYYMMDDHHMMSS = "yyyyMMddhhmmss";
    /** yyyyMMddhhmmssSSS */
    public static final String DATE_FORMAT_YYYYMMDDHHMMSSSSS = "yyyyMMddhhmmssSSS";

    public static String getCurrectDate(String format) {
        return new SimpleDateFormat( format ).format( new Date() );
    }

    public static String getDate(String format, Date date) {
        return new SimpleDateFormat( format ).format( date );
    }

    public static Date getDate(String format, String date) {
        try {
            return new SimpleDateFormat( format ).parse( date );
        } catch ( ParseException e ) {
            throw new RuntimeException();
        }
    }

    /**
     * @param date
     * @return
     */
    public static String betweenTime(String date){
    	String strDate=date.replace("/","");
        int intDate = Integer.parseInt(strDate);
        String end = String.valueOf(intDate+1);
        StringBuilder sbOne = new StringBuilder(end);
        String  StrDateOne = sbOne.insert(4, "/").toString();
        StringBuilder sbTwo = new StringBuilder(StrDateOne);
        String  StrDateTwo = sbTwo.insert(7, "/").toString();
		return StrDateTwo;

    }
    public static void main(String[] args) {
        String s = DateTimeUtils.getDate( DATE_FORMAT_YYYYMMDDHHMMSS, new Date() );
        System.out.println( s );
    }
}
