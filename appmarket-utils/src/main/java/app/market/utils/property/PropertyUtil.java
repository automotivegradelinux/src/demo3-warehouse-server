/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.property;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PropertyUtil {

    public static final String DEFAULLABEL = "please choose";

    private static final String ESCAPE_COLON = "&col&";

    public static Properties prop;

    private static Properties getPropertyInstance() {
        if ( prop == null ) {
            prop = new Properties();
            try {
                prop.load( new InputStreamReader(
                        PropertyUtil.class.getClassLoader().getResourceAsStream( "property.properties" ), "UTF-8" ) );
            } catch ( FileNotFoundException e ) {
                throw new RuntimeException();
            } catch ( IOException e ) {
                throw new RuntimeException();
            }
        }
        return PropertyUtil.prop;
    }

    /**
     * Get propertites
     *
     * @param type
     * @return String
     */
    public static String getPropertites(String type) {
        prop = getPropertyInstance();
        return prop.getProperty( type );
    }

    /**
     * Get dropdown list
     *
     * @param type
     * @param hasSpace
     * @return List<Option>
     */
    public static List<Option> getPropertites(String type, boolean hasSpace) {
        prop = getPropertyInstance();
        Pattern pattern = Pattern.compile( type + "[0-9]" );
        Matcher matcher = null;

        final List<Option> selectList = new ArrayList<Option>();
        Option option = null;
        if ( hasSpace ) {
            option = new Option( DEFAULLABEL, "" );
            selectList.add( option );
        }
        Enumeration<?> keys = prop.keys();
        while ( keys.hasMoreElements() ) {
            String key = (String) keys.nextElement();
            matcher = pattern.matcher( key );
            if ( matcher.matches() ) {
                String value = prop.getProperty( key );
                String[] split = value.split( ":" );
                String escapeLabel = split[1].trim();
                if ( -1 != escapeLabel.lastIndexOf( ESCAPE_COLON ) ) {
                    escapeLabel = escapeLabel.replace( ESCAPE_COLON, ":" );
                }
                option = new Option( escapeLabel, split[0] );
                selectList.add( option );
            }
        }
        return selectList;
    }

    /**
     * Get label
     *
     * @param type
     * @param value
     * @return
     */
    public static String getPropertites(String type, String value) {
        prop = getPropertyInstance();
        Pattern pattern = Pattern.compile( type + "[0-9]" );
        Matcher matcher = null;

        String str = "";
        Enumeration<?> keys = prop.keys();
        while ( keys.hasMoreElements() ) {
            String key = (String) keys.nextElement();
            matcher = pattern.matcher( key );
            if ( matcher.matches() ) {
                String sValue = prop.getProperty( key );
                String[] split = sValue.split( ":" );
                if ( value.equals( split[0] ) ) {
                    str = split[1];
                    break;
                }
            }
        }
        return str;
    }

}
