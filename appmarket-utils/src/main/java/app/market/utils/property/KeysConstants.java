/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.property;

public class KeysConstants {
    public static final String PROJECT_ERROR = "project_error";
    public static final String STATUS_UNAUTHORIZED = "status_unauthorized";
    public static final String STATUS_TOO_MANY_CONNECTIONS = "status_too_many_connections";
    public static final String STATUS_FORBIDDEN = "status_forbidden";
    public static final String STATUS_BAD_REQUEST = "bad_request";
    public static final String INVALID_QUERYPARAM = "invalid_query_param";
    public static final String INVALID_BODY = "invalid_body";
    public static final String ALREADY_EXIST = "already_exist";
    public static final String USER_NOT_DELETE = "user_not_delete";
    public static final String EXPIRED_REFRESH_TOKEN = "expired_refresh_token";
    /** ▼▼▼▼▼▼▼▼▼▼ ErrorCode Message ▼▼▼▼▼▼▼▼▼▼ */
    public static final String MISSING_NECESSARY_QUERYPARAM = "missing_necessary_queryparam";
    public static final String THE_PICTURE_SIZES = "the_picture_sizes_should_not_exceed_500KB";
    public static final String UPLOAD_PICTURES_ONLY = "you_can_upload_pictures_only";
    public static final String INVALID_OPERATION = "invalid_operation";
    public static final String RESOURCE_ALREADY_EXISTS = "resource_already_exists";
    public static final String RESOURCE_APP_ALREADY_EXISTS = "resource_app_already_exists";
    public static final String DEVELOPER_IS_NOT_EXIST = "developer_is_not_exist";
    public static final String TYPENAME_ALREADY_EXISTS = "typename_already_exists";
    /** ▲▲▲▲▲▲▲▲▲▲ ErrorCode Message ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ login ▼▼▼▼▼▼▼▼▼▼ */
    public static final String LOGIN_LOGINID_IS_NOT_EXIST = "login_loginId_is_not_exist";
    public static final String LOGIN_LOGINID_IS_NOT_EMPTY = "login_loginId_is_not_empty";
    public static final String LOGIN_PASSWORD_IS_NOT_EMPTY = "login_password_is_not_empty";
    /** ▲▲▲▲▲▲▲▲▲▲ login ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ user ▼▼▼▼▼▼▼▼▼▼ */
    public static final String USER_PASSWORD_IS_NOT_EMPTY = "user_password_is_not_empty";
    public static final String USER_PASSWORD_IS_NOT_SPACES = "no_spaces_can_be_found_in_the_password";
    public static final String USER_REPASSWORD_IS_NOT_EMPTY = "user_repassword_is_not_empty";
    public static final String USER_USERNAME_IS_NOT_EMPTY = "user_username_is_not_empty";
    public static final String USER_USERNAME_MAX_ERROR = "user_username_max_error";
    public static final String USER_PASSWORD_MAX_ERROR = "user_password_max_error";
    public static final String USER_REPASSWORD_MAX_ERROR = "user_repassword_max_error";
    public static final String USER_MAILADDRESS_MAX_ERROR = "user_mailaddress_max_error";
    public static final String USER_USERNAME_IS_NOT_REPEATED = "user_username_is_not_repeated";
    public static final String USER_MAILADDRESS_IS_NOT_EMPTY = "user_mailaddress_is_not_empty";
    public static final String USER_USERID_IS_NOT_EMPTY = "user_userid_is_not_empty";
    public static final String USER_OLD_PASSWORD_IS_NOT_EMPTY = "user_old_password_is_not_empty";
    public static final String USER_NEW_PASSWORD_IS_NOT_EMPTY = "user_new_password_is_not_empty";
    public static final String USER_NEW_REPASSWORD_IS_NOT_EMPTY = "user_new_repassword_is_not_empty";
    public static final String USER_NEW_REPASSWORD_IS_NOT_EQUALS = "user_new_repassword_is_not_equals";
    public static final String USER_SAVE_IS_FAILED = "user_save_is_failed";
    public static final String USER_SAVE_IS_SUCCESS = "user_save_is_success";
    public static final String USER_SAVE_IS_EXIST = "user_is_exist";
    public static final String USER_LOGINID_IS_FAILED = "user_loginid_is_failed";
    public static final String USER_INFO_CHANGE_SUCCESS = "user_info_change_success";
    public static final String USER_IS_SUCCESS = "user_is_success";
    public static final String USER_TITLE_NAME_INSERT = "user_title_name_insert";
    public static final String USER_TITLE_NAME_MODIFY = "user_title_name_modify";
    public static final String USER_NAME_IS_NOT_EMPTY = "user_name_is_not_empty";
    public static final String USER_GET_LIST_IS_FAILED = "user_get_list_is_failed";
    public static final String USER_DELETE_FAILED = "user_delete_failed";
    public static final String USER_MAILADDRESS_IS_NOT_EQUALS = "user_mailaddress_is_not_equals";
    public static final String USER_REPASSWORD_ERROR = "user_repassword_error";
    public static final String USER_PASSWORD_ERROR = "user_password_error";
    /** ▲▲▲▲▲▲▲▲▲▲ user ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ app ▼▼▼▼▼▼▼▼▼▼ */
    public static final String APP_APPNAME_IS_NOT_EMPTY = "app_appname_is_not_empty";
    public static final String APP_DEVICETYPE_IS_NOT_EMPTY = "app_DeviceType_is_not_empty";
    public static final String APP_VERSIONNAME_IS_NOT_EMPTY = "app_versionname_is_not_empty";
    public static final String APP_TYPEID_IS_NOT_EMPTY = "app_typeid_is_not_empty";
    public static final String APP_FILEPATH_IS_NOT_EMPTY = "app_filepath_is_not_empty";
    public static final String APP_ABSTRACT_IS_NOT_EMPTY = "app_abstract_is_not_empty";
    public static final String APP_IMAGRPATH_IS_NOT_EMPTY = "app_imagepath_is_not_empty";
    public static final String APP_SAVE_IS_FAILED = "app_save_is_failed";
    public static final String APP_SAVE_IS_SUCCESS = "app_save_is_success";
    public static final String APP_UPLOAD_PARAM_FILE_IS_NULL = "app_upload_param_file_is_null";
    public static final String APP_UPLOAD_MD5 = "md5_failed";
    public static final String APP_TITLE_NAME_MODIFY = "app_title_name_modify";
    public static final String APP_TITLE_NAME_INSERT = "app_title_name_insert";
    public static final String APP_APPNAME_MAX_ERROR = "app_appname_max_error";
    public static final String APP_FILEPATH_MAX_ERROR = "app_filepath_max_error";
    public static final String APP_VERSIONNAME_MAX_ERROR = "app_versionname_max_error";
    public static final String APP_ABSTRACT_MAX_ERROR = "app_abstract_max_error";
    public static final String APP_FILE_TYPE_IS_UNSUPPORTED = "app_file_type_is_unsupported";
    public static final String APP_FILE_READ_FAILED = "app_file_read_failed";
    public static final String APP_FILE_UNCOMPRESS_FAILED = "app_file_uncompress_failed";
    /** ▲▲▲▲▲▲▲▲▲▲ app ▲▲▲▲▲▲▲▲▲▲ */

    /** ▼▼▼▼▼▼▼▼▼▼ authority ▼▼▼▼▼▼▼▼▼▼ */
    public static final String AUTHORITY_ID_IS_NOT_EMPTY = "authority_id_is_not_empty";
    /** ▼▼▼▼▼▼▼▼▼▼ authority ▼▼▼▼▼▼▼▼▼▼ */

    /** ▼▼▼▼▼▼▼▼▼▼ system ▼▼▼▼▼▼▼▼▼▼ */
    public static final String SYS_ERROR_SAVE_SUCCESS = "sys_error_save_success";
    /** ▼▼▼▼▼▼▼▼▼▼ system ▼▼▼▼▼▼▼▼▼▼ */


}
