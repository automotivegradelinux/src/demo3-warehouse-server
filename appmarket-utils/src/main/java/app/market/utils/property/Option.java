/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.property;

import java.io.Serializable;

/**
 * Page select's option
 *
 * @author Toyota
 */
public class Option implements Serializable, Comparable<Option> {

    private static final long serialVersionUID = 3717357659129461807L;

    private String label;
    private String value;

    public Option() {
    }

    public Option(String label, String value) {
        this.label = label;
        this.value = value;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label == null ? "" : label;
    }

    /**
     * @param label
     *            the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int compareTo(Option o) {
        int b = Integer.valueOf( o.getValue() );
        int n = ( this.getValue() == null || "".equals( this.getValue().trim() ) ) ? 0
                : Integer.valueOf( this.getValue() );
        return b > n ? -1 : 1;
    }

}
