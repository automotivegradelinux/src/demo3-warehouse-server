/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.property;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

import app.market.utils.constants.Constants;

public class MessageUtil {

    public static Properties prop = null;
    public static String i18n = Constants.I18N_JA_JP;

    private static Properties getPropertyInstance() {
        if ( prop == null ) {
            prop = new Properties();
            try {
                prop.load( new InputStreamReader( MessageUtil.class.getClassLoader().getResourceAsStream( "message." + i18n + ".properties" ), "UTF-8" ) );
            } catch ( FileNotFoundException e ) {
                throw new RuntimeException();
            } catch ( IOException e ) {
                throw new RuntimeException();
            }
        }
        return MessageUtil.prop;
    }

    /**
     * Get propertites
     *
     * @param type
     * @return String
     */
    public static String getPropertites(String type) {
        prop = getPropertyInstance();
        return prop.getProperty( type );
    }

}
