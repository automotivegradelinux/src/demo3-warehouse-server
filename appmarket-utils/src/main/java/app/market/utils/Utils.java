/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import app.market.utils.constants.Constants;
import app.market.utils.datetime.DateTimeUtils;

/**
 * tools
 *
 * @author Toyota
 * @date 2017/10/10
 */
public class Utils {

    /**
     * Entity converter
     *
     * @see set in/out class
     *
     * @param org
     * @param dest
     * @throws NoSuchFieldException
     * @throws SecurityException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static void reflect(Object org, Object dest) throws Exception {
        Class<?> orgClass = org.getClass();
        Class<?> destClass = dest.getClass();

        for (Field orgField : orgClass.getDeclaredFields()) {
            orgField.setAccessible( true );
            String orgFieldName = orgField.getName();
            if ( "serialVersionUID".equals( orgFieldName ) ) {
                continue;
            }
            Field destField;
            try {
                destField = destClass.getDeclaredField( orgFieldName );
            } catch ( NoSuchFieldException e ) {
                continue;
            }
            destField.setAccessible( true );
            if ( ( orgField.getType().getName().equals( "java.lang.Integer" )
                    || orgField.getType().getName().equals( "int" ) ) && orgField.get( org ) != null ) {
                if ( destField.getType().getName().equals( "java.lang.String" ) ) {
                    destField.set( dest, String.valueOf( orgField.get( org ) ) );
                } else {
                    destField.set( dest, orgField.get( org ) );
                }
            } else if ( orgField.getType().getName().equals( "java.util.Date" ) && orgField.get( org ) != null ) {
                // to string
                if ( destField.getType().getName().equals( "java.lang.String" ) ) {
                    destField.set( dest, DateTimeUtils.getDate( DateTimeUtils.DATE_FORMAT_YYYYMMDDHHMMSS,
                            (Date) orgField.get( org ) ) );
                } else {
                    destField.set( dest, (Date) orgField.get( org ) );
                }
            } else if ( orgField.getType().getName().equals( "java.util.List" ) ) {
                for (int i = 0; i < ( (ArrayList<?>) orgField.get( org ) ).size(); i++) {
                    Object orgItem = ( (ArrayList<?>) orgField.get( org ) ).get( i );
                    Type destItemType = ( (ParameterizedType) destField.getGenericType() ).getActualTypeArguments()[0];
                    Object destItem = ( Class.forName( destItemType.getTypeName() ) ).newInstance();
                    reflect( orgItem, destItem );
                    ( (ArrayList<Object>) destField.get( dest ) ).add( destItem );
                }
            } else {
                if ( destField.getType().getName().equals( "java.util.Date" ) && orgField.get( org ) != null
                        && StringUtils.isNotEmpty( orgField.get( org ).toString() ) ) {
                    destField.set( dest, DateTimeUtils.getDate( DateTimeUtils.DATE_FORMAT_YYYYMMDD,
                            orgField.get( org ).toString() ) );
                } else {
                    if ( destField.getType().getName().equals( orgField.getType().getName() ) ) {
                        destField.set( dest, orgField.get( org ) );
                    }
                }
            }
        }
    }

    /**
     * Get UUID
     *
     * @return
     */
    public static String generatorUUID() {
        return UUID.randomUUID().toString();
    }

	public static boolean strIsEmpty(String s) {
		if (s == null || "".equals(s)) {
			return true;
		}
		return false;
	}

	/***
	 * Get file name from path
	 * @param filePath
	 * @return succeed, return file name
	 *         failed, return null
	 */
	public static String getFileNameFromPath(String filePath) {
		String ret = "";
		try{
			ret = filePath.substring(filePath.lastIndexOf(Constants.PATH_SEPARATOR) + 1);
		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
		return ret;
	}
}
