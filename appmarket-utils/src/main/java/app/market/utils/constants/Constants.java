/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.constants;

import org.springframework.http.HttpStatus;

public class Constants {

    public static final String CHARACTER_UTF8 = "UTF-8";
    public static final String CHARACTER_ISO = "ISO-8859-1";

    /** success */
    public static final int STATUS_SUCCESS = HttpStatus.OK.value();
    /** unauthorized */
    public static final int STATUS_UNAUTHORIZED = 401;

    public static final int STATUS_REFRESH_TOKEN = 220;

    public static final int STATUS_BAD_FILE = 402;
    /** forbidden */
    public static final int STATUS_FORBIDDEN = 403;
    /** already exists */
    public static final int STATUS_ALREADY_EXISTS = 409;
    /** unsupport media type */
    public static final int STATUS_UNSUPPORT = 415;
    /** too many connections */
    public static final int STATUS_TOO_MANY_CONNECTIONS = 421;
    /** bad request */
    public static final int STATUS_BAD_REQUEST = HttpStatus.BAD_REQUEST.value();
    /** error */
    public static final int STATUS_ERROR = HttpStatus.INTERNAL_SERVER_ERROR.value();

    public static final int NOT_FOUND = HttpStatus.NOT_FOUND.value();

    public static final int STATUS_CONFLICT = HttpStatus.CONFLICT.value();

    public static final int STATUS_SERVER_ERROR = 500;
    /** If parameter is blank, replace restful parameter. */
    public static final String EMPTY_STRING_FALG = "%22%22";
    /** Usefull deta flag */
    public static final String DETA_ENABLE_FLAG = "0";

    /** token */
    public static final String GUEST_ID = "appMarketGuestAuthorizationUser";
    public static final String ADMID_ID = "super_user";

    public static final String TOKEN_AUTHORIZATION = "authorization_token";
    public static final String TOKEN_AUTHORIZATION_REFRESH = "authorization_refreshtoken";
    public static final String TOKEN_AUTHORIZATION_RESOURCE = "authorization_servlet_path";
    public static final String AUTHORIZATION_CLIENT = "authorization_client";
    public static final String AUTHORIZATION_CLIENT_VALUE_TOOL = "tool";

    public static final String PAGINATION_COUNTER = "pagination_counter";
    public static final String PAGINATION_DATA = "pagination_data";

    public static final String USER_SESSION_KEY = "USER_SESSION_KEY";
    public static final String I18N_JA_JP = "ja.JP";

    /** uploading */
    public static final int BUFFER_SIZE = 1024;
    public static final String FILE_TYPE = ".wgt";
    public static final long IMAGE_SIZE = 1024*500;
    public static final String IMAGE_TYPE = "image";
    public static final int IMAGE_INT = 0;
    public static final String IMAGE_SVG = ".svg";
    public static final String IMAGE_PNG = ".PNG";

    /** distinct modify or create app*/
    public static final int APP_CREATE_DETAIL = 0;
    public static final int APP_MODIFY = 1;
    public static final int APP_DETAIL_MODIFY = 2;

    //database delete flad
	public static final String DATA_INVALID = "1";
	public static final String DATA_VALID = "0";

	public static final String TOKEN_SEPARATOR = ":";

	public static final String PROPERTIES_FILE_NAME_PROPERTIES = "config/properties.properties";

	/**
	 * webservice Utils
	 */
	public static final String PROPERTIES_FILE_NAME_REDIS = "redis.properties";
	public static final String PROPERTIES_FILE_NAME_FILE = "fileManager.properties";
	public static final String PROPERTIES_FILE_NAME_DEFAULT = "properties.properties";

	public static final String UPLOAD_PATH_DEFAULT="/var/lib/tomcat8/appFile/appMarket/appFile/";
	public static final String UPLOAD_PATH_TYPE_DEFAULT="appType/";
	public static final String UPLOAD_PATH_APP_DEFAULT="appId/";

	public static final String PATH_SEPARATOR="/";

	public static final String FILE_SEPARATOR = "_";

	public static final String FILE_DOT = ".";

	//public static final String SERVER_PATH = "http://localhost:8080/resource";
	public static final String UPLOAD_APP_FILE_PATH = "/appFile";
	public static final String UPLOAD_APP_RES_PATH_ICON = "/appIcon";
	public static final String UPLOAD_APP_UNCOMPRESS_PATH = "/uncompress";

	public static final String UPLOAD_APP_UNCOMPRESS_FILE_CONFIG = "/config.xml";


	/**
	 * wgt/config.xml
	 */
	public static final String CONFIG_APP_PARAM_ID = "id";
	public static final String CONFIG_APP_PARAM_VERSION = "version";
	public static final String CONFIG_APP_PARAM_NAME = "name";
	public static final String CONFIG_APP_PARAM_ICON = "icon";
	public static final String CONFIG_APP_PARAM_SRC = "src";
	public static final String CONFIG_APP_PARAM_DESCRIPTION = "description";

	/***
	 * local app list
	 */
	public static final String APP_ID_SEPARATOR = "@";
	public static final String APP_ID_LAUNCHER = "launcher@0.1";
	public static final String APP_ID_HOMESCREEN = "homescreen@0.1";
	public static final String APP_ID_WAREHOUSE = "warehouse@1.0";

	/**
	 * download app
	 */
	public static final String APP_HEADER_KEY = "Content-Disposition";
	public static final String APP_HEADER_VALUE = "attachment;filename=";

	/**
	 * session
	 */
	public static final String SESSION_TOKEN = "session_token";
	public static final String SESSION_REFRESH_TOKEN = "session_refresh_token";
	public static final String SESSION_PORT = "session_port";
	public static final String SESSION_SYSTEM_TOKEN = "session_system_token";
	public static final String SESSION_INSTALL_PATH = "session_install_path";

	/**
	 * access url parameter/
	 */
	public static final String ACCESS_PORT = "port";
	public static final String ACCESS_SYSTEM_TOKEN = "systemToken";
	public static final String ACCESS_INSTALL_PATH = "installPath";
}
