/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.constants;

import org.apache.commons.lang3.StringUtils;

/**
 * User define in project
 *
 * @author Toyota
 */
public enum ProjectUserRoleEnum {

    MANAGER("pm", "0"), LEADER("leader", "1"), MEMBER("member", "2"), ALL("all", "99");

    private String name;
    private String value;

    private ProjectUserRoleEnum(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public static String getName(String value) {
        for (ProjectUserRoleEnum c : ProjectUserRoleEnum.values()) {
            if ( StringUtils.equalsIgnoreCase( c.getValue(), value ) ) {
                return c.name;
            }
        }
        return null;
    }

    public static String getValue(String name) {
        for (ProjectUserRoleEnum c : ProjectUserRoleEnum.values()) {
            if ( StringUtils.equalsIgnoreCase( c.getName(), name ) ) {
                return c.value;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
