/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.token;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TokenModel implements Serializable {

    private static final long serialVersionUID = 2544696323418911040L;

    private String loginId;
    private String token; //accessToken
    private String refreshToken; //refreshToken
    private String ipAddress;
    private Object userEntity;
    private List<Object> apiList = new ArrayList<>();
    private List<Object> resourceList = new ArrayList<>();

    public TokenModel() {

    }

    public TokenModel(String loginId, Object user, String token) {
        this.loginId = loginId;
        this.userEntity = user;
        this.token = token;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Object getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(Object userEntity) {
        this.userEntity = userEntity;
    }

    public List<Object> getResourceList() {
        return resourceList;
    }

    public void setResourceList(List<Object> resourceList) {
        this.resourceList = resourceList;
    }

	public List<Object> getApiList() {
		return apiList;
	}

	public void setApiList(List<Object> apiList) {
		this.apiList = apiList;
	}

}
