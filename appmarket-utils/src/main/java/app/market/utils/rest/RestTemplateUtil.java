/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.rest;

import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import app.market.utils.SpringWebUtil;
import app.market.utils.constants.Constants;
import app.market.utils.json.JsonResult;
import app.market.utils.property.KeysConstants;
import app.market.utils.property.MessageUtil;
import app.market.utils.webservice.ApiParam;

public class RestTemplateUtil {

    private RestTemplate restTemplate;

    public RestTemplateUtil() {
        this.restTemplate = new RestTemplate();
    }

    /**
     *
     * @param url
     * @param model
     * @return
     * @throws Exception
     */
    public String post(String url, Object model, HttpSession session) throws Exception {
        HttpHeaders requestHeaders = new HttpHeaders();
        Class<?> clx = model.getClass();
        //String token = SpringWebUtil.getRequest().getParameter( "token" );
        String token = (String) session.getAttribute(Constants.SESSION_TOKEN);
        String servletPath = SpringWebUtil.getRequest().getServletPath();
        if ( StringUtils.isEmpty( token ) || StringUtils.isEmpty( servletPath ) )
            throw new HttpClientErrorException( HttpStatus.UNAUTHORIZED,
                    MessageUtil.getPropertites( KeysConstants.STATUS_UNAUTHORIZED ) );
        requestHeaders.add( Constants.TOKEN_AUTHORIZATION, token );
        //requestHeaders.add( Constants.TOKEN_AUTHORIZATION_RESOURCE, servletPath );

        HttpEntity<Object> requestEntity = new HttpEntity<Object>( clx.getClass().forName( clx.getName() ).cast( model ), requestHeaders );
        String result = restTemplate.postForObject( url, requestEntity, String.class );
        return result;
    }



    /**
     * Restful GET
     * @param url
     * @param params
     * @return
     * @throws Exception
     */
    public String get(String url,HttpSession session, String... params) throws Exception {
    	HttpEntity<Object> requestEntity = getRequestEntity(null, session);

        ResponseEntity<String> result = restTemplate.exchange( url, HttpMethod.GET, requestEntity, String.class, params );
        return result.getBody();
    }

    /**
     * getQueryParam
     * @param url
     * @param queryParams
     * @return
     * @throws Exception
     */
	public String getByQueryParam(String url, Map<String, Object> queryParams, HttpSession session) throws Exception {
		HttpEntity<Object> requestEntity = getRequestEntity(null, session);
		url = addParamURL(url, queryParams);
		ResponseEntity<String> result = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class,	queryParams);
		return result.getBody();
	}

    /**
     * Restful GET (download)
     * @param url
     * @param param
     * @return
     * @throws Exception
     */
	public ResponseEntity<byte[]> download(String url, Map<String, Object> queryParams, HttpSession session) throws Exception {
		HttpEntity<Object> requestEntity = getRequestEntity(null, session);

		url = addParamURL(url, queryParams);

		ResponseEntity<byte[]> exchange = restTemplate.exchange(url, HttpMethod.GET, requestEntity, byte[].class);
		return exchange;
	}

    /**
     * Restful Post
     * @param restUserSelectloginuser
     * @param postData
     * @return
     * @throws Exception
     */
    public ResponseEntity<String> Post(String restUserSelectloginuser, Object postData, HttpSession session) throws Exception {
        ResponseEntity<String> rEntity = restTemplate.postForEntity( restUserSelectloginuser, postData, String.class);
        return rEntity;
    }

    /**
     * Restful POST(update)
     * @param url
     * @param param
     * @param class1
     * @return
     * @throws ClassNotFoundException
     */
    public String post(String url, MultiValueMap<String, Object> queryParam, Class<String> classz, String[] pathParam, HttpSession session)
            throws ClassNotFoundException {
    	HttpEntity<Object> requestEntity = getRequestEntity(queryParam, session);

    	String result = null;

		if(pathParam == null){
    		result  = restTemplate.postForObject( url, requestEntity, String.class);
    	}else{
    		result = restTemplate.postForObject( url, requestEntity, String.class, pathParam);
    	}

        return result;
    }

    /**
     * Restful DELETE
     * @param url
     * @param id
     * @return
     * @throws Exception
     */
	public String delete(String url, String id, HttpSession session) throws Exception {
		String json = "";
	    HttpEntity<Object> requestEntity = getRequestEntity(id, session);
		ResponseEntity<String> exchange = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, String.class, id);
		json = exchange.getBody();
		return json;
	}

/*===========================================================================================================================
 * PRIVATE METHOD
===========================================================================================================================*/

    /**
     * getRequestEntity
     * @param body
     * @param session
     * @return
     */
	private HttpEntity<Object> getRequestEntity(Object body, HttpSession session)
	{
		HttpHeaders requestHeaders = new HttpHeaders();
		//String token = SpringWebUtil.getRequest().getParameter(ApiParam.API_RESPONSE_TOKEN);
		String token = (String) session.getAttribute(Constants.SESSION_TOKEN);
		if(token == null) token = "";
		String servletPath = SpringWebUtil.getRequest().getServletPath();
		if (StringUtils.isEmpty(servletPath)) {
			throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED,
					MessageUtil.getPropertites(KeysConstants.STATUS_UNAUTHORIZED));
		}
		requestHeaders.add(Constants.TOKEN_AUTHORIZATION, token);
		//requestHeaders.add(Constants.TOKEN_AUTHORIZATION_RESOURCE, servletPath);

		HttpEntity<Object> requestEntity = new HttpEntity<Object>( body, requestHeaders );
		return requestEntity;
	}

	private String addParamURL(String url, Map<String, Object> queryParams ){
		url += "?";
		for (String key : queryParams.keySet()) {
			if (!url.endsWith("?")) {
				url += "&";
			}
			Object val = queryParams.get(key);
			if(val != null){
				url += key + "=" + val.toString();
			}
		}
		return url;
	}
}
