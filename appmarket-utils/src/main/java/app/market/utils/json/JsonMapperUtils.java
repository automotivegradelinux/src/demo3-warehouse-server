/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.utils.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.JavaType;
import org.codehaus.jackson.type.TypeReference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import app.market.utils.constants.Constants;

/**
 * JSON Tool
 *
 * @author Toyota
 */
public class JsonMapperUtils {
    private static Logger logger = LoggerFactory.getLogger( JsonMapperUtils.class );

    private static ObjectMapper mapper = new ObjectMapper();

    public JsonMapperUtils() {
        super();
    }

    public static ObjectMapper getInstance() {
        return mapper;
    }

    /**
     * write object value
     *
     * @param object
     * @return
     */
    public static String writeValueAsString(Object object) {
        String json = null;
        try {
            json = mapper.writeValueAsString( object );
        } catch ( JsonGenerationException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        } catch ( JsonMappingException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        } catch ( IOException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        }
        return json;
    }

    /**
     * read object value
     *
     * @see #constructParametricType(Class, Class...)
     */
    @SuppressWarnings("unchecked")
    public static <T> T readValue(String jsonString, Class<T> clazz) {
        if ( StringUtils.isEmpty( jsonString ) ) {
            return null;
        }
        Object obj = null;
        try {
            obj = mapper.readValue( jsonString, clazz );
        } catch ( JsonParseException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        } catch ( JsonMappingException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        } catch ( IOException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        }
        return (T) obj;
    }

    /**
     * read object value
     *
     * @see #constructParametricType(Class, Class...)
     */
    @SuppressWarnings("unchecked")
    public static <T> T readValue(String jsonString, JavaType javaType) {
        if ( StringUtils.isEmpty( jsonString ) ) {
            return null;
        }
        Object obj = null;
        try {
            obj = mapper.readValue( jsonString, javaType );
        } catch ( JsonParseException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        } catch ( JsonMappingException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        } catch ( IOException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        }
        return (T) obj;
    }

    /**
     * read object value
     *
     * @see #constructParametricType(Class, Class...)
     */
    @SuppressWarnings("unchecked")
    public static <T> T readValue(String jsonString, TypeReference<T> typeReference) {
        if ( StringUtils.isEmpty( jsonString ) ) {
            return null;
        }
        Object obj = null;
        try {
            obj = mapper.readValue( jsonString, typeReference );
        } catch ( JsonParseException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        } catch ( JsonMappingException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        } catch ( IOException e ) {
            logger.info( e.getMessage() );
            e.printStackTrace();
        }
        return (T) obj;
    }

    /**
     * String to tree
     *
     * @param content
     * @return
     * @throws JsonProcessingException
     * @throws IOException
     */
    public static JsonNode readTree(String content) throws JsonProcessingException, IOException {
        return mapper.readTree( content );
    }

    /**
     * Get JSON information
     *
     * @param type
     * @param forward
     * @param msg
     * @return
     */
    public static String getJsonString(Object type, String forward, Object msg) {
        Map<String, Object> map = new LinkedHashMap<String, Object>();
        map.put( "result", type );
        map.put( "forward", forward );
        if ( msg != null ) {
            map.put( "message", msg );
        } else {
        }
        String jsonStr = JsonMapperUtils.writeValueAsString( map );
        logger.info( jsonStr );
        return jsonStr;
    }

	public static boolean getResult(String jsonStr) {
		boolean ret = false;
		Map<String, Object> map = JsonMapperUtils.readValue(jsonStr, Map.class);
		if ((int) map.get("result") == Constants.STATUS_SUCCESS) {
			ret = true;
		}
		return ret;
	}

	public static Object getMessage(String jsonStr) {
		Map<String, Object> map = JsonMapperUtils.readValue(jsonStr, Map.class);
		return (Object)map.get("message");
	}

	/**
	 * get json boject
	 * @param jsonStr
	 * @return
	 */
	public static JSONObject getJsonObject(String jsonStr)
	{
		JSONObject jsonObject = JSONObject.parseObject(jsonStr);
        return jsonObject;
	}

    public static  List getJsonList(String jsonstring, Class cls) {
        List list = new ArrayList();
        try {
            list = JSON.parseArray(jsonstring, cls);
        } catch (Exception e) {
            // TODO: handle exception
        }
        return list;
    }

}