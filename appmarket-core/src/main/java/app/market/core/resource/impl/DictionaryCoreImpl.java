/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.core.resource.impl;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.market.core.resource.DictionaryCore;
import app.market.model.resource.Dictionary;
import app.market.model.resource.DictionaryExample;
import app.market.model.resource.DictionaryExample.Criteria;
import app.market.persistence.mapper.resource.DictionaryMapper;

@Service
@Transactional
public class DictionaryCoreImpl implements DictionaryCore {

    @javax.annotation.Resource(name = "DictionaryMapper")
    private DictionaryMapper dicMapper;

	@Override
	public List<Dictionary> selectItemsByTypeId(String typeId) {
        List<Dictionary> list = dicMapper.selectItemsByTypeId( typeId );
        return list;
	}

	@Override
	public int countDicforItem(String dicTypeId, String valueId){
		DictionaryExample example = new DictionaryExample();
		Criteria uc = example.createCriteria();
		uc.andDicValueEqualTo(valueId);
		uc.andDicTypeEqualTo(dicTypeId);
		return dicMapper.countByExample(example);
	}

	@Override
	public int insert(Dictionary record) {
		int i = dicMapper.insert(record);
		return i;
	}

	@Override
	public int updateByExample(Dictionary record, DictionaryExample example) {
		int i = dicMapper.updateByExample(record, example);
		return i;
	}

	@Override
	public int countByExample(DictionaryExample example) {
        int ret = dicMapper.countByExample(example);
        return ret;
	}
}
