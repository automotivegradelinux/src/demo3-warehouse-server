/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.core.resource;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import app.market.model.resource.Dictionary;
import app.market.model.resource.DictionaryExample;

public interface DictionaryCore {

    List<Dictionary> selectItemsByTypeId(String typeId);

	int countDicforItem(String dicTypeId, String valueId);

	int insert(Dictionary record);

	int updateByExample(@Param("record") Dictionary record, @Param("example") DictionaryExample example);

	int countByExample(DictionaryExample example);
}
