/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.core.authority.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.market.core.authority.AuthorityCore;
import app.market.model.authority.Authority;
import app.market.persistence.mapper.authority.AuthorityMapper;
import app.market.utils.property.Option;
import app.market.utils.property.PropertyUtil;

@Service
@Transactional
public class AuthorityCoreImpl implements AuthorityCore {

    private static Logger logger = LoggerFactory.getLogger( AuthorityCoreImpl.class );

    @Resource(name = "AuthorityMapper")
    private AuthorityMapper authorityMapper;

    @Override
    public List<Option> getAuListOption() {
        logger.info( "Get role list." );
        List<Option> list = new ArrayList<Option>();
        for (Authority au : authorityMapper.selectOption()) {
            Option option = new Option( au.getAuName(), au.getAuId().toString() );
            list.add( option );
        }
        return list;
    }

}
