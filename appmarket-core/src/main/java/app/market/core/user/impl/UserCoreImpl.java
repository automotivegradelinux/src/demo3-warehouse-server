/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.core.user.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.exceptions.TooManyResultsException;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.market.core.user.UserCore;
import app.market.model.user.User;
import app.market.model.user.UserAuthorityLinkExample;
import app.market.model.user.UserAuthorityLinkKey;
import app.market.model.user.UserExample;
import app.market.persistence.mapper.user.UserAuthorityLinkMapper;
import app.market.persistence.mapper.user.UserMapper;
import app.market.utils.Utils;
import app.market.utils.exception.ExceptionEnum;

@Service
@Transactional
public class UserCoreImpl implements UserCore {

    private static Logger logger = LoggerFactory.getLogger( UserCoreImpl.class );

    @Resource(name = "UserMapper")
    private UserMapper userMapper;

    @Resource(name = "UserAuthorityLinkMapper")
    private UserAuthorityLinkMapper userAuthorityLinkMapper;

    @Override
    public User selectByExample(UserExample example) throws Exception {
        try {
            return userMapper.selectOneByExample( example );
        } catch ( TooManyResultsException exception ) {
            logger.debug( "MultiUser count is found." );
            throw new Exception( ExceptionEnum.MULTIPLE_DATA.toString() );
        }
    }

    @Override
    public List<User> selectByExample(int offset, int limit, UserExample example) {
        return userMapper.selectByExample( new RowBounds( offset, limit ), example );
    }

    @Override
    public int countByExample(UserExample example) {
        int ret = userMapper.countByExample( example );
        return ret;
    }

    @Override
    public User selectUserByUserId(String userId) {
        User user = userMapper.selectByPrimaryKey( userId );
        return user;
    }

    @Override
    public User selectUserByUserName(String userName) {
        User user = userMapper.selectByUserName( userName );
        return user;
    }

    @Override
    public int saveUser(User user) {
		int ret = 0;
		if (StringUtils.isEmpty(user.getUserId())) {
			if (userMapper.countByUserName(user.getUserName()) == 0) {
				user.setUserId(Utils.generatorUUID());
				ret = userMapper.insert(user);
			} else {
				return -1;
			}
		} else {
			ret = userMapper.updateByPrimaryKeySelective(user);
		}

		// Delete user authority
		UserAuthorityLinkExample example = new UserAuthorityLinkExample();
		app.market.model.user.UserAuthorityLinkExample.Criteria criteria = example.createCriteria();
		criteria.andUserIdEqualTo(user.getUserId());
		userAuthorityLinkMapper.deleteByExample(example);

		// Add user authority
		UserAuthorityLinkKey record = new UserAuthorityLinkKey();
		record.setUserId(user.getUserId());
		record.setAuId(user.getAuId());
		userAuthorityLinkMapper.insert(record);
		return ret;
    }

    public int updatePassword(User user) {
        return userMapper.updatePassword( user );
    }

    @Override
    public int deleteByUserId(String id) {
        // Delete user authority
        UserAuthorityLinkExample example = new UserAuthorityLinkExample();
        app.market.model.user.UserAuthorityLinkExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo( id );
        userAuthorityLinkMapper.deleteByExample( example );

        int ret = userMapper.deleteByPrimaryKey( id );

        return ret;
    }

    @Override
    public int deleteByUserName(String name) {

        // Delete user authority
        UserAuthorityLinkExample example = new UserAuthorityLinkExample();
        app.market.model.user.UserAuthorityLinkExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo( name );
        userAuthorityLinkMapper.deleteByExample( example );

        int ret = userMapper.deleteByUserName( name );

        return ret;
    }

	@Override
	public int countById(String userId) {
		return userMapper.countByUserId( userId );
	}
}
