/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.core.user;

import java.util.List;

import app.market.model.user.User;
import app.market.model.user.UserExample;

public interface UserCore {

    /**
     * Get user by example
     *
     * @param user
     * @return
     * @throws Exception
     */
    User selectByExample(UserExample user) throws Exception;

    /**
     * Get user list by example
     *
     * @param offset
     * @param limit
     * @param example
     * @return
     */
    List<User> selectByExample(int offset, int limit, UserExample example);

    /**
     * Get user list count by example
     *
     * @param example
     * @return
     */
    int countByExample(UserExample example);

    /**
     * Get user list by id
     *
     * @param example
     * @return
     */
    int countById(String userId);

    /**
     * Get user by id
     *
     * @param userId
     * @return
     */
    User selectUserByUserId(String userId);

    /**
     * Save user information
     *
     * @param user
     * @return
     */
    int saveUser(User user);

    /**
     * Change password
     *
     * @param user
     * @return
     */
    int updatePassword(User user);

    /**
     * Delete user by id
     *
     * @param id
     * @return
     */
    int deleteByUserId(String id);

    /**
     * Delete user by name
     *
     * @param name
     * @return
     */
	int deleteByUserName(String Name);

	/**
	 * Get user by user name
	 * @param userName
	 * @return
	 */
	User selectUserByUserName(String userName);
}
