/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.core.app.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import app.market.core.app.AppCore;
import app.market.core.user.impl.UserCoreImpl;
import app.market.model.app.App;
import app.market.model.app.AppExample;
import app.market.model.app.AppVersion;
import app.market.model.app.AppVersionExample;
import app.market.persistence.mapper.app.AppMapper;
import app.market.persistence.mapper.app.AppVersionMapper;
import app.market.utils.Utils;

@Service
@Transactional
public class AppCoreImpl implements AppCore{

    private static Logger logger = LoggerFactory.getLogger( UserCoreImpl.class );

    @Resource(name = "AppMapper")
    private AppMapper mapper;

    @Resource(name = "AppVersionMapper")
    private AppVersionMapper versionMapper;

	@Override
	public List<App> selectByExample(int offset, int limit, AppExample example) {
		mapper.selectByExample(example);
		return mapper.selectByExample( new RowBounds( offset, limit ), example );
	}

	@Override
	public int countByExample(AppExample example) {
        int ret = mapper.countByExample( example );
        return ret;
	}

	@Override
	public App selectAppByAppId(String appId) {
        App app = mapper.selectByPrimaryKey( appId );
        return app;
	}

	@Override
	public App selectAppByAppCustomId(String appIdCustom) {
        App app = mapper.selectByCustomId( appIdCustom );
        return app;
	}

	@Override
	public int save(App app) {
        int ret = 0;
        if ( StringUtils.isEmpty( app.getAppId() ) ) {
        	app.setAppId( Utils.generatorUUID() );
        	ret = mapper.insert( app );
		} else {
			ret = mapper.updateByPrimaryKeySelective(app);
		}


        return ret;
	}

	@Override
	public int deleteByAppId(String appId) {
		int ret = 0;
		// delete version
		ret = versionMapper.deleteByAppId(appId);

		// delete application information
		ret = mapper.deleteByAppId(appId);
		return ret;
	}

	@Override
	public int saveVision(AppVersion appVer) {
        int ret = 0;

        if ( StringUtils.isEmpty( appVer.getVersionId())) {
        	appVer.setVersionId( Utils.generatorUUID() );
        	ret = versionMapper.insert(appVer);
        } else {
        	ret = versionMapper.updateByPrimaryKeySelective(appVer);
		}

        return ret;
	}

	@Override
	public int countByExample(AppVersionExample example) {
        int ret = versionMapper.countByExample( example );
        return ret;
	}
}
