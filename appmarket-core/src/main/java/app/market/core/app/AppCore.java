/*
 * Copyright (c) 2019 TOYOTA MOTOR CORPORATION
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package app.market.core.app;

import java.util.List;

import app.market.model.app.App;
import app.market.model.app.AppExample;
import app.market.model.app.AppVersion;
import app.market.model.app.AppVersionExample;



public interface AppCore {

    /**
     * Get application information
     *
     * @param offset
     * @param limit
     * @param example
     * @return
     */
    List<App> selectByExample(int offset, int limit, AppExample example);

    /**
     * Get total count of applications
     *
     * @param example
     * @return
     */
    int countByExample(AppExample example);

    /**
     * Search application by ID
     *
     * @param appId
     * @return
     */
    App selectAppByAppId(String appId);

    /**
     * Search application by IdCustom
     * @param appIdCustom
     * @return
     */
    App selectAppByAppCustomId(String appIdCustom);

    /**
     * Save application information
     *
     * @param app
     * @return
     */
    int save(App app);

    /**
     * Delete application
     *
     * @param id
     * @return
     */
    int deleteByAppId(String id);

	/**
	 * Save  application version
	 * @param appVer
	 * @return
	 */
	int saveVision(AppVersion appVer);

    /**
     * Get total count of app versions
     *
     * @param example
     * @return
     */
	int countByExample(AppVersionExample example);
}
